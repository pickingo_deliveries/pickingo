# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.first_or_create!(email:'prashant@gmail.com',password:'cloudno9')

['Gurgaon','New Delhi'].each do |city|
  Hub.find_or_create_by!(:name => city, :city => city)
end

# User.create!(email:'hubowner@gmail.com',password:'cloudno9',:role => 'hub_owner', :hub => Hub.find_by(:name => 'Gurgaon'))
User.create!(email:'delhihub@gmail.com',password:'cloudno9',:role => 'hub_owner', :hub => Hub.find_by(:name => 'New Delhi'))
# User.create!(email:'noidahub@gmail.com',password:'cloudno9',:role => 'hub_owner', :hub => Hub.find_by(:name => 'Noida'))


CSV.foreach("db/pincodes.csv",:headers=> :first_row) do |row|
  Pincode.find_or_create_by!(:code => row[0],:hub =>Hub.find_by(:city => row[1]))
end

CSV.foreach("db/pickup_boys.csv", :headers => :first_row) do |row|
  PickupBoy.create!(:name => row[0], :hub => Hub.find_by(:city => row[1]), :phone => row[2],:password => 'pickingo')
end

['Jabong','Snapdeal','Fabfurnish','Koovs','Shopclues'].each do |client|
  Client.find_or_create_by!(:name => client)
end
