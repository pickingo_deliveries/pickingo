# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151204065238) do

  create_table "Line_Haul_Report_harshit", id: false, force: :cascade do |t|
    t.integer  "id",                     limit: 4
    t.string   "Client_Request_id",      limit: 255
    t.string   "Current_status",         limit: 255
    t.datetime "Created_at"
    t.datetime "Received_at"
    t.datetime "HubManifest_created_at"
    t.datetime "HubManifest_transit_at"
    t.datetime "DC_received_at"
    t.datetime "DCManifest_created_at"
    t.datetime "DCManifest_transit_at"
    t.datetime "Closed_at"
    t.string   "hub_name",               limit: 255
    t.string   "client_name",            limit: 255
    t.string   "Coloader_name",          limit: 255
    t.string   "Coloader_mode",          limit: 255
  end

  create_table "Line_Haul_Report_harshit_old", id: false, force: :cascade do |t|
    t.integer  "id",                     limit: 4
    t.integer  "Client_Request_id",      limit: 4
    t.string   "Current_status",         limit: 255
    t.datetime "Created_at"
    t.datetime "Received_at"
    t.datetime "HubManifest_created_at"
    t.datetime "HubManifest_transit_at"
    t.datetime "DC_received_at"
    t.datetime "DCManifest_created_at"
    t.datetime "DCManifest_transit_at"
    t.datetime "Closed_at"
    t.string   "hub_name",               limit: 255
    t.string   "client_name",            limit: 255
    t.string   "Coloader_name",          limit: 255
    t.string   "Coloader_mode",          limit: 255
  end

  create_table "Rishav_Pickup_Report", id: false, force: :cascade do |t|
    t.integer  "id",                  limit: 4
    t.datetime "Created_at"
    t.datetime "Updated_at"
    t.string   "client_order_number", limit: 255
    t.string   "client_request_id",   limit: 255
    t.string   "preffered_location",  limit: 255
    t.string   "Order_status",        limit: 255
    t.integer  "pickup_boy_id",       limit: 4
    t.string   "pickup_boy_name",     limit: 255
    t.integer  "hub_id",              limit: 4
    t.string   "hub_name",            limit: 255
    t.string   "remarks",             limit: 255
    t.integer  "Client_id",           limit: 4
    t.string   "client_name",         limit: 255
    t.datetime "scheduled_date"
    t.datetime "last_attempt_date"
    t.integer  "runsheet_id",         limit: 4
    t.datetime "Picked_Date"
    t.datetime "WH_Received_date"
    t.datetime "first_attempt_date"
    t.integer  "Attempts",            limit: 4
    t.integer  "not_attempted_count", limit: 4
    t.decimal  "Weight",                          precision: 10, scale: 3
    t.decimal  "Vol_weight",                      precision: 10, scale: 3
    t.string   "locality",            limit: 255
    t.string   "pincode",             limit: 255
    t.string   "sub_locality",        limit: 255
    t.string   "Last_day_status",     limit: 255
  end

  create_table "addresses", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.text     "address_line",      limit: 65535
    t.string   "city",              limit: 255
    t.integer  "pincode",           limit: 4
    t.string   "country",           limit: 255,   default: "India"
    t.string   "phone_number",      limit: 255
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "pickup_request_id", limit: 4
    t.float    "lat",               limit: 24
    t.float    "lon",               limit: 24
    t.string   "locality",          limit: 255
    t.string   "sub_locality",      limit: 255
    t.string   "formatted_address", limit: 255
    t.boolean  "is_ambiguous",      limit: 1,     default: false
    t.boolean  "is_manual",         limit: 1,     default: false
  end

  add_index "addresses", ["locality"], name: "index_addresses_on_locality", using: :btree
  add_index "addresses", ["pickup_request_id"], name: "index_addresses_on_pickup_request_id", using: :btree
  add_index "addresses", ["sub_locality"], name: "index_addresses_on_sub_locality", using: :btree

  create_table "asset_transitions", force: :cascade do |t|
    t.integer  "asset_id",    limit: 4
    t.integer  "user_id",     limit: 4
    t.string   "state",       limit: 255
    t.date     "assign_date"
    t.date     "return_date"
    t.integer  "quantity",    limit: 4
    t.text     "remarks",     limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "asset_transitions", ["asset_id"], name: "index_asset_transitions_on_asset_id", using: :btree
  add_index "asset_transitions", ["user_id"], name: "index_asset_transitions_on_user_id", using: :btree

  create_table "assets", force: :cascade do |t|
    t.string   "asset_type",   limit: 255
    t.string   "serial_no",    limit: 255
    t.string   "reference_no", limit: 255
    t.integer  "entity_id",    limit: 4
    t.string   "entity_type",  limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "state",        limit: 255
    t.text     "if_other",     limit: 65535
    t.integer  "user_id",      limit: 4
  end

  create_table "attendances", force: :cascade do |t|
    t.integer  "pickup_boy_id",          limit: 4
    t.date     "date"
    t.float    "start_km",               limit: 24
    t.float    "end_km",                 limit: 24
    t.integer  "start_time",             limit: 4
    t.integer  "end_time",               limit: 4
    t.string   "status",                 limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.datetime "check_in_time"
    t.datetime "check_out_time"
    t.string   "start_pic_file_name",    limit: 255
    t.string   "start_pic_content_type", limit: 255
    t.integer  "start_pic_file_size",    limit: 4
    t.datetime "start_pic_updated_at"
    t.string   "end_pic_file_name",      limit: 255
    t.string   "end_pic_content_type",   limit: 255
    t.integer  "end_pic_file_size",      limit: 4
    t.datetime "end_pic_updated_at"
    t.integer  "user_id",                limit: 4
  end

  add_index "attendances", ["date"], name: "index_attendances_on_date", using: :btree
  add_index "attendances", ["pickup_boy_id"], name: "index_attendances_on_pickup_boy_id", using: :btree

  create_table "awb_numbers", force: :cascade do |t|
    t.string   "code",              limit: 255
    t.integer  "client_dsp_id",     limit: 4
    t.boolean  "used",              limit: 1,   default: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "pickup_request_id", limit: 4
  end

  add_index "awb_numbers", ["client_dsp_id"], name: "index_awb_numbers_on_client_dsp_id", using: :btree
  add_index "awb_numbers", ["pickup_request_id"], name: "awb_pur", using: :btree

  create_table "barcode_sheets", force: :cascade do |t|
    t.integer  "pickup_boy_id",     limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.integer  "user_id",           limit: 4
  end

  add_index "barcode_sheets", ["pickup_boy_id"], name: "index_barcode_sheets_on_pickup_boy_id", using: :btree

  create_table "bundled_slips", force: :cascade do |t|
    t.string   "slip_file_name",    limit: 255
    t.string   "slip_content_type", limit: 255
    t.integer  "slip_file_size",    limit: 4
    t.datetime "slip_updated_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "client_customers", force: :cascade do |t|
    t.string   "provider",               limit: 255,                null: false
    t.string   "uid",                    limit: 255,   default: "", null: false
    t.string   "encrypted_password",     limit: 255,   default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.string   "name",                   limit: 255
    t.string   "nickname",               limit: 255
    t.string   "image",                  limit: 255
    t.string   "email",                  limit: 255
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "client_id",              limit: 4
  end

  add_index "client_customers", ["client_id"], name: "index_client_customers_on_client_id", using: :btree
  add_index "client_customers", ["email"], name: "index_client_customers_on_email", using: :btree
  add_index "client_customers", ["reset_password_token"], name: "index_client_customers_on_reset_password_token", unique: true, using: :btree
  add_index "client_customers", ["uid", "provider"], name: "index_client_customers_on_uid_and_provider", unique: true, using: :btree

  create_table "client_dsp_pincodes", force: :cascade do |t|
    t.integer  "pincode_id",    limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "client_dsp_id", limit: 4
    t.integer  "priority",      limit: 4
  end

  add_index "client_dsp_pincodes", ["client_dsp_id"], name: "index_client_dsp_pincodes_on_client_dsp_id", using: :btree
  add_index "client_dsp_pincodes", ["pincode_id"], name: "index_client_dsp_pincodes_on_pincode_id", using: :btree

  create_table "client_dsps", force: :cascade do |t|
    t.integer  "client_id",  limit: 4
    t.integer  "dsp_id",     limit: 4
    t.integer  "priority",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "client_dsps", ["client_id", "dsp_id"], name: "index_client_dsps_on_client_id_and_dsp_id", using: :btree
  add_index "client_dsps", ["client_id"], name: "index_client_dsps_on_client_id", using: :btree
  add_index "client_dsps", ["dsp_id"], name: "index_client_dsps_on_dsp_id", using: :btree

  create_table "client_pincode_destinations", force: :cascade do |t|
    t.integer  "pincode_id",         limit: 4
    t.integer  "client_id",          limit: 4
    t.integer  "dispatch_center_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "client_pincode_destinations", ["client_id"], name: "cpd_client_index", using: :btree
  add_index "client_pincode_destinations", ["dispatch_center_id"], name: "cpd_dc_index", using: :btree
  add_index "client_pincode_destinations", ["pincode_id"], name: "cpd_pincode_index", using: :btree

  create_table "client_seller_pincode_destinations", force: :cascade do |t|
    t.integer  "client_id",        limit: 4
    t.integer  "pincode_id",       limit: 4
    t.integer  "destination_id",   limit: 4
    t.string   "destination_type", limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "client_seller_pincode_destinations", ["client_id"], name: "cspd_client_id", using: :btree
  add_index "client_seller_pincode_destinations", ["destination_id", "destination_type"], name: "cspd_destination", using: :btree
  add_index "client_seller_pincode_destinations", ["pincode_id"], name: "cspd_pincode_id", using: :btree

  create_table "client_tokens", force: :cascade do |t|
    t.string   "access_token",       limit: 255
    t.integer  "client_customer_id", limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "client_tokens", ["client_customer_id"], name: "index_client_tokens_on_client_customer_id", using: :btree

  create_table "client_warehouses", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "address_line", limit: 255
    t.string   "pincode",      limit: 255
    t.integer  "client_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "client_warehouses", ["client_id"], name: "index_client_warehouses_on_client_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "api_name",   limit: 255
    t.string   "initials",   limit: 255
  end

  add_index "clients", ["name"], name: "index_clients_on_name", using: :btree

  create_table "coloaders", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "identifier", limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string   "title",            limit: 50,    default: ""
    t.text     "comment",          limit: 65535
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 255
    t.integer  "crm_user_id",      limit: 4
    t.string   "role",             limit: 255,   default: "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",           limit: 255
  end

  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id", using: :btree
  add_index "comments", ["commentable_type"], name: "index_comments_on_commentable_type", using: :btree
  add_index "comments", ["crm_user_id"], name: "index_comments_on_crm_user_id", using: :btree

  create_table "crm_users", force: :cascade do |t|
    t.string   "provider",               limit: 255,   default: "email", null: false
    t.string   "uid",                    limit: 255,   default: "",      null: false
    t.string   "encrypted_password",     limit: 255,   default: "",      null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "name",                   limit: 255
    t.string   "nickname",               limit: 255
    t.string   "image",                  limit: 255
    t.string   "email",                  limit: 255
    t.string   "role_string",            limit: 255
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_id",                limit: 4
    t.integer  "department_id",          limit: 4
  end

  add_index "crm_users", ["department_id"], name: "index_crm_users_on_department_id", using: :btree
  add_index "crm_users", ["email"], name: "index_crm_users_on_email", using: :btree
  add_index "crm_users", ["reset_password_token"], name: "index_crm_users_on_reset_password_token", unique: true, using: :btree
  add_index "crm_users", ["role_id"], name: "index_crm_users_on_role_id", using: :btree
  add_index "crm_users", ["uid", "provider"], name: "index_crm_users_on_uid_and_provider", unique: true, using: :btree

  create_table "dc_vehicles", force: :cascade do |t|
    t.integer  "dispatch_center_id", limit: 4
    t.string   "reg_number",         limit: 255
    t.string   "vehicle_model",      limit: 255
    t.string   "driver_name",        limit: 255
    t.string   "driver_phone",       limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "dc_vehicles", ["dispatch_center_id"], name: "dcv_dc", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "delivery_pincodes", force: :cascade do |t|
    t.integer  "pincode_id",           limit: 4
    t.integer  "delivery_center_id",   limit: 4
    t.string   "delivery_center_type", limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "delivery_pincodes", ["delivery_center_id", "delivery_center_type"], name: "dp_dcenter", using: :btree
  add_index "delivery_pincodes", ["pincode_id"], name: "dp_pincode", using: :btree

  create_table "departments", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "display_name", limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "dispatch_center_manifest_histories", force: :cascade do |t|
    t.string   "state",                       limit: 255
    t.integer  "dispatch_center_manifest_id", limit: 4
    t.text     "remarks",                     limit: 65535
    t.integer  "dc_vehicle_id",               limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "dispatch_center_manifest_histories", ["dc_vehicle_id"], name: "dcmh_dcv", using: :btree
  add_index "dispatch_center_manifest_histories", ["dispatch_center_manifest_id"], name: "dcmh_dcm", using: :btree

  create_table "dispatch_center_manifests", force: :cascade do |t|
    t.integer  "origin_id",                          limit: 4
    t.integer  "destination_id",                     limit: 4
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "client_id",                          limit: 4
    t.string   "uid",                                limit: 255
    t.integer  "dc_vehicle_id",                      limit: 4
    t.decimal  "length",                                           precision: 10
    t.decimal  "breadth",                                          precision: 10
    t.decimal  "height",                                           precision: 10
    t.decimal  "weight",                                           precision: 10
    t.string   "state",                              limit: 255
    t.integer  "dispatch_center_master_manifest_id", limit: 4
    t.string   "pdf_file_file_name",                 limit: 255
    t.string   "pdf_file_content_type",              limit: 255
    t.integer  "pdf_file_file_size",                 limit: 4
    t.datetime "pdf_file_updated_at"
    t.string   "excel_file_file_name",               limit: 255
    t.string   "excel_file_content_type",            limit: 255
    t.integer  "excel_file_file_size",               limit: 4
    t.datetime "excel_file_updated_at"
    t.string   "seal_number",                        limit: 255
    t.text     "request_ids",                        limit: 65535
  end

  add_index "dispatch_center_manifests", ["client_id"], name: "dc_client_index", using: :btree
  add_index "dispatch_center_manifests", ["dc_vehicle_id"], name: "dcm_dcv", using: :btree
  add_index "dispatch_center_manifests", ["destination_id"], name: "dc_destination_index", using: :btree
  add_index "dispatch_center_manifests", ["dispatch_center_master_manifest_id"], name: "dcm_dcmm", using: :btree
  add_index "dispatch_center_manifests", ["origin_id"], name: "dc_origin_index", using: :btree

  create_table "dispatch_center_master_manifest_histories", force: :cascade do |t|
    t.string   "state",                              limit: 255
    t.integer  "dispatch_center_master_manifest_id", limit: 4
    t.text     "remarks",                            limit: 65535
    t.integer  "dc_vehicle_id",                      limit: 4
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  add_index "dispatch_center_master_manifest_histories", ["dc_vehicle_id"], name: "dcmmh_dcv", using: :btree
  add_index "dispatch_center_master_manifest_histories", ["dispatch_center_master_manifest_id"], name: "dcmmh_dcmm", using: :btree

  create_table "dispatch_center_master_manifests", force: :cascade do |t|
    t.integer  "origin_id",                       limit: 4
    t.integer  "destination_id",                  limit: 4
    t.string   "state",                           limit: 255
    t.string   "uid",                             limit: 255
    t.integer  "client_id",                       limit: 4
    t.integer  "dc_vehicle_id",                   limit: 4
    t.string   "pdf_file_file_name",              limit: 255
    t.string   "pdf_file_content_type",           limit: 255
    t.integer  "pdf_file_file_size",              limit: 4
    t.datetime "pdf_file_updated_at"
    t.string   "excel_file_file_name",            limit: 255
    t.string   "excel_file_content_type",         limit: 255
    t.integer  "excel_file_file_size",            limit: 4
    t.datetime "excel_file_updated_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "request_excel_file_file_name",    limit: 255
    t.string   "request_excel_file_content_type", limit: 255
    t.integer  "request_excel_file_file_size",    limit: 4
    t.datetime "request_excel_file_updated_at"
  end

  add_index "dispatch_center_master_manifests", ["client_id"], name: "dcmm_client", using: :btree
  add_index "dispatch_center_master_manifests", ["dc_vehicle_id"], name: "dcmm_dcv", using: :btree
  add_index "dispatch_center_master_manifests", ["destination_id"], name: "dcmm_destination", using: :btree
  add_index "dispatch_center_master_manifests", ["origin_id"], name: "dcmm_origin", using: :btree

  create_table "dispatch_centers", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "address_line", limit: 255
    t.string   "city",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "display_name", limit: 255
    t.string   "identifier",   limit: 255
  end

  create_table "dsps", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "identifier",     limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "alternate_name", limit: 255
  end

  create_table "expenses", force: :cascade do |t|
    t.string "exp_type",   limit: 255
    t.string "exp_nature", limit: 255
  end

  create_table "generated_requests", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.integer  "client_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "generated_requests", ["client_id"], name: "index_generated_requests_on_client_id", using: :btree

  create_table "hub_destination_routes", force: :cascade do |t|
    t.integer  "hub_id",         limit: 4
    t.integer  "destination_id", limit: 4
    t.integer  "route_id",       limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "hub_destination_routes", ["destination_id"], name: "index_hub_destination_routes_on_destination_id", using: :btree
  add_index "hub_destination_routes", ["hub_id"], name: "index_hub_destination_routes_on_hub_id", using: :btree
  add_index "hub_destination_routes", ["route_id"], name: "index_hub_destination_routes_on_route_id", using: :btree

  create_table "hub_manifest_histories", force: :cascade do |t|
    t.string   "state",           limit: 255
    t.integer  "hub_manifest_id", limit: 4
    t.string   "remarks",         limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "hub_manifest_histories", ["hub_manifest_id"], name: "index_hub_manifest_histories_on_hub_manifest_id", using: :btree

  create_table "hub_manifests", force: :cascade do |t|
    t.integer  "origin_id",               limit: 4
    t.integer  "destination_id",          limit: 4
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "uid",                     limit: 255
    t.string   "state",                   limit: 255
    t.integer  "hub_master_manifest_id",  limit: 4
    t.string   "destination_type",        limit: 255
    t.decimal  "weight",                                precision: 10
    t.decimal  "length",                                precision: 10
    t.decimal  "breadth",                               precision: 10
    t.decimal  "height",                                precision: 10
    t.string   "pdf_file_file_name",      limit: 255
    t.string   "pdf_file_content_type",   limit: 255
    t.integer  "pdf_file_file_size",      limit: 4
    t.datetime "pdf_file_updated_at"
    t.string   "excel_file_file_name",    limit: 255
    t.string   "excel_file_content_type", limit: 255
    t.integer  "excel_file_file_size",    limit: 4
    t.datetime "excel_file_updated_at"
    t.string   "seal_number",             limit: 255
    t.text     "request_ids",             limit: 65535
  end

  add_index "hub_manifests", ["destination_id", "destination_type"], name: "manifest_destination_index", using: :btree
  add_index "hub_manifests", ["hub_master_manifest_id"], name: "hm_hmm_index", using: :btree
  add_index "hub_manifests", ["origin_id"], name: "index_hub_manifests_on_origin_id", using: :btree

  create_table "hub_master_manifest_histories", force: :cascade do |t|
    t.string   "state",                  limit: 255
    t.integer  "hub_master_manifest_id", limit: 4
    t.string   "remarks",                limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "hub_master_manifest_histories", ["hub_master_manifest_id"], name: "histories_index", using: :btree

  create_table "hub_master_manifests", force: :cascade do |t|
    t.integer  "origin_id",               limit: 4
    t.integer  "destination_id",          limit: 4
    t.string   "destination_type",        limit: 255
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.string   "state",                   limit: 255
    t.string   "uid",                     limit: 255
    t.integer  "coloader_id",             limit: 4
    t.string   "transport_mode",          limit: 255
    t.string   "tracking_id",             limit: 255
    t.string   "pdf_file_file_name",      limit: 255
    t.string   "pdf_file_content_type",   limit: 255
    t.integer  "pdf_file_file_size",      limit: 4
    t.datetime "pdf_file_updated_at"
    t.string   "excel_file_file_name",    limit: 255
    t.string   "excel_file_content_type", limit: 255
    t.integer  "excel_file_file_size",    limit: 4
    t.datetime "excel_file_updated_at"
    t.decimal  "chargeable_weight",                   precision: 10, scale: 2
    t.decimal  "gross_weight",                        precision: 10, scale: 2
  end

  add_index "hub_master_manifests", ["coloader_id"], name: "hmm_coloader", using: :btree
  add_index "hub_master_manifests", ["destination_id", "destination_type"], name: "master_destination_index", using: :btree
  add_index "hub_master_manifests", ["origin_id"], name: "origin_index", using: :btree

  create_table "hubs", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "address_line", limit: 255
    t.string   "city",         limit: 255
    t.string   "identifier",   limit: 255
    t.boolean  "dec",          limit: 1,   default: false
  end

  add_index "hubs", ["name"], name: "index_hubs_on_name", using: :btree

  create_table "inventory_excels", force: :cascade do |t|
    t.string   "excel_file_file_name",    limit: 255
    t.string   "excel_file_content_type", limit: 255
    t.integer  "excel_file_file_size",    limit: 4
    t.datetime "excel_file_updated_at"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "pincode",      limit: 255
    t.string   "sub_string",   limit: 255
    t.string   "sub_locality", limit: 255
    t.string   "locality",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "pickup_boys", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "phone",                  limit: 255
    t.integer  "hub_id",                 limit: 4
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "provider",               limit: 255,   default: "email", null: false
    t.string   "uid",                    limit: 255,   default: "",      null: false
    t.string   "encrypted_password",     limit: 255,   default: "",      null: false
    t.string   "reset_password_token",   limit: 255
    t.string   "reset_password_sent_at", limit: 255
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.string   "unconfirmed_email",      limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "nickname",               limit: 255
    t.string   "image",                  limit: 255
    t.string   "email",                  limit: 255
    t.text     "tokens",                 limit: 65535
    t.boolean  "active",                 limit: 1,     default: true
    t.string   "aasm_state",             limit: 255
  end

  add_index "pickup_boys", ["email"], name: "index_pickup_boys_on_email", using: :btree
  add_index "pickup_boys", ["hub_id"], name: "index_pickup_boys_on_hub_id", using: :btree
  add_index "pickup_boys", ["name"], name: "index_pickup_boys_on_name", using: :btree
  add_index "pickup_boys", ["reset_password_token"], name: "index_pickup_boys_on_reset_password_token", unique: true, using: :btree
  add_index "pickup_boys", ["uid", "provider"], name: "index_pickup_boys_on_uid_and_provider", unique: true, using: :btree

  create_table "pickup_request_state_histories", force: :cascade do |t|
    t.string   "state",                 limit: 255
    t.integer  "pickup_request_id",     limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.text     "comment",               limit: 65535
    t.integer  "pickup_boy_id",         limit: 4
    t.integer  "manifest_id",           limit: 4
    t.string   "manifest_type",         limit: 255
    t.integer  "user_id",               limit: 4
    t.integer  "current_location_id",   limit: 4
    t.string   "current_location_type", limit: 255
  end

  add_index "pickup_request_state_histories", ["manifest_id", "manifest_type"], name: "prsh_manifest", using: :btree
  add_index "pickup_request_state_histories", ["pickup_request_id"], name: "index_pickup_request_state_histories_on_pickup_request_id", using: :btree

  create_table "pickup_requests", force: :cascade do |t|
    t.datetime "created_at",                                                                         null: false
    t.datetime "updated_at",                                                                         null: false
    t.string   "client_order_number",         limit: 255
    t.string   "client_request_id",           limit: 255
    t.string   "preferred_location",          limit: 255
    t.date     "preferred_date"
    t.string   "preferred_time",              limit: 255
    t.string   "request_type",                limit: 255
    t.string   "aasm_state",                  limit: 255
    t.integer  "pickup_boy_id",               limit: 4
    t.integer  "hub_id",                      limit: 4
    t.string   "barcode",                     limit: 255
    t.string   "remarks",                     limit: 255
    t.integer  "client_id",                   limit: 4
    t.date     "scheduled_date"
    t.decimal  "weight",                                    precision: 10, scale: 3, default: 0.0,   null: false
    t.decimal  "length",                                    precision: 10, scale: 3, default: 0.0
    t.decimal  "breadth",                                   precision: 10, scale: 3, default: 0.0
    t.decimal  "height",                                    precision: 10, scale: 3, default: 0.0
    t.integer  "runsheet_id",                 limit: 4
    t.integer  "barcode_sheet_id",            limit: 4
    t.string   "signature_file_name",         limit: 255
    t.string   "signature_content_type",      limit: 255
    t.integer  "signature_file_size",         limit: 4
    t.datetime "signature_updated_at"
    t.integer  "seller_local_manifest_id",    limit: 4
    t.integer  "seller_dispatch_manifest_id", limit: 4
    t.integer  "dsp_id",                      limit: 4
    t.integer  "final_destination_id",        limit: 4
    t.string   "final_destination_type",      limit: 255
    t.integer  "current_location_id",         limit: 4
    t.string   "current_location_type",       limit: 255
    t.integer  "manifest_id",                 limit: 4
    t.string   "manifest_type",               limit: 255
    t.integer  "user_id",                     limit: 4
    t.decimal  "initial_weight",                            precision: 10, scale: 2
    t.decimal  "price",                                     precision: 10, scale: 2
    t.text     "return_reason",               limit: 65535
    t.boolean  "via_api",                     limit: 1,                              default: false
    t.boolean  "dispatch_flag",               limit: 1,                              default: false
    t.integer  "is_urgent",                   limit: 4,                              default: 0
    t.datetime "picked_on"
    t.string   "cancelled_by",                limit: 255,                            default: " "
  end

  add_index "pickup_requests", ["aasm_state"], name: "index_pickup_requests_on_aasm_state", using: :btree
  add_index "pickup_requests", ["client_id"], name: "index_pickup_requests_on_client_id", using: :btree
  add_index "pickup_requests", ["client_request_id"], name: "index_pickup_requests_on_client_request_id", using: :btree
  add_index "pickup_requests", ["current_location_id", "current_location_type"], name: "pur_cl", using: :btree
  add_index "pickup_requests", ["dsp_id"], name: "index_pickup_requests_on_dsp_id", using: :btree
  add_index "pickup_requests", ["final_destination_id", "final_destination_type"], name: "pur_fd", using: :btree
  add_index "pickup_requests", ["hub_id"], name: "index_pickup_requests_on_hub_id", using: :btree
  add_index "pickup_requests", ["pickup_boy_id"], name: "index_pickup_requests_on_pickup_boy_id", using: :btree
  add_index "pickup_requests", ["scheduled_date"], name: "index_pickup_requests_on_scheduled_date", using: :btree

  create_table "pincodes", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.integer  "hub_id",     limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "pincodes", ["code"], name: "index_pincodes_on_code", using: :btree
  add_index "pincodes", ["hub_id"], name: "index_pincodes_on_hub_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "department_id", limit: 4
    t.string   "short_name",    limit: 255
  end

  add_index "roles", ["department_id"], name: "index_roles_on_department_id", using: :btree

  create_table "routes", force: :cascade do |t|
    t.integer  "origin_id",              limit: 4
    t.string   "default",                limit: 255, default: "1"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "route_destination_id",   limit: 4
    t.string   "route_destination_type", limit: 255
  end

  add_index "routes", ["origin_id"], name: "index_routes_on_origin_id", using: :btree
  add_index "routes", ["route_destination_id", "route_destination_type"], name: "route_destination", using: :btree

  create_table "runsheets", force: :cascade do |t|
    t.integer  "pickup_boy_id",     limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.integer  "user_id",           limit: 4
  end

  add_index "runsheets", ["pickup_boy_id"], name: "index_runsheets_on_pickup_boy_id", using: :btree

  create_table "seller_dispatch_manifest_histories", force: :cascade do |t|
    t.string   "state",                       limit: 255
    t.text     "remarks",                     limit: 65535
    t.integer  "seller_dispatch_manifest_id", limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "seller_dispatch_manifest_histories", ["seller_dispatch_manifest_id"], name: "sdmh_sdm", using: :btree

  create_table "seller_dispatch_manifests", force: :cascade do |t|
    t.integer  "origin_id",               limit: 4
    t.string   "tracking_id",             limit: 255
    t.integer  "dsp_id",                  limit: 4
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "state",                   limit: 255
    t.string   "pdf_file_file_name",      limit: 255
    t.string   "pdf_file_content_type",   limit: 255
    t.integer  "pdf_file_file_size",      limit: 4
    t.datetime "pdf_file_updated_at"
    t.string   "excel_file_file_name",    limit: 255
    t.string   "excel_file_content_type", limit: 255
    t.integer  "excel_file_file_size",    limit: 4
    t.datetime "excel_file_updated_at"
    t.decimal  "length",                              precision: 10
    t.decimal  "breadth",                             precision: 10
    t.decimal  "height",                              precision: 10
    t.decimal  "weight",                              precision: 10
    t.string   "origin_type",             limit: 255
    t.string   "uid",                     limit: 255
  end

  add_index "seller_dispatch_manifests", ["dsp_id"], name: "sdm_dsp", using: :btree
  add_index "seller_dispatch_manifests", ["origin_id", "origin_type"], name: "sdm_origin", using: :btree

  create_table "seller_local_manifest_histories", force: :cascade do |t|
    t.integer  "seller_local_manifest_id", limit: 4
    t.string   "state",                    limit: 255
    t.text     "remarks",                  limit: 65535
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "seller_local_manifest_histories", ["seller_local_manifest_id"], name: "slmh_slm", using: :btree

  create_table "seller_local_manifests", force: :cascade do |t|
    t.integer  "origin_id",               limit: 4
    t.string   "state",                   limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "dsp_id",                  limit: 4
    t.string   "pdf_file_file_name",      limit: 255
    t.string   "pdf_file_content_type",   limit: 255
    t.integer  "pdf_file_file_size",      limit: 4
    t.datetime "pdf_file_updated_at"
    t.string   "excel_file_file_name",    limit: 255
    t.string   "excel_file_content_type", limit: 255
    t.integer  "excel_file_file_size",    limit: 4
    t.datetime "excel_file_updated_at"
    t.decimal  "length",                              precision: 10
    t.decimal  "breadth",                             precision: 10
    t.decimal  "height",                              precision: 10
    t.decimal  "weight",                              precision: 10
    t.string   "origin_type",             limit: 255
    t.string   "uid",                     limit: 255
    t.string   "tracking_id",             limit: 255
  end

  add_index "seller_local_manifests", ["dsp_id"], name: "slm_dsp", using: :btree
  add_index "seller_local_manifests", ["origin_id", "origin_type"], name: "slm_origin", using: :btree

  create_table "seller_slips", force: :cascade do |t|
    t.integer  "pickup_request_id", limit: 4
    t.string   "slip_file_name",    limit: 255
    t.string   "slip_content_type", limit: 255
    t.integer  "slip_file_size",    limit: 4
    t.datetime "slip_updated_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "seller_slips", ["pickup_request_id"], name: "ss_pur", using: :btree

  create_table "sellers", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.text     "address_line",      limit: 65535
    t.string   "city",              limit: 255
    t.string   "country",           limit: 255,   default: "India"
    t.string   "phone",             limit: 255
    t.integer  "pickup_request_id", limit: 4
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "pincode_id",        limit: 4
  end

  add_index "sellers", ["pickup_request_id"], name: "index_sellers_on_pickup_request_id", using: :btree
  add_index "sellers", ["pincode_id"], name: "index_sellers_on_pincode_id", using: :btree

  create_table "skus", force: :cascade do |t|
    t.string   "client_sku_id",     limit: 255
    t.text     "name",              limit: 65535
    t.integer  "pickup_request_id", limit: 4
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.boolean  "picked",            limit: 1,                              default: false
    t.decimal  "price",                           precision: 10, scale: 2
  end

  add_index "skus", ["pickup_request_id"], name: "index_skus_on_pickup_request_id", using: :btree

  create_table "sms", force: :cascade do |t|
    t.string   "phone",                           limit: 255
    t.string   "content",                         limit: 255
    t.integer  "pickup_request_state_history_id", limit: 4
    t.boolean  "sent",                            limit: 1,   default: false
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.integer  "transaction_id",                  limit: 4
  end

  add_index "sms", ["pickup_request_state_history_id"], name: "index_sms_on_pickup_request_state_history_id", using: :btree

  create_table "subscribers", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "ticket_sub_types", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "display_name",   limit: 255
    t.integer  "ticket_type_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "ticket_sub_types", ["ticket_type_id"], name: "index_ticket_sub_types_on_ticket_type_id", using: :btree

  create_table "ticket_types", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "display_name",  limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "identifier",    limit: 255
    t.integer  "department_id", limit: 4
  end

  add_index "ticket_types", ["department_id"], name: "index_ticket_types_on_department_id", using: :btree

  create_table "tickets", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.text     "description",        limit: 65535
    t.integer  "owner_id",           limit: 4
    t.integer  "assignee_id",        limit: 4
    t.integer  "pickup_request_id",  limit: 4
    t.integer  "priority",           limit: 4
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "ticket_type_id",     limit: 4
    t.integer  "ticket_sub_type_id", limit: 4
    t.string   "uid",                limit: 255
    t.string   "status",             limit: 255
    t.integer  "assigned_to_id",     limit: 4
    t.integer  "state",              limit: 4
    t.integer  "statefield",         limit: 4
    t.string   "call_type",          limit: 255,   default: ""
  end

  add_index "tickets", ["assigned_to_id"], name: "index_tickets_on_assigned_to_id", using: :btree
  add_index "tickets", ["assignee_id"], name: "index_tickets_on_assignee_id", using: :btree
  add_index "tickets", ["owner_id"], name: "index_tickets_on_owner_id", using: :btree
  add_index "tickets", ["pickup_request_id"], name: "index_tickets_on_pickup_request_id", using: :btree
  add_index "tickets", ["ticket_sub_type_id"], name: "index_tickets_on_ticket_sub_type_id", using: :btree
  add_index "tickets", ["ticket_type_id"], name: "index_tickets_on_ticket_type_id", using: :btree

  create_table "transaction_excels", force: :cascade do |t|
    t.string   "excel_file_file_name",    limit: 255
    t.string   "excel_file_content_type", limit: 255
    t.integer  "excel_file_file_size",    limit: 4
    t.datetime "excel_file_updated_at"
  end

  create_table "transactions", force: :cascade do |t|
    t.string   "expense_type",     limit: 255
    t.string   "expense_sub_type", limit: 255
    t.string   "sender_id",        limit: 255
    t.string   "reciever_id",      limit: 255
    t.string   "mod_of_payment",   limit: 255
    t.integer  "amount",           limit: 4
    t.string   "transaction_type", limit: 255
    t.string   "payment_id",       limit: 255
    t.datetime "payment_date"
    t.string   "bank",             limit: 255
    t.integer  "hub_id",           limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "sender_name",      limit: 255
    t.string   "remark",           limit: 255
    t.datetime "paid_for_date"
  end

  add_index "transactions", ["hub_id"], name: "index_transactions_on_hub_id", using: :btree
  add_index "transactions", ["reciever_id"], name: "index_transactions_on_reciever_id", using: :btree
  add_index "transactions", ["sender_id"], name: "index_transactions_on_sender_id", using: :btree
  add_index "transactions", ["transaction_type"], name: "index_transactions_on_transaction_type", using: :btree

  create_table "user_profiles", force: :cascade do |t|
    t.datetime "date_of_joining"
    t.string   "personal_email",           limit: 255
    t.string   "first_name",               limit: 255
    t.string   "middle_name",              limit: 255
    t.string   "last_name",                limit: 255
    t.string   "alias",                    limit: 255
    t.datetime "date_of_birth"
    t.string   "gender",                   limit: 255
    t.string   "marital_status",           limit: 255
    t.string   "blood_group",              limit: 255
    t.string   "father_name",              limit: 255
    t.string   "father_number",            limit: 255
    t.text     "permanent_address",        limit: 65535
    t.string   "permanent_district",       limit: 255
    t.string   "permanent_city",           limit: 255
    t.string   "permanent_state",          limit: 255
    t.integer  "permanent_pincode",        limit: 4
    t.text     "current_address",          limit: 65535
    t.string   "current_district",         limit: 255
    t.string   "current_city",             limit: 255
    t.string   "current_state",            limit: 255
    t.integer  "current_pincode",          limit: 4
    t.string   "personal_number",          limit: 255
    t.string   "company_number",           limit: 255
    t.string   "vehicle_number",           limit: 255
    t.string   "dl_number",                limit: 255
    t.string   "rc_number",                limit: 255
    t.string   "emergency_contact_name",   limit: 255
    t.string   "emergency_contact_number", limit: 255
    t.string   "relation",                 limit: 255
    t.integer  "user_id",                  limit: 4,     null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.datetime "app_installed_on"
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               limit: 255,   default: "email",  null: false
    t.string   "uid",                    limit: 255,   default: "",       null: false
    t.string   "encrypted_password",     limit: 255,   default: "",       null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,        null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.string   "email",                  limit: 255
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_id",                limit: 4
    t.integer  "dispatch_center_id",     limit: 4
    t.string   "status",                 limit: 255,   default: "active"
    t.string   "code",                   limit: 255
    t.string   "reason_of_resignation",  limit: 255
    t.text     "remarks",                limit: 65535
    t.datetime "date_of_resignation"
    t.datetime "last_working_date"
    t.integer  "entity_id",              limit: 4
    t.string   "entity_type",            limit: 255
    t.string   "aasm_state",             limit: 255
    t.boolean  "active",                 limit: 1,     default: true
    t.integer  "vendor_id",              limit: 4
    t.string   "vendor_emp_id",          limit: 255
    t.string   "reffered_by",            limit: 255
    t.integer  "ctc",                    limit: 4
    t.string   "user_name",              limit: 255
    t.boolean  "is_login",               limit: 1,     default: false
  end

  add_index "users", ["code"], name: "index_users_on_code", using: :btree
  add_index "users", ["dispatch_center_id"], name: "users_dc", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree

  create_table "vendors", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "via_locations", force: :cascade do |t|
    t.integer  "route_id",       limit: 4
    t.integer  "sequence",       limit: 4
    t.boolean  "is_origin",      limit: 1
    t.boolean  "is_destination", limit: 1
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "location_id",    limit: 4
    t.string   "location_type",  limit: 255
  end

  add_index "via_locations", ["location_id", "location_type"], name: "location_index", using: :btree
  add_index "via_locations", ["route_id"], name: "index_via_locations_on_route_id", using: :btree

  create_table "wallets", force: :cascade do |t|
    t.integer  "entity_id",   limit: 4
    t.string   "entity_type", limit: 255
    t.integer  "amount",      limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "wallets", ["entity_id"], name: "index_wallets_on_entity_id", using: :btree

  add_foreign_key "asset_transitions", "assets"
  add_foreign_key "asset_transitions", "users"
end
