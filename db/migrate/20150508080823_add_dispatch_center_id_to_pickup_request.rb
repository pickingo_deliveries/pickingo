class AddDispatchCenterIdToPickupRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :dispatch_center_id, :integer
  end
end
