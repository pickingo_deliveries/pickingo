class RenamePincodeIn < ActiveRecord::Migration
  def change
    remove_column :sellers, :pincode
    add_column :sellers, :pincode_id, :integer
  end
end
