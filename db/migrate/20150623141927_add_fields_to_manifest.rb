class AddFieldsToManifest < ActiveRecord::Migration
  def change
    add_column :dispatch_center_manifests, :seal_number, :string
    add_column :hub_manifests, :seal_number, :string
  end
end
