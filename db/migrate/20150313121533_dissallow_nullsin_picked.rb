class DissallowNullsinPicked < ActiveRecord::Migration
  def change
    def change
      change_column :skus, :picked, :boolean, :null => false
    end
  end
end
