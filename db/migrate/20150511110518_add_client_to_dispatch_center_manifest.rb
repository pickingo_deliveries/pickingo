class AddClientToDispatchCenterManifest < ActiveRecord::Migration
  def change
    add_column :dispatch_center_manifests, :client_id, :integer
  end
end
