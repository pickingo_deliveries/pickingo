class AddStateToSellerManifests < ActiveRecord::Migration
  def change
     add_column :seller_dispatch_manifests, :state, :string
  end
end
