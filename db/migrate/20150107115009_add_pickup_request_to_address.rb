class AddPickupRequestToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :pickup_request_id, :integer
  end
end
