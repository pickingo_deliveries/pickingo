class AddRemarksToRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :remarks, :string
  end
end
