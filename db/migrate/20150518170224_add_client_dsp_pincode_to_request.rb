class AddClientDspPincodeToRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :client_dsp_pincode_id, :integer
  end
end
