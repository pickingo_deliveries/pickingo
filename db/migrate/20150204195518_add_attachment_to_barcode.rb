class AddAttachmentToBarcode < ActiveRecord::Migration
  def change
    add_attachment :barcode_sheets, :file
  end
end
