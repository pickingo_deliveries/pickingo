class CreateSellers < ActiveRecord::Migration
  def change
    create_table :sellers do |t|
      t.string :name
      t.text :address_line
      t.integer :pincode
      t.string :city
      t.string :country, :default => 'India'
      t.string :phone
      t.integer :pickup_request_id

      t.timestamps null: false
    end
  end
end
