class AddCancelledByToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :cancelled_by, :string, :default => ' '
  end
end
