class AddWeightToHubMasterManifest < ActiveRecord::Migration
  def change
    add_column :hub_master_manifests, :chargeable_weight, :decimal, :precision => 10, :scale => 2
    add_column :hub_master_manifests, :gross_weight, :decimal, :precision => 10, :scale => 2
  end
end
