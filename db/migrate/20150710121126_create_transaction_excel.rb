class CreateTransactionExcel < ActiveRecord::Migration
  def change
    create_table :transaction_excels do |t|
    	t.attachment :excel_file
    end
  end
end
