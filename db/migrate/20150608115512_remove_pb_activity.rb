class RemovePbActivity < ActiveRecord::Migration
  def change
    if ActiveRecord::Base.connection.table_exists?('pb_activities')
      drop_table :pb_activities
    end
  end
end
