class AddDispatchFlagToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :dispatch_flag, :boolean, :default => false
  end
end
