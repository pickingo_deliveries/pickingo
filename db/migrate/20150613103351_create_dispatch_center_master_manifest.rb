class CreateDispatchCenterMasterManifest < ActiveRecord::Migration
  def change
    create_table :dispatch_center_master_manifests do |t|
      t.integer  "origin_id"
      t.integer  "destination_id"
      t.string   "state"
      t.string   "uid"
      t.integer  "client_id"
      t.integer  "dc_vehicle_id"
      t.attachment :pdf_file
      t.attachment :excel_file

      t.timestamps null: false
    end
  end
end
