class AddInitialsToClient < ActiveRecord::Migration
  def change
    add_column :clients, :initials, :string
  end
end
