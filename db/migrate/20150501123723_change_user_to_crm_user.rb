class ChangeUserToCRMUser < ActiveRecord::Migration
  def change
    rename_column :comments, :user_id, :crm_user_id
  end
end
