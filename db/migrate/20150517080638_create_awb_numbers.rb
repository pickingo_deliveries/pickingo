class CreateAwbNumbers < ActiveRecord::Migration
  def change
    create_table :awb_numbers do |t|
      t.string :code
      t.integer :client_dsp_id
      t.boolean :used, :default => false

      t.timestamps null: false
    end
  end
end
