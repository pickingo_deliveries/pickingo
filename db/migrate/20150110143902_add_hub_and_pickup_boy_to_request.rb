class AddHubAndPickupBoyToRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :pickup_boy_id, :integer
    add_column :pickup_requests, :hub_id, :integer
  end
end
