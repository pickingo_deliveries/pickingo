class AddActiveFlagToPickupBoy < ActiveRecord::Migration
  def change
    add_column :pickup_boys, :active, :boolean, :default => true
  end
end
