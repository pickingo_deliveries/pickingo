class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :title
      t.string :description
      t.integer :owner_id
      t.integer :assignee_id
      t.integer :pickup_request_id
      t.integer :priority

      t.timestamps null: false
    end
  end
end
