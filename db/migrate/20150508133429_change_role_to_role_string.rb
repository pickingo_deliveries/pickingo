class ChangeRoleToRoleString < ActiveRecord::Migration
  def change
    rename_column :users, :role, :role_string
  end
end
