class ChangeDateToDateTime < ActiveRecord::Migration
  def change
  	change_column :transactions, :payment_date, :datetime
  	change_column :transactions, :paid_for_date, :datetime
  end
end
