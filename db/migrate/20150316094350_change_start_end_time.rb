class ChangeStartEndTime < ActiveRecord::Migration
  def change
    change_column :attendances, :start_time, :integer
    change_column :attendances, :end_time, :integer
  end
end
