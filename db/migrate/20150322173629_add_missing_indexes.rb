class AddMissingIndexes < ActiveRecord::Migration
  def change
    add_index :pickup_requests, :pickup_boy_id
    add_index :pickup_requests, :hub_id
    add_index :pickup_requests, :client_id
    add_index :client_customers, :client_id
    add_index :client_warehouses, :client_id
    add_index :generated_requests, :client_id
    add_index :barcode_sheets, :pickup_boy_id
    add_index :pickup_boys, :hub_id
    add_index :attendances, :pickup_boy_id
    add_index :users, :hub_id
    add_index :runsheets, :pickup_boy_id
    add_index :sms, :pickup_request_state_history_id
    add_index :pincodes, :hub_id
    add_index :pickup_request_state_histories, :pickup_request_id
  end
end
