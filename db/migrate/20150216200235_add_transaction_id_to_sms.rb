class AddTransactionIdToSms < ActiveRecord::Migration
  def change
    add_column :sms, :transaction_id, :integer
  end
end
