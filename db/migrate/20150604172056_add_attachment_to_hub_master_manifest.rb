class AddAttachmentToHubMasterManifest < ActiveRecord::Migration
  def change
    add_attachment :hub_master_manifests, :pdf_file
    add_attachment :hub_master_manifests, :excel_file
  end
end
