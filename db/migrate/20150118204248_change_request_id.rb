class ChangeRequestId < ActiveRecord::Migration
  def change
    change_column :pickup_requests, :client_request_id, :string
  end
end
