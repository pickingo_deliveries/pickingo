class ChangeUserDcColumn < ActiveRecord::Migration
  def change
    remove_column :users, :dc_owner
    add_column :users, :dispatch_center_id, :integer
  end
end
