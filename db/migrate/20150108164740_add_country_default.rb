class AddCountryDefault < ActiveRecord::Migration
  def change
    change_column :addresses, :country, :string, :default => 'India'
  end
end
