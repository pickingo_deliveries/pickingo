class AddClientIdToPickupRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :client_id, :integer
  end
end
