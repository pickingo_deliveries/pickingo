class AddPriorityToClientDspPincode < ActiveRecord::Migration
  def change
    add_column :client_dsp_pincodes, :priority, :integer
  end
end
