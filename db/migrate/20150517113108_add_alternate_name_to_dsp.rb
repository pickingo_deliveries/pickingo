class AddAlternateNameToDsp < ActiveRecord::Migration
  def change
    add_column :dsps, :alternate_name, :string
  end
end
