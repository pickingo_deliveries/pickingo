class AddColumnToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :formatted_address, :string
  end
end
