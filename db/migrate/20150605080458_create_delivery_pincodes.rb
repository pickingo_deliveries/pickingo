class CreateDeliveryPincodes < ActiveRecord::Migration
  def change
    create_table :delivery_pincodes do |t|
      t.integer :pincode_id
      t.integer :delivery_center_id
      t.string :delivery_center_type

      t.timestamps null: false
    end
  end
end
