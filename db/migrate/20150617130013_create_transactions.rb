class CreateTransactions < ActiveRecord::Migration
  def up
    create_table :transactions do |t|
    	t.string :expense_type
      t.string :expense_sub_type
      t.integer :from_id
      t.integer :to_id
      t.string :mod_of_payment
      t.string :amount
      t.string :transaction_type
      t.string :payment_id
      t.date :payment_date
      t.string :bank
      t.integer :hub_id
      t.integer :auth_id

      t.timestamps null: false
    end
  end

  def down
  	drop_table :transactions
  end
end
