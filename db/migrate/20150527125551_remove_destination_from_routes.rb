class RemoveDestinationFromRoutes < ActiveRecord::Migration
  def change
    remove_column :routes,:destination_id
    add_column :routes, :route_destination_id, :integer
    add_column :routes, :route_destination_type, :string
  end
end
