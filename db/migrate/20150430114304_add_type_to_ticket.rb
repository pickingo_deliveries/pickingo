class AddTypeToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :ticket_type_id, :integer
    add_column :tickets, :ticket_sub_type_id, :integer
  end
end
