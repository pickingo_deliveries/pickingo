class AddIndexOnTransaction < ActiveRecord::Migration
  def change
  	add_index :transactions, :sender_code
  	add_index :transactions, :reciever_code
  	add_index :transactions, :hub_id
  	add_index :transactions, :auth_id
  	add_index :transactions, :transaction_type

  end
end
