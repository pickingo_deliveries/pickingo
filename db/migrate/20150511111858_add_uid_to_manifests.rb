class AddUidToManifests < ActiveRecord::Migration
  def change
    add_column :hub_manifests, :uid, :string
    add_column :dispatch_center_manifests, :uid, :string
  end
end
