class CreateSellerDispatchManifestHistories < ActiveRecord::Migration
  def change
    create_table :seller_dispatch_manifest_histories do |t|
      t.string :state
      t.text :remarks
      t.integer :seller_dispatch_manifest_id

      t.timestamps null: false
    end
  end
end
