class CreateInventoryExcel < ActiveRecord::Migration
  def change
    create_table :inventory_excels do |t|
      t.attachment :excel_file
    end
  end
end
