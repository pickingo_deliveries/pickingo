class AddScheduledDateToPickupRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :scheduled_date, :date
  end
end
