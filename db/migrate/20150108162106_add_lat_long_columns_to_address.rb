class AddLatLongColumnsToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :lat, :float
    add_column :addresses, :lon, :float
    add_column :addresses, :locality, :string
    add_column :addresses, :sub_locality, :string
  end
end
