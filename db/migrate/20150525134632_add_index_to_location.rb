class AddIndexToLocation < ActiveRecord::Migration
  def change
    add_index :locations, :pincode
  end
end
