class CreatePbActivities < ActiveRecord::Migration
  def change
    unless table_exists? :pb_activities
    create_table :pb_activities do |t|

      t.timestamps null: false
    end
    end
  end
end
