class AddAttachmentToHubManifest < ActiveRecord::Migration
  def change
    add_attachment :hub_manifests, :pdf_file
    add_attachment :hub_manifests, :excel_file
  end
end
