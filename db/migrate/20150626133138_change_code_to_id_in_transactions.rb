class ChangeCodeToIdInTransactions < ActiveRecord::Migration
  def change
  	rename_column :transactions, :reciever_code, :reciever_id
  	change_column :transactions, :reciever_id, :string
  	rename_column :transactions, :sender_code, :sender_id
  	change_column :transactions, :sender_id, :string
  	remove_column :transactions, :auth_id
  end
end
