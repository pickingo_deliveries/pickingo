class AddTransitInfoToHubMasterManifest < ActiveRecord::Migration
  def change
    add_column :hub_master_manifests, :coloader_id, :integer
    add_column :hub_master_manifests, :transport_mode, :string
    add_column :hub_master_manifests, :tracking_id, :string
  end
end
