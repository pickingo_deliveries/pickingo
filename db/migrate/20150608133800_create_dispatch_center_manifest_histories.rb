class CreateDispatchCenterManifestHistories < ActiveRecord::Migration
  def change
    create_table :dispatch_center_manifest_histories do |t|
      t.string :state
      t.integer :dispatch_center_manifest_id
      t.text :remarks
      t.integer :dc_vehicle_id

      t.timestamps null: false
    end
  end
end
