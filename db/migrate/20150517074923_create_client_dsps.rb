class CreateClientDsps < ActiveRecord::Migration
  def change
    create_table :client_dsps do |t|
      t.integer :client_id
      t.integer :dsp_id
      t.integer :priority

      t.timestamps null: false
    end
  end
end
