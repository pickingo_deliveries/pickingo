class CreateSkus < ActiveRecord::Migration
  def change
    create_table :skus do |t|
      t.string :client_sku_id
      t.string :name
      t.integer :pickup_request_id

      t.timestamps null: false
    end
  end
end
