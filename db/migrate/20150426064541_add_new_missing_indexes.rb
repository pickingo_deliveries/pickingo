class AddNewMissingIndexes < ActiveRecord::Migration
  def change
    add_index :client_tokens, :client_customer_id
  end
end
