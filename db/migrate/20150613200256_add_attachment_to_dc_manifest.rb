class AddAttachmentToDcManifest < ActiveRecord::Migration
  def change
    add_attachment :dispatch_center_manifests, :pdf_file
    add_attachment :dispatch_center_manifests, :excel_file
  end
end
