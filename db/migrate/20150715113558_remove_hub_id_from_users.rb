class RemoveHubIdFromUsers < ActiveRecord::Migration
  def self.up
  	remove_column :users, :hub_id
  end

  def self.down
  	add_column :users, :hub_id, :integer
  end
end
