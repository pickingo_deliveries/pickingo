class CreateHubManifests < ActiveRecord::Migration
  def change
    create_table :hub_manifests do |t|
      t.integer :origin_id
      t.integer :destination_id
      t.integer :start_id
      t.integer :halt_id
      t.string :halt_type

      t.timestamps null: false
    end
  end
end
