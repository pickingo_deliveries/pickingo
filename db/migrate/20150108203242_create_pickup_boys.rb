class CreatePickupBoys < ActiveRecord::Migration
  def change
    create_table :pickup_boys do |t|
      t.string :name
      t.string :phone
      t.integer :hub_id

      t.timestamps null: false
    end
  end
end
