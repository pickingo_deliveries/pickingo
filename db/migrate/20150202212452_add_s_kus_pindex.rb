class AddSKusPindex < ActiveRecord::Migration
  def change
    add_index :skus, :pickup_request_id
  end
end
