class AddIdentifierToTicketType < ActiveRecord::Migration
  def change
    add_column :ticket_types, :identifier, :string
  end
end
