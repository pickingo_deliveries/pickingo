class ChangeCommentTypeInPickupRequestStateHistory < ActiveRecord::Migration
  def self.up
    change_column :pickup_request_state_histories, :comment, :text
  end

  def self.down
    change_column :pickup_request_state_histories, :comment, :string
  end
end
