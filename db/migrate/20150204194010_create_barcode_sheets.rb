class CreateBarcodeSheets < ActiveRecord::Migration
  def change
    create_table :barcode_sheets do |t|
      t.integer :pickup_boy_id

      t.timestamps null: false
    end
  end
end
