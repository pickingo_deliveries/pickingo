class CreatePickupRequestStateHistories < ActiveRecord::Migration
  def change
    create_table :pickup_request_state_histories do |t|
      t.string :state
      t.integer :pickup_request_id

      t.timestamps null: false
    end
  end
end
