class AddCommentToPickupRequestStateHistory < ActiveRecord::Migration
  def change
    add_column :pickup_request_state_histories, :comment, :string
  end
end
