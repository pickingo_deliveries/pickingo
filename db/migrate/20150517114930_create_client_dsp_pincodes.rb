class CreateClientDspPincodes < ActiveRecord::Migration
  def change
    create_table :client_dsp_pincodes do |t|
      t.integer :client_id
      t.integer :dap_id
      t.integer :pincode_id

      t.timestamps null: false
    end
  end
end
