class AddCallTypeInTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :call_type, :string, :default => ''
  end
end
