class AddLbhwToDispatchManifest < ActiveRecord::Migration
  def change
    add_column :dispatch_center_manifests, :length, :decimal
    add_column :dispatch_center_manifests, :breadth, :decimal
    add_column :dispatch_center_manifests, :height, :decimal
    add_column :dispatch_center_manifests, :weight, :decimal
  end
end
