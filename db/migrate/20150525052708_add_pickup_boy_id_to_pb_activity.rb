class AddPickupBoyIdToPbActivity < ActiveRecord::Migration
  def change
    unless column_exists? :pb_activities, :pickup_boy_id
      add_column :pb_activities, :pickup_boy_id, :integer
    end

  end
end
