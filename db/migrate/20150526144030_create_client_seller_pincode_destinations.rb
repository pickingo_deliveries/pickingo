class CreateClientSellerPincodeDestinations < ActiveRecord::Migration
  def change
    create_table :client_seller_pincode_destinations do |t|
      t.integer :client_id
      t.integer :pincode_id
      t.integer :destination_id
      t.string :destination_type

      t.timestamps null: false
    end
  end
end
