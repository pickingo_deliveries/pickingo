class AddUploadIndexes < ActiveRecord::Migration
  def change
    add_index :client_dsp_pincodes, :client_dsp_id
    add_index :client_dsp_pincodes, :pincode_id
    add_index :client_dsps, :client_id
    add_index :client_dsps, :dsp_id
    add_index :client_dsps, [:client_id, :dsp_id]
    add_index :awb_numbers, :client_dsp_id
    add_index :sellers, :pickup_request_id
  end
end
