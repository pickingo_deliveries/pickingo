class CreateSellerDispatchManifests < ActiveRecord::Migration
  def change
    create_table :seller_dispatch_manifests do |t|
      t.integer :origin_id
      t.string :tracking_id
      t.integer :dsp_id

      t.timestamps null: false
    end
  end
end
