class CreateUserProfile < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.datetime :date_of_joining
      t.string   :personal_email
      t.string   :first_name
      t.string   :middle_name
      t.string   :last_name
      t.string   :alias
      t.datetime :date_of_birth
      t.string   :gender
      t.string   :marital_status
      t.string   :blood_group
      t.string   :father_name
      t.string   :father_number
      t.text     :permanent_address
      t.string   :permanent_district
      t.string   :permanent_city
      t.string   :permanent_state
      t.integer  :permanent_pincode
      t.text     :current_address
      t.string   :current_district
      t.string   :current_city
      t.string   :current_state
      t.integer  :current_pincode
      t.string   :personal_number
      t.string   :company_number
      t.string   :vehicle_number
      t.string   :dl_number
      t.string   :rc_number
      t.string   :emergency_contact_name
      t.string   :emergency_contact_number
      t.string   :relation
      t.string   :vendor
      t.integer  :user_id, null: false

      t.timestamps null: false
    end
  end
end
