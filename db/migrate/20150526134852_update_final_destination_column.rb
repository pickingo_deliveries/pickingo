class UpdateFinalDestinationColumn < ActiveRecord::Migration
  def change
    remove_column :pickup_requests, :dispatch_center_id
    add_column :pickup_requests, :final_destination_id, :integer
    add_column :pickup_requests, :final_destination_type, :string
  end
end
