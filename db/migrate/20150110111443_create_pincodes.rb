class CreatePincodes < ActiveRecord::Migration
  def change
    create_table :pincodes do |t|
      t.string :code
      t.integer :hub_id

      t.timestamps null: false
    end
  end
end
