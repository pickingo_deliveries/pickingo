class AddAssignedToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :assigned_to_id, :integer
  end
end
