class CreatePickupRequests < ActiveRecord::Migration
  def change
    create_table :pickup_requests do |t|

      t.timestamps null: false
    end
  end
end
