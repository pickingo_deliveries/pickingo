class RemoveStartEndFromHubManifest < ActiveRecord::Migration
  def change
    remove_column :hub_manifests, :start_id
    remove_column :hub_manifests, :halt_id
  end
end
