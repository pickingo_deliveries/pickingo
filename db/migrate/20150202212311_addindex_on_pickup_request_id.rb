class AddindexOnPickupRequestId < ActiveRecord::Migration
  def change
    add_index :addresses, :pickup_request_id
  end
end
