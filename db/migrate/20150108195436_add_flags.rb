class AddFlags < ActiveRecord::Migration
  def change
    add_column :addresses, :is_ambiguous, :boolean, :default => false
    add_column :addresses, :is_manual, :boolean, :default => false
  end
end
