class AddVehicleToDcManifest < ActiveRecord::Migration
  def change
    add_column :dispatch_center_manifests, :dc_vehicle_id, :integer
  end
end
