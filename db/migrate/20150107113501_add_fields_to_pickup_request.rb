class AddFieldsToPickupRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :client_order_number, :integer
    add_column :pickup_requests, :client_request_id, :integer
    add_column :pickup_requests, :preferred_location, :string
    add_column :pickup_requests, :preferred_date, :date
    add_column :pickup_requests, :preferred_time, :string
    add_column :pickup_requests, :status, :string
    add_column :pickup_requests, :request_type, :string
  end
end
