class AddAttachmentStartPicToPbActivities < ActiveRecord::Migration
  def self.up
    unless column_exists? :pb_activities, :start_pic_file_name
    change_table :pb_activities do |t|
      t.attachment :start_pic
    end
    end

  end

  def self.down
    remove_attachment :pb_activities, :start_pic
  end
end
