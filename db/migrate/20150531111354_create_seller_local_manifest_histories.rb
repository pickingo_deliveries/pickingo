class CreateSellerLocalManifestHistories < ActiveRecord::Migration
  def change
    create_table :seller_local_manifest_histories do |t|
      t.integer :seller_local_manifest_id
      t.string :state
      t.text :remarks

      t.timestamps null: false
    end
  end
end
