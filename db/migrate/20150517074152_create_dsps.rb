class CreateDsps < ActiveRecord::Migration
  def change
    create_table :dsps do |t|
      t.string :name
      t.string :identifier

      t.timestamps null: false
    end
  end
end
