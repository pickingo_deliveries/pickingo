class AddAddresslineToHub < ActiveRecord::Migration
  def change
    add_column :hubs, :address_line, :string
    add_column :hubs, :city, :string
    remove_column :hubs, :address_id
  end
end
