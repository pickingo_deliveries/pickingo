class AddSellerManifestsToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :seller_local_manifest_id, :integer
    add_column :pickup_requests, :seller_dispatch_manifest_id, :integer
  end
end
