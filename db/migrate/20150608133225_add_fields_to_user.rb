class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :status, :string, default: 'active'
    add_column :users, :code, :string
    add_column :users, :reason_of_resignation, :string
    add_column :users, :remarks, :text
    add_column :users, :date_of_resignation, :datetime
    add_column :users, :last_working_date, :datetime
    add_reference :users, :entity, polymorphic: true
    add_column :users, :aasm_state, :string
    add_column :users, :active, :boolean, default: true
    remove_column :users, :name, :string
    remove_column :users, :nickname, :string
    remove_column :users, :image, :string
    remove_column :users, :role_string, :string
  end
end
