class AddStateToDispatchCenterManifest < ActiveRecord::Migration
  def change
    add_column :dispatch_center_manifests, :state, :string
  end
end
