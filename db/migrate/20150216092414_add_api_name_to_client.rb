class AddAPINameToClient < ActiveRecord::Migration
  def change
    add_column :clients, :api_name, :string
  end
end
