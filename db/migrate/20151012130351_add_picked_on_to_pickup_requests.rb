class AddPickedOnToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :picked_on, :datetime
  end
end
