class RemoveCheckinFromPickupBoy < ActiveRecord::Migration
  def change
    remove_column :pickup_boys, :check_in_time, :time
    remove_column :pickup_boys, :check_out_time, :time
  end
end
