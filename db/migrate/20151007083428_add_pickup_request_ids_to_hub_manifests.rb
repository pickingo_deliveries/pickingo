class AddPickupRequestIdsToHubManifests < ActiveRecord::Migration
  def change
    add_column :hub_manifests, :request_ids, :string,array: true, default: []
  end
end
