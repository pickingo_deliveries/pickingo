class AddDepartmentToRole < ActiveRecord::Migration
  def change
    add_column :roles, :department_id, :integer
  end
end
