class RemoveVendorFromUserProfile < ActiveRecord::Migration
  def change
    remove_column :user_profiles, :vendor, :string
  end
end
