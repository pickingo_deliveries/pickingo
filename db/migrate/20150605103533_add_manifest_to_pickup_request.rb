class AddManifestToPickupRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :manifest_id, :integer
    add_column :pickup_requests, :manifest_type, :string
  end
end
