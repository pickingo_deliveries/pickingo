class AddDestinationTypeToHubManifest < ActiveRecord::Migration
  def change
    add_column :hub_manifests, :destination_type, :string
  end
end
