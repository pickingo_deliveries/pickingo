class CreateViaLocations < ActiveRecord::Migration
  def change
    create_table :via_locations do |t|
      t.integer :route_id
      t.integer :sequence
      t.boolean :is_origin
      t.boolean :is_destination

      t.timestamps null: false
    end
  end
end
