class AddCRMIndexes < ActiveRecord::Migration
  def change
    add_index :tickets, :ticket_type_id
    add_index :tickets, :ticket_sub_type_id
    add_index :tickets, :owner_id
    add_index :tickets, :assignee_id
    add_index :tickets, :assigned_to_id
    add_index :tickets, :pickup_request_id
    add_index :roles, :department_id
    add_index :ticket_sub_types, :ticket_type_id
    add_index :ticket_types, :department_id
    add_index :crm_users, :role_id
    add_index :crm_users, :department_id
  end
end
