class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :pincode
      t.string :sub_string
      t.string :sub_locality
      t.string :locality

      t.timestamps null: false
    end
  end
end
