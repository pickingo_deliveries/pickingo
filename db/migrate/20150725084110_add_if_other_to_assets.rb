class AddIfOtherToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :if_other, :text
  end
end
