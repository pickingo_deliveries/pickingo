class ChangeNull < ActiveRecord::Migration
  def change
    change_column_null :pickup_requests, :weight, false, 0
    change_column_null :pickup_requests, :length, false, 0
    change_column_null :pickup_requests, :breadth, false, 0
    change_column_null :pickup_requests, :height, false, 0
  end
end
