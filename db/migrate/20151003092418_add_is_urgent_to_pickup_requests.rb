class AddIsUrgentToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :is_urgent, :integer,:default => 0
  end
end
