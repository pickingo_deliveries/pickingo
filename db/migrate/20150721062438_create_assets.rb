class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :asset_type
      t.string :serial_no
      t.string :reference_no
      t.integer :entity_id
      t.string :entity_type

      t.timestamps null: false
    end
  end
end
