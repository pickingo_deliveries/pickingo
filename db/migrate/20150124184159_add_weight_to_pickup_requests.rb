class AddWeightToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :weight, :decimal
    add_column :pickup_requests, :length, :decimal
    add_column :pickup_requests, :breadth, :decimal
    add_column :pickup_requests, :height, :decimal
  end
end
