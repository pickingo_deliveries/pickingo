class AddAttachmentToSellerDispatchManifest < ActiveRecord::Migration
  def change
    add_attachment :seller_dispatch_manifests, :pdf_file
    add_attachment :seller_dispatch_manifests, :excel_file
  end
end
