class AddCheckInTimeToPickupBoy < ActiveRecord::Migration
  def change
    add_column :pickup_boys, :check_in_time, :Time
  end
end
