class AddManifestToHistory < ActiveRecord::Migration
  def change
    add_column :pickup_request_state_histories, :manifest_id, :integer
    add_column :pickup_request_state_histories, :manifest_type, :string
  end
end
