class AddDefaultToDec < ActiveRecord::Migration
  def change
    change_column :hubs, :dec, :boolean, :default => false
  end
end
