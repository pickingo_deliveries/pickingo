class CreateClientPincodeDestinations < ActiveRecord::Migration
  def change
    create_table :client_pincode_destinations do |t|
      t.integer :pincode_id
      t.integer :client_id
      t.integer :dispatch_center_id

      t.timestamps null: false
    end
  end
end
