class AddColumnPickedToSkus < ActiveRecord::Migration
  def change
    add_column :skus, :picked, :boolean, :default => false
  end
end
