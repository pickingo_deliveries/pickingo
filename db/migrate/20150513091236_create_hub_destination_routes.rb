class CreateHubDestinationRoutes < ActiveRecord::Migration
  def change
    create_table :hub_destination_routes do |t|
      t.integer :hub_id
      t.integer :destination_id
      t.integer :route_id

      t.timestamps null: false
    end
  end
end
