class AddHubIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :hub_id, :integer
  end
end
