class CreateHubMasterManifests < ActiveRecord::Migration
  def change
    create_table :hub_master_manifests do |t|
      t.integer :origin_id
      t.integer :destination_id
      t.string :destination_type

      t.timestamps null: false
    end
  end
end
