class RemoveHaltTypeFromHubManifest < ActiveRecord::Migration
  def change
    remove_column :hub_manifests, :halt_type
  end
end
