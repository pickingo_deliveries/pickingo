class AddBarcodeToRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :barcode, :string
  end
end
