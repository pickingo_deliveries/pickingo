class RenameClientDspPincode < ActiveRecord::Migration
  def change
    rename_column :pickup_requests, :client_dsp_pincode_id, :dsp_id
  end
end
