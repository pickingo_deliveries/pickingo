class ChangeColumsnToText < ActiveRecord::Migration
  def change
    change_column :skus, :name, :text
    change_column :addresses, :address_line, :text

  end
end
