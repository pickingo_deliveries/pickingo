class CreateHubManifestHistories < ActiveRecord::Migration
  def change
    create_table :hub_manifest_histories do |t|
      t.string :state
      t.integer :hub_manifest_id
      t.string :remarks

      t.timestamps null: false
    end
  end
end
