class AddCheckOutTimeToPickupBoy < ActiveRecord::Migration
  def change
    add_column :pickup_boys, :check_out_time, :Time
  end
end
