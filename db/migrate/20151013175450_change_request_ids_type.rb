class ChangeRequestIdsType < ActiveRecord::Migration
  def change
    change_column_default :hub_manifests, :request_ids, nil
    change_column :hub_manifests, :request_ids, :text, array: true
    change_column_default :dispatch_center_manifests, :request_ids, nil
    change_column :dispatch_center_manifests, :request_ids, :text, array: true
  end
end
