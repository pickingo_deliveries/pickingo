class ChangePincodeToInteger < ActiveRecord::Migration
  def change
    change_column :addresses, :pincode, :integer
  end
end
