class AddStateToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :aasm_state, :string
  end
end
