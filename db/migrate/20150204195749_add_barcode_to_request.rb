class AddBarcodeToRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :barcode_sheet_id, :integer
  end
end
