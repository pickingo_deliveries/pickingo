class ChangeClientRequestId < ActiveRecord::Migration
  def change
    change_column :pickup_requests, :client_order_number, :string
  end
end
