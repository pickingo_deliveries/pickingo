class DisallowNil < ActiveRecord::Migration
  def change
    change_column :pickup_requests, :weight, :decimal, :precision => 10, :scale => 3,:default => 0, :nil => false
    change_column :pickup_requests, :length, :decimal, :precision => 10, :scale => 3,:default => 0, :nil => false
    change_column :pickup_requests, :breadth, :decimal,:precision => 10, :scale => 3, :default => 0, :nil => false
    change_column :pickup_requests, :height, :decimal, :precision => 10, :scale => 3,:default => 0, :nil => false
  end
end
