class AddCurrentLocationIdAndCurrentLocationTypeToPickupRequestStateHistories < ActiveRecord::Migration
  def change
    add_column :pickup_request_state_histories, :current_location_id, :integer
    add_column :pickup_request_state_histories, :current_location_type, :string
  end
end
