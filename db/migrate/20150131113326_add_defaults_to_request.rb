class AddDefaultsToRequest < ActiveRecord::Migration
  def change
    change_column :pickup_requests, :weight, :decimal,:default => 0
    change_column :pickup_requests, :length, :decimal,:default => 0
    change_column :pickup_requests, :breadth, :decimal,:default => 0
    change_column :pickup_requests, :height, :decimal,:default => 0
  end
end
