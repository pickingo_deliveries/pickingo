class ChangeDescription < ActiveRecord::Migration
  def change
    change_column :tickets, :description, :text
  end
end
