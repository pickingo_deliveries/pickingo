class CreateAssetTransitions < ActiveRecord::Migration
  def change
    create_table :asset_transitions do |t|
      t.references :asset, index: true
      t.references :user, index: true
      t.string :state
      t.date :assign_date
      t.date :return_date
      t.integer :quantity
      t.text :remarks

      t.timestamps null: false
    end
    add_foreign_key :asset_transitions, :assets
    add_foreign_key :asset_transitions, :users
  end
end
