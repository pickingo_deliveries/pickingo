class AddDepartmentToCRMUser < ActiveRecord::Migration
  def change
    add_column :crm_users, :department_id, :integer
  end
end
