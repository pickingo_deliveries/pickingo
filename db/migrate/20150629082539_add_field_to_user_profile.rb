class AddFieldToUserProfile < ActiveRecord::Migration
  def change
    add_column :user_profiles, :app_installed_on, :datetime
  end
end
