class RemoveValidationFromRequest < ActiveRecord::Migration
  def change
    change_column :pickup_requests, :length, :decimal, :precision => 10, :scale => 3, :default => 0.0, :null => true
    change_column :pickup_requests, :breadth, :decimal, :precision => 10, :scale => 3, :default => 0.0, :null => true
    change_column :pickup_requests, :height, :decimal, :precision => 10, :scale => 3, :default => 0.0, :null => true
  end
end
