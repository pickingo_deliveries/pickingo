class AddEvenMoreIndexes < ActiveRecord::Migration
  def change
    add_index :hub_manifests, :origin_id
    add_index :hub_manifests, [:destination_id, :destination_type], :name => 'manifest_destination_index'
    add_index :hub_manifests, :hub_master_manifest_id, name:'hm_hmm_index'
    add_index :pickup_requests, :dispatch_center_id, name:'pur_dc_index'
    add_index :routes, :origin_id
    add_index :routes, :destination_id
    add_index :via_locations, :route_id
    add_index :via_locations, [:location_id, :location_type], :name => 'location_index'
    add_index :hub_manifest_histories, :hub_manifest_id
  end
end
