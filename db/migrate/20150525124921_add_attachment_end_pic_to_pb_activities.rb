class AddAttachmentEndPicToPbActivities < ActiveRecord::Migration
  def self.up
    unless column_exists? :pb_activities,  :end_pic_file_name
    change_table :pb_activities do |t|
      t.attachment :end_pic
    end
    end

  end

  def self.down
    remove_attachment :pb_activities, :end_pic
  end
end
