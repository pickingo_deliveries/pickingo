class CreateSellerSlips < ActiveRecord::Migration
  def change
    create_table :seller_slips do |t|
      t.integer :pickup_request_id
      t.attachment :slip

      t.timestamps null: false
    end
  end
end
