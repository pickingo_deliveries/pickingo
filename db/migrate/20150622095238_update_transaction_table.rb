class UpdateTransactionTable < ActiveRecord::Migration
  def change
  	change_column :transactions, :to_id, :string
  	rename_column :transactions, :to_id, :reciever_code
  	rename_column :transactions, :from_id, :sender_code
  	change_column :transactions, :sender_code, :string
  	add_column :transactions, :sender_name, :string
  	add_column :transactions, :remark, :string
  	add_column :transactions, :paid_for_date, :date
  end
end
