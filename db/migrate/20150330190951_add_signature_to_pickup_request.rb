class AddSignatureToPickupRequest < ActiveRecord::Migration
  def change
    add_attachment :pickup_requests, :signature
  end
end
