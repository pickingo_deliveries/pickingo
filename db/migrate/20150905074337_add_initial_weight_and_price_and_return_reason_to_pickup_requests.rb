class AddInitialWeightAndPriceAndReturnReasonToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :initial_weight, :decimal,:precision => 10, :scale => 2
    add_column :pickup_requests, :price, :decimal,:precision => 10, :scale => 2
    add_column :pickup_requests, :return_reason, :text
  end
end
