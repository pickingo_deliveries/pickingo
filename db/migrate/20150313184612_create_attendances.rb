class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.integer :pickup_boy_id
      t.date :date
      t.integer :start_km
      t.integer :end_km
      t.time :start_time
      t.time :end_time
      t.string :status

      t.timestamps null: false
    end

    add_index :attendances, :date
  end
end
