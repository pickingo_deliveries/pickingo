class AddDimentionToHubManifest < ActiveRecord::Migration
  def change
    add_column :hub_manifests, :weight, :decimal
    add_column :hub_manifests, :length, :decimal
    add_column :hub_manifests, :breadth, :decimal
    add_column :hub_manifests, :height, :decimal
  end
end
