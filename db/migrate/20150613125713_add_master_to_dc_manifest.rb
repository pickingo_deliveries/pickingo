class AddMasterToDcManifest < ActiveRecord::Migration
  def change
    add_column :dispatch_center_manifests, :dispatch_center_master_manifest_id, :integer
  end
end
