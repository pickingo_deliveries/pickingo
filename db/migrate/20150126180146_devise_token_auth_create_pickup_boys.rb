class DeviseTokenAuthCreatePickupBoys < ActiveRecord::Migration
  def change
      ## Required
      add_column :pickup_boys, :provider,:string, :null => false,:default => "email"
      add_column :pickup_boys, :uid, :string,:null => false, :default => ""

      ## Database authenticatable
      add_column :pickup_boys, :encrypted_password, :string, :null => false, :default => ""

      ## Recoverable
      add_column :pickup_boys,   :reset_password_token, :string
      add_column :pickup_boys, :reset_password_sent_at, :string

      ## Rememberable
      add_column :pickup_boys, :remember_created_at, :datetime

      ## Trackable
      add_column :pickup_boys,:sign_in_count,:integer, :default => 0, :null => false
      add_column :pickup_boys, :current_sign_in_at, :datetime
      add_column :pickup_boys, :last_sign_in_at, :datetime
      add_column :pickup_boys,   :current_sign_in_ip, :string
      add_column :pickup_boys,   :last_sign_in_ip, :string

      add_column :pickup_boys,   :confirmation_token, :string
      add_column :pickup_boys,   :unconfirmed_email, :string
      add_column :pickup_boys, :confirmed_at, :datetime
      add_column :pickup_boys, :confirmation_sent_at, :datetime


      ## Lockable
      # t.integer  :failed_attempts, :default => 0, :null => false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at

      ## User Info
      add_column :pickup_boys,   :nickname, :string
      add_column :pickup_boys,   :image, :string
      add_column :pickup_boys,   :email, :string

      ## Tokens
      add_column :pickup_boys, :tokens, :text

    add_index :pickup_boys, :email
    add_index :pickup_boys, [:uid, :provider],     :unique => true
    add_index :pickup_boys, :reset_password_token, :unique => true
    # add_index :pickup_boys, :confirmation_token,   :unique => true
    # add_index :pickup_boys, :unlock_token,         :unique => true
  end
end
