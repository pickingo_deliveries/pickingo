class AddDefault < ActiveRecord::Migration
  def change
    change_column :routes, :default, :string, :default => true
  end
end
