class AddRunsheetIdToPickupRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :runsheet_id, :integer
  end
end
