class AddUserId < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :user_id, :integer
    add_column :pickup_request_state_histories, :user_id, :integer
    add_column :runsheets, :user_id, :integer
    add_column :barcode_sheets, :user_id, :integer
    add_column :attendances, :user_id, :integer
  end
end
