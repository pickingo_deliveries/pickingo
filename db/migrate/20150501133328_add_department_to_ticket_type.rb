class AddDepartmentToTicketType < ActiveRecord::Migration
  def change
    add_column :ticket_types, :department_id, :integer
  end
end
