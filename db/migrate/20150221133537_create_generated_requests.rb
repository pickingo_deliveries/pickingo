class CreateGeneratedRequests < ActiveRecord::Migration
  def change
    create_table :generated_requests do |t|
      t.string :code
      t.integer :client_id

      t.timestamps null: false
    end
  end
end
