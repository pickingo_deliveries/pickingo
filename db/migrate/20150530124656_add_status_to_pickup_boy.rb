class AddStatusToPickupBoy < ActiveRecord::Migration
  def self.up
    add_column :pickup_boys, :aasm_state, :string
  end

  def self.down
    remove_column :pickup_boys, :aasm_state
  end
end
