class CreateClientWarehouses < ActiveRecord::Migration
  def change
    create_table :client_warehouses do |t|
      t.string :name
      t.string :address_line
      t.string :pincode
      t.integer :client_id

      t.timestamps null: false
    end
  end
end
