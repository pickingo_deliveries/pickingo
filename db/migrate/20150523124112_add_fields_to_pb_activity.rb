class AddFieldsToPbActivity < ActiveRecord::Migration
  def change

    unless column_exists? :pb_activities , :activity_date
      add_column :pb_activities, :activity_date, :date
    end
    unless column_exists? :pb_activities , :check_in_time
      add_column :pb_activities, :check_in_time, :timestamp
    end
    unless column_exists? :pb_activities , :check_out_time
      add_column :pb_activities, :check_out_time, :timestamp
    end

  end
end
