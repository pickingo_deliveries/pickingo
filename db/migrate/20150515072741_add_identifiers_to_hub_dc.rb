class AddIdentifiersToHubDc < ActiveRecord::Migration
  def change
    add_column :hubs , :identifier, :string
    add_column :dispatch_centers , :identifier, :string
  end
end
