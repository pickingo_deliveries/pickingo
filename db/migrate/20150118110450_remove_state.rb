class RemoveState < ActiveRecord::Migration
  def change
    remove_column :pickup_requests, :status
  end
end
