class AddViaAPIToPickupRequests < ActiveRecord::Migration
  def change
    add_column :pickup_requests, :via_api, :boolean, :default => false
  end
end
