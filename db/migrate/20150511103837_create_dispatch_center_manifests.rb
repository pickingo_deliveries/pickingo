class CreateDispatchCenterManifests < ActiveRecord::Migration
  def change
    create_table :dispatch_center_manifests do |t|
      t.integer :origin_id
      t.integer :destination_id
      t.integer :start_id
      t.integer :halt_id

      t.timestamps null: false
    end
  end
end
