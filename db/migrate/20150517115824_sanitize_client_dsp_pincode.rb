class SanitizeClientDspPincode < ActiveRecord::Migration
  def change
    remove_column :client_dsp_pincodes, :client_id
    remove_column :client_dsp_pincodes, :dap_id
    add_column :client_dsp_pincodes, :client_dsp_id, :integer
  end
end
