class AddVendorToUser < ActiveRecord::Migration
  def change
    add_column :users, :vendor_id, :integer
    add_column :users, :vendor_emp_id, :string
  end
end
