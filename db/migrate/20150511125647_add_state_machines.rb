class AddStateMachines < ActiveRecord::Migration
  def change
    add_column :hub_manifests, :state, :string
    add_column :hub_master_manifests, :state, :string
    add_column :hub_manifests, :hub_master_manifest_id, :integer
  end
end
