class CreateClientTokens < ActiveRecord::Migration
  def change
    create_table :client_tokens do |t|
      t.string :access_token
      t.integer :client_customer_id

      t.timestamps null: false
    end
  end
end
