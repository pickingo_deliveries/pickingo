class CreateColoader < ActiveRecord::Migration
  def change
    create_table :coloaders do |t|
      t.string :name
      t.string :identifier

      t.timestamps null: false
    end
  end
end
