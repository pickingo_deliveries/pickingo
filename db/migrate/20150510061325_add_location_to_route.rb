class AddLocationToRoute < ActiveRecord::Migration
  def change
    add_column :via_locations, :location_id, :integer
    add_column :via_locations, :location_type, :string
  end
end
