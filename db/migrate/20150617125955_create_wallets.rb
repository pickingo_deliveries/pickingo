class CreateWallets < ActiveRecord::Migration
  def up
    create_table :wallets do |t|
    	t.integer :entity_id
      t.string :entity_type
    	t.string :amount

      t.timestamps null: false
    end
    add_index :wallets, :entity_id
  end

  def down
  	drop_table :wallets
  end
end
