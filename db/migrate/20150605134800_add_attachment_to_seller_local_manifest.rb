class AddAttachmentToSellerLocalManifest < ActiveRecord::Migration
  def change
    add_attachment :seller_local_manifests, :pdf_file
    add_attachment :seller_local_manifests, :excel_file
  end
end
