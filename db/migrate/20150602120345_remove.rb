class Remove < ActiveRecord::Migration
  def change
    remove_column :dispatch_center_manifests, :start_id
    remove_column :dispatch_center_manifests, :halt_id
  end
end
