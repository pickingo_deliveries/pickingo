class AddMissingFields < ActiveRecord::Migration
  def change
    add_column :seller_local_manifests, :length, :decimal
    add_column :seller_local_manifests, :breadth, :decimal
    add_column :seller_local_manifests, :height, :decimal
    add_column :seller_local_manifests, :weight, :decimal
    add_column :seller_local_manifests, :origin_type, :string

    add_column :seller_dispatch_manifests, :length, :decimal
    add_column :seller_dispatch_manifests, :breadth, :decimal
    add_column :seller_dispatch_manifests, :height, :decimal
    add_column :seller_dispatch_manifests, :weight, :decimal
    add_column :seller_dispatch_manifests, :origin_type, :string
  end
end
