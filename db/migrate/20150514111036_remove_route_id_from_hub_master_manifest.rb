class RemoveRouteIdFromHubMasterManifest < ActiveRecord::Migration
  def change
    remove_column :hub_master_manifests, :route_id
  end
end
