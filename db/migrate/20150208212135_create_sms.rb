class CreateSms < ActiveRecord::Migration
  def change
    create_table :sms do |t|
      t.string :phone
      t.string :content
      t.integer :pickup_request_state_history_id
      t.boolean :sent, :default => false

      t.timestamps null: false
    end
  end
end
