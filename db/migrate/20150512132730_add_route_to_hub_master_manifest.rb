class AddRouteToHubMasterManifest < ActiveRecord::Migration
  def change
    add_column :hub_master_manifests, :route_id, :integer
  end
end
