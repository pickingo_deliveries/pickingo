class AddFieldsToAttendance < ActiveRecord::Migration
  def change
    add_column :attendances, :check_in_time, :datetime
    add_column :attendances, :check_out_time, :datetime
    add_attachment :attendances, :start_pic
    add_attachment :attendances, :end_pic
    change_column :attendances, :start_km, :float
    change_column :attendances, :end_km, :float
  end
end
