class AddPickupRequestIdsToDispatchCenterManifests < ActiveRecord::Migration
  def change
    add_column :dispatch_center_manifests, :request_ids, :string,array: true, default: []
  end
end
