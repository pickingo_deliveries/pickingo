class CreateRunsheets < ActiveRecord::Migration
  def change
    create_table :runsheets do |t|
      t.integer :pickup_boy_id

      t.timestamps null: false
    end
  end
end
