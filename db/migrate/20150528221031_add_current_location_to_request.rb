class AddCurrentLocationToRequest < ActiveRecord::Migration
  def change
    add_column :pickup_requests,:current_location_id,:integer
    add_column :pickup_requests,:current_location_type,:string
  end
end
