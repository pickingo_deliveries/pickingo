class CreateBundledSlips < ActiveRecord::Migration
  def change
    create_table :bundled_slips do |t|
      t.attachment :slip

      t.timestamps null: false
    end
  end
end
