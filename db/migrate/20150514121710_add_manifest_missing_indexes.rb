class AddManifestMissingIndexes < ActiveRecord::Migration
  def change
    add_index :users, :role_id
    add_index :hub_destination_routes, :hub_id
    add_index :hub_destination_routes, :destination_id
    add_index :hub_destination_routes, :route_id
    add_index :hub_master_manifests, :origin_id, :name => 'origin_index'
    add_index :hub_master_manifests, [:destination_id, :destination_type], :name => 'master_destination_index'
    add_index :hub_master_manifest_histories, :hub_master_manifest_id, :name => 'histories_index'
    add_index :dispatch_center_manifests, :origin_id , :name => 'dc_origin_index'
    add_index :dispatch_center_manifests, :destination_id, :name => 'dc_destination_index'
    add_index :dispatch_center_manifests, :start_id, :name => 'dc_start_index'
    add_index :dispatch_center_manifests, :halt_id, :name => 'dc_halt_index'
    add_index :dispatch_center_manifests, :client_id, :name => 'dc_client_index'
    add_index :client_pincode_destinations, :client_id, :name => 'cpd_client_index'
    add_index :client_pincode_destinations, :pincode_id, :name => 'cpd_pincode_index'
    add_index :client_pincode_destinations, :dispatch_center_id, :name => 'cpd_dc_index'
  end
end
