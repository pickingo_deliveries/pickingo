class CreateTicketSubTypes < ActiveRecord::Migration
  def change
    create_table :ticket_sub_types do |t|
      t.string :name
      t.string :display_name
      t.integer :ticket_type_id

      t.timestamps null: false
    end
  end
end
