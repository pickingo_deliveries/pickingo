class CreateDcVehicles < ActiveRecord::Migration
  def change
    create_table :dc_vehicles do |t|
      t.integer :dispatch_center_id
      t.string :reg_number
      t.string :vehicle_model
      t.string :driver_name
      t.string :driver_phone

      t.timestamps null: false
    end
  end
end
