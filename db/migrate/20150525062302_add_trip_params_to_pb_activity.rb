class AddTripParamsToPbActivity < ActiveRecord::Migration
  def change
    unless column_exists? :pb_activities, :start_km
    add_column :pb_activities, :start_km, :float
    end
    unless column_exists? :pb_activities, :end_km
    add_column :pb_activities, :end_km, :float
    end

  end
end
