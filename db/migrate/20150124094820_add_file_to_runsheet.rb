class AddFileToRunsheet < ActiveRecord::Migration
  def change
    add_attachment :runsheets, :file
  end
end
