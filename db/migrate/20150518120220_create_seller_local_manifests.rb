class CreateSellerLocalManifests < ActiveRecord::Migration
  def change
    create_table :seller_local_manifests do |t|
      t.integer :origin_id
      t.string :state

      t.timestamps null: false
    end
  end
end
