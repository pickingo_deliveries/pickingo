class AddPurIdToAwb < ActiveRecord::Migration
  def change
    add_column :awb_numbers, :pickup_request_id, :integer
  end
end
