class AddIndexes < ActiveRecord::Migration
  def change
    add_index :pickup_requests, :client_request_id
    add_index :pickup_requests, :scheduled_date
    add_index :pickup_requests, :aasm_state
    add_index :pickup_boys, :name
    add_index :hubs, :name
    add_index :pincodes, :code
    add_index :clients, :name
    add_index :addresses, :locality
    add_index :addresses, :sub_locality
  end
end
