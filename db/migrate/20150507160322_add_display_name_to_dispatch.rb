class AddDisplayNameToDispatch < ActiveRecord::Migration
  def change
    add_column :dispatch_centers, :display_name, :string
  end
end
