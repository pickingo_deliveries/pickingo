class CreateDispatchCenters < ActiveRecord::Migration
  def change
    create_table :dispatch_centers do |t|
      t.string :name
      t.string :address_line
      t.string :city

      t.timestamps null: false
    end
  end
end
