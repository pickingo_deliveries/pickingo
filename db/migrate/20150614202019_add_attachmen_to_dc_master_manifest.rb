class AddAttachmenToDcMasterManifest < ActiveRecord::Migration
  def change
    add_attachment :dispatch_center_master_manifests, :request_excel_file
  end
end
