class AddPickupBoyIdToHistory < ActiveRecord::Migration
  def change
    add_column :pickup_request_state_histories, :pickup_boy_id, :integer
  end
end
