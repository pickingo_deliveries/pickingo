module Uploaders
  class ClientDspPincodeUploader
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :file, :success_count, :fail_count, :fail_data, :fail_errors

    def initialize(attributes = {})
      attributes.each { |name, value| send("#{name}=", value) }
    end

    def persisted?
      false
    end

    def save
      client_dsp_pincodes = load_imported_client_dsp_pincodes.reject(&:nil?)
      valid_client_dsp_pincodes = [];invalid_client_dsp_pincodes=[];invalid_rows = []
      client_dsp_pincodes.each_with_index do |cdp,index|
        if !cdp.valid?
          invalid_client_dsp_pincodes << cdp
          invalid_rows << index+2
        else
          valid_client_dsp_pincodes << cdp
        end
      end
      if invalid_client_dsp_pincodes.size < 20
        process_valid_client_dsp_pincodes(valid_client_dsp_pincodes)
        process_invalid_client_dsp_pincodes(invalid_client_dsp_pincodes,invalid_rows)
        true
      else
        return false
      end
    end



    def process_valid_client_dsp_pincodes(client_dsp_pincodes)
      Parallel.each(client_dsp_pincodes) do |cdp|
        cdp.save!
      end
      self.success_count = client_dsp_pincodes.size
    end

    def process_invalid_client_dsp_pincodes(client_dsp_pincodes,invalid_rows)
      client_dsp_pincodes.each_with_index do |cdp,index|
        messages = []
        self.fail_data << {row:invalid_rows[index], awb_number:cdp.attributes}
        self.fail_count = self.fail_count + 1
        cdp.errors.full_messages.each do |message|
          messages << message
        end
        fail_errors << {(index+2) => messages}
      end
    end


    def import_client_dsp_pincodes
      @imported_client_dsp_pincodes ||= load_imported_client_dsp_pincodes
    end


    def load_imported_client_dsp_pincodes
      spreadsheet = open_spreadsheet
      cdp_arr = []
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        clients, dsp = get_client_dsp(row)
        if clients.length > 0 && dsp
          clients.each do |client|
            client_dsp = ClientDsp.find_by(client:client,dsp:dsp)
            pincode = get_pincode(row)
            cdp = ClientDspPincode.find_or_initialize_by(client_dsp:client_dsp,pincode:pincode) if client_dsp && pincode
            cdp.priority = row['Priority']
            cdp_arr << cdp
          end
        end
      end
      cdp_arr
    end


    def open_spreadsheet
      case File.extname(file.original_filename)
        when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
        when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
        else raise "Unknown file type: #{file.original_filename}"
      end
    end

    def get_pincode(row)
      pincode = Pincode.find_by(code:row['Pincode'].to_i) || Pincode.new(code:row['Pincode'].to_i)
      pincode.save if !pincode.id
      return pincode
    end

    def get_client_dsp(row)
      dsp = Dsp.where('name = ? OR alternate_name = ?', row['DSP'], row['DSP']).first
      names = row['Clients'].split(',')
      clients = Array.new.tap do |arr|
        names.each do |name|
          client = Client.find_by(name:name)
          arr << client if client
        end
      end
      return clients,dsp
    end
  end
end