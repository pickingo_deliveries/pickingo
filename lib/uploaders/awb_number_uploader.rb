module Uploaders
  class AwbNumberUploader
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :file, :success_count, :fail_count, :fail_data, :fail_errors

    def initialize(attributes = {})
      attributes.each { |name, value| send("#{name}=", value) }
    end

    def persisted?
      false
    end

    def save
      awb_numbers = load_imported_awb_numbers.reject(&:nil?)
      valid_awb_numbers = [];invalid_awb_numbers=[];invalid_rows = []
      awb_numbers.each_with_index do |awb,index|
        if !awb.valid?
          invalid_awb_numbers << awb
          invalid_rows << index+2
        else
          valid_awb_numbers << awb
        end
      end
      if invalid_awb_numbers.size < 20
        process_valid_awb_numbers(valid_awb_numbers)
        process_invalid_awb_numbers(invalid_awb_numbers,invalid_rows)
        true
      else
        return false
      end
    end



    def process_valid_awb_numbers(awb_numbers)
      awb_numbers.each do |awb_number|
        awb_number.save!
      end
      self.success_count = awb_numbers.size
    end

    def process_invalid_awb_numbers(awb_numbers,invalid_rows)
      awb_numbers.each_with_index do |awb_number,index|
        messages = []
        self.fail_data << {row:invalid_rows[index], awb_number:awb_number.attributes}
        self.fail_count = self.fail_count + 1
        awb_number.errors.full_messages.each do |message|
          messages << message
        end
        fail_errors << {(index+2) => messages}
      end
    end


    def import_awb_numbers
      @imported_awb_numbers ||= load_imported_awb_numbers
    end


    def load_imported_awb_numbers
      spreadsheet = open_spreadsheet
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        client_dsp = get_client_dsp(row)
        if client_dsp
          #awb_code = row['AWB No.'].to_s.chomp!('.0')
          awb_code = row['AWB No.'].to_s.gsub('.0','')
          AwbNumber.find_or_initialize_by(code:awb_code,client_dsp:client_dsp)
        end
      end
    end


    def open_spreadsheet
      case File.extname(file.original_filename)
        when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
        when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
        else raise "Unknown file type: #{file.original_filename}"
      end
    end


    def get_client_dsp(row)
      dsp = Dsp.find_by(name:row['DSP'])
      client = Client.find_by(name:row['Client'])
      ClientDsp.find_by(dsp:dsp,client:client) if client && dsp
    end
  end
end