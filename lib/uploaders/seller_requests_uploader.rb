module Uploaders
  class SellerRequestsUploader
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :file, :success_count, :fail_count, :fail_data, :fail_errors

    def initialize(attributes = {})
      attributes.each { |name, value| send("#{name}=", value) }
    end

    def persisted?
      false
    end


    def update
      requests =  import
      requests.each do |req|
        puts req
        update_req(req[0],req[1])
      end
      self.success_count = requests.size - fail_data.size
      self.fail_count = fail_data.size
      self.fail_data = fail_data
    end

    def update_req(row,index)
      request = row['Request ID'].to_s
      if is_number? (request)
        client_req_id = row['Request ID'].to_i.to_s
      else
        client_req_id = row['Request ID'].to_s
      end
      pick_req = PickupRequest.find_by(client_request_id: client_req_id)
      status = row['Status'].to_s
      if pick_req && (pick_req.manifest_type == "SellerDispatchManifest" || pick_req.manifest_type == "SellerLocalManifest")
        begin
          pick_req.req_close! if status == "Closed"
            unless pick_req.manifest.nil?
              pick_req.manifest.shut! if pick_req.manifest.check_closed_req
            end
          pick_req.return_to_client! if status == "Return To Client"
          pick_req.remarkz = row['Remarks'] unless row['Remarks'].nil?
          pick_req.undeliver! if status == "Undelivered"
        rescue AASM::InvalidTransition => e
          fail_data << {index:pick_req.client_request_id,message:e.message}
        end
      end
    end

    def is_number? string
      true if Float(string) rescue false
    end

    def import
      req_arr = []
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        req_arr << [row, i]
      end
      req_arr
    end

    def open_spreadsheet(file)
      case File.extname(file.original_filename)
        when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
        when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
        else raise "Unknown file type: #{file.original_filename}"
      end
    end

  end
end