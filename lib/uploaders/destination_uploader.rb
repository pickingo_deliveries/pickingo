module Uploaders
  class DestinationUploader
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :file, :success_count, :fail_count, :fail_data, :fail_errors

    def initialize(attributes = {})
      attributes.each { |name, value| send("#{name}=", value) }
    end

    def persisted?
      false
    end

    def save
      client_pincodes = load_imported_client_pincodes.reject(&:nil?)
      valid_client_pincodes = [];invalid_client_pincodes=[];invalid_rows = []
      client_pincodes.each_with_index do |client_pincodes,index|
        if !client_pincodes.valid?
          invalid_client_pincodes << client_pincodes
          invalid_rows << index+2
        else
          valid_client_pincodes << client_pincodes
        end
      end
      if invalid_client_pincodes.size < 20
        process_valid_client_pincodes(valid_client_pincodes)
        process_invalid_client_pincodes(invalid_client_pincodes,invalid_rows)
        true
      else
        return false
      end
    end



    def process_valid_client_pincodes(client_pincodes)
      Parallel.each(client_pincodes) do |client_pincode|
        client_pincode.save!
      end
      self.success_count = client_pincodes.size
    end

    def process_invalid_client_pincodes(client_pincodes,invalid_rows)
      client_pincodes.each_with_index do |client_pincode,index|
        messages = []
        self.fail_data << {row:invalid_rows[index], client_pincode:client_pincode.attributes}
        self.fail_count = self.fail_count + 1
        client_pincode.errors.full_messages.each do |message|
          messages << message
        end
        fail_errors << {(index+2) => messages}
      end
    end


    def imported_client_pincodes
      @imported_client_pincodes ||= load_imported_client_pincodes
    end


    def load_imported_client_pincodes
      spreadsheet = open_spreadsheet
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        client_pincode_destination = get_client_pincode(row)
        if get_dc(row)
          client_pincode_destination.dispatch_center_id = get_dc(row).id
          client_pincode_destination
        end
      end
    end


    def open_spreadsheet
      case File.extname(file.original_filename)
        when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
        when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
        else raise "Unknown file type: #{file.original_filename}"
      end
    end

    def get_client_pincode(row)
      pincode = get_pincode(row)
      client = Client.find_by(name:row['Client'])
      if pincode && client
        ClientPincodeDestination.find_or_initialize_by(pincode_id:pincode.id,client_id:client.id)
      else
        nil
      end
    end

    def get_dc(row)
      DispatchCenter.find_by(display_name:row['Destination'])
    end

    def get_pincode(row)
      pincode = Pincode.find_or_initialize_by(code:row['Pincode'].to_s.split('.')[0])
      pincode.save!
      pincode
    end
  end
end