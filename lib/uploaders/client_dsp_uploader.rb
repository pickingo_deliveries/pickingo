module Uploaders
  class ClientDspUploader
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :file, :success_count, :fail_count, :fail_data, :fail_errors

    def initialize(attributes = {})
      attributes.each { |name, value| send("#{name}=", value) }
    end

    def persisted?
      false
    end

    def save
      client_dsps = load_imported_client_dsps.reject(&:nil?)
      valid_client_dsps = [];invalid_client_dsps=[];invalid_rows = []
      client_dsps.each_with_index do |client_dsp,index|
        if !client_dsp.valid?
          invalid_client_dsps << client_dsp
          invalid_rows << index+2
        else
          valid_client_dsps << client_dsp
        end
      end
      if invalid_client_dsps.size < 20
        process_valid_client_dsps(valid_client_dsps)
        process_invalid_client_dsps(invalid_client_dsps,invalid_rows)
        true
      else
        return false
      end
    end



    def process_valid_client_dsps(client_dsps)
      Parallel.each(client_dsps) do |client_dsp|
        client_dsp.save!
      end
      self.success_count = client_dsps.size
    end

    def process_invalid_client_dsps(client_dsps,invalid_rows)
      client_dsps.each_with_index do |client_dsp,index|
        messages = []
        self.fail_data << {row:invalid_rows[index], awb_number:client_dsp.attributes}
        self.fail_count = self.fail_count + 1
        client_dsp.errors.full_messages.each do |message|
          messages << message
        end
        fail_errors << {(index+2) => messages}
      end
    end


    def import_client_dsps
      @imported_client_dsps ||= load_imported_client_dsps
    end


    def load_imported_client_dsps
      spreadsheet = open_spreadsheet
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        client, dsp = get_client_and_dsp(row)
        if client && dsp
          client_dsp = ClientDsp.find_or_initialize_by(client:client,dsp:dsp)
          client_dsp.priority = row['Priority']
          client_dsp
        end
      end
    end


    def open_spreadsheet
      case File.extname(file.original_filename)
        when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
        when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
        else raise "Unknown file type: #{file.original_filename}"
      end
    end


    def get_client_and_dsp(row)
      client = Client.find_by(name:row['Client'])
      dsp = Dsp.find_by(name:row['DSP'])
      return client, dsp
    end
  end
end