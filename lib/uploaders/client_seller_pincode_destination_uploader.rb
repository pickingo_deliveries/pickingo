module Uploaders
  class ClientSellerPincodeDestinationUploader
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :file, :success_count, :fail_count, :fail_data, :fail_errors

    def initialize(attributes = {})
      attributes.each { |name, value| send("#{name}=", value) }
    end

    def persisted?
      false
    end

    def save
      cspd = load_imported_cspd.reject(&:nil?)
      valid_cspd = [];invalid_cspd=[];invalid_rows = []
      cspd.each_with_index do |client_seller_pincode,index|
        if !client_seller_pincode.valid?
          invalid_cspd << client_seller_pincode
          invalid_rows << index+2
        else
          valid_cspd << client_seller_pincode
        end
      end
      if invalid_cspd.size < 20
        process_valid_cspd(valid_cspd)
        process_invalid_cspd(invalid_cspd,invalid_rows)
        true
      else
        return false
      end
    end



    def process_valid_cspd(client_seller_pincode_destinations)
      Parallel.each(client_seller_pincode_destinations) do |client_seller_pincode_destination|
        client_seller_pincode_destination.save!
      end
      self.success_count = client_seller_pincode_destinations.size
    end

    def process_invalid_cspd(client_seller_pincode_destinations,invalid_rows)
      client_seller_pincode_destinations.each_with_index do |client_seller_pincode_destination,index|
        messages = []
        self.fail_data << {row:invalid_rows[index], client_pincode:client_seller_pincode_destination.attributes}
        self.fail_count = self.fail_count + 1
        client_seller_pincode_destination.errors.full_messages.each do |message|
          messages << message
        end
        fail_errors << {(index+2) => messages}
      end
    end


    def imported_client_pincodes
      @imported_cspd ||= load_imported_cspd
    end


    def load_imported_cspd
      spreadsheet = open_spreadsheet
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        get_client_pincode_destination(row)
      end
    end


    def open_spreadsheet
      case File.extname(file.original_filename)
        when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
        when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
        else raise "Unknown file type: #{file.original_filename}"
      end
    end

    def get_client_pincode_destination(row)
      pincode = get_pincode(row)
      client = Client.find_by(name:row['Client'])
      destination = get_destination(row)
      if pincode && client && destination
        ClientSellerPincodeDestination.find_or_initialize_by(pincode_id:pincode.id,client_id:client.id,destination:destination)
      else
        nil
      end
    end

    def get_destination(row)
      if row['Type'] == 'Hub'
        return Hub.find_by(name:row['Destination'])
      elsif row['Type'] == 'DC'
        return DispatchCenter.find_by(display_name:row['Destination'])
      end
    end

    def get_pincode(row)
      pincode = Pincode.find_or_initialize_by(code:row['Pincode'].to_s.split('.')[0])
      pincode.save!
      pincode
    end
  end
end