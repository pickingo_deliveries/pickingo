namespace :crm do

  desc 'Seed Crm Data'
  task :all => [:departments, :ticket_types, :ticket_sub_types]


  desc 'Seed Departments'
  task :departments => :environment do
    %w[customer_care logistics customer_service].each do |name|
      department = Department.find_or_initialize_by(name:name)
      department.save!
    end
  end

  desc 'Seed Ticket Types'
  task :ticket_types => :environment do
    obj = [
        {name:'query', department:'customer_care'},
        {name:'complaint', department:'customer_service'},
        {name:'exception', department:'customer_service'},
        {name:'action', department:'logistics'},
        {name:'client_request', department:'customer_service'},
    ]
    obj.each do |o|
      ticket_type = TicketType.find_or_initialize_by(name:o[:name], identifier:o[:name].first.upcase, department:Department.find_by(name:o[:department]))
      ticket_type.save!
    end
  end


  desc 'Seed Ticket Sub Types'
  task :ticket_sub_types => :environment do
    ['pickup_schedule', 'delivery_confirmation','refund','others'].each do |name|
      ticket_sub_type = TicketSubType.find_or_initialize_by(name:name,ticket_type:TicketType.find_by(name:'query'))
      ticket_sub_type.save!
    end

    ['pickup_not_done', 'delivery_boy_complaint','others'].each do |name|
      ticket_sub_type = TicketSubType.find_or_initialize_by(name:name,ticket_type:TicketType.find_by(name:'complaint'))
      ticket_sub_type.save!
    end

    ['additional_product', 'wrong_product','refund','incomplete'].each do |name|
      ticket_sub_type = TicketSubType.find_or_initialize_by(name:name,ticket_type:TicketType.find_by(name:'exception'))
      ticket_sub_type.save!
    end

    ['cid_date', 'cancelled','schedule','edit_details','others'].each do |name|
      ticket_sub_type = TicketSubType.find_or_initialize_by(name:name,ticket_type:TicketType.find_by(name:'action'))
      ticket_sub_type.save!
    end

    ['urgent_pickup', 'lost_item','items_mismatch','rvp_slip_required','others'].each do |name|
      ticket_sub_type = TicketSubType.find_or_initialize_by(name:name,ticket_type:TicketType.find_by(name:'client_request'))
      ticket_sub_type.save!
    end
  end

  desc 'add ticket statefield'
  task :ticket_state_update => :environment do
    Ticket.all.each do |ticket|
      ticket.statefield = Ticket.states[ticket.status]
      ticket.save
    end
  end


end