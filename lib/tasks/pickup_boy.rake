namespace :pickup_boy do

  desc 'move data for all tables'
  task :pb_move => [:move_to_users, :user_entity, :attendance, :barcode_sheet, :pur_user, :pur_history, :runsheet]

  desc 'Move Pickup boys to User table'
  task :move_to_users => :environment  do
    PickupBoy.all.find_each do |boy|
      MoveUsersWorker.perform_async(boy.id)
    end
  end

  task :user_entity => :environment  do
    User.all.find_each do |user|
      UserEntityWorker.perform_async(user.id)
    end
  end

  task :attendance => :environment  do
    Attendance.all.find_each do |attendance|
      AttendanceWorker.perform_async(attendance.id)
    end
  end

  task :barcode_sheet => :environment  do
    BarcodeSheet.all.find_each do |barcode_sheet|
      BarcodeSheetWorker.perform_async(barcode_sheet.id)
    end
  end

  task :pur_user => :environment  do
    PickupRequest.all.find_each do |pickup_request|
      PurUserWorker.perform_async(pickup_request.id)
    end
  end

  task :pur_history => :environment  do
    PickupRequestStateHistory.all.find_each do |pickup_history|
      PurHistoryWorker.perform_async(pickup_history.id)
    end
  end

  task :runsheet => :environment  do
    Runsheet.all.find_each do |runsheet|
      RunsheetWorker.perform_async(runsheet.id)
    end
  end

  desc 'Checkout all pickup boys if not'
  task :checkout => :environment  do
    started_boys = PickupBoy.where(aasm_state: 'started_trip')
    started_boys.each do |boy|
      attendance = Attendance.find_or_create_by(pickup_boy_id: boy.id, date: Date.today)
      attendance.update(end_km: 0)
      boy.trip_end!
    end

    PickupBoy.where.not(aasm_state: ['checked_out', 'not_checked_in']).each(&:check_out!)
  end
end