namespace :one_time do

  desc 'Login for test'
  task :test_login => :environment do
    Client.all.each{|c| c.save}
    User.create!(email:'testhub@gmail.com',password:'cloudno9',:role => 'hub_owner', :hub => Hub.find_by(:name => 'Test hub - Andaman'))
  end

  desc 'update localities'
  task :fuzzy_test => :environment do
    Parallel.each(Address.all) do |address|
      fz = FuzzyMatch.new(Location.where(:pincode => address.pincode), :read => :sub_string)
      address.sub_locality =  fz.find(address.address_line).try(:sub_locality)
      address.locality =  fz.find(address.address_line).try(:locality)
      address.save
    end
    Address.connection.reconnect!
  end

  desc 'update addresses'
  task :addresses => :environment do
    CSV.foreach("db/address.csv",:headers=> :first_row) do |row|
      request = PickupRequest.find_by(client_request_id:row['request_id'])
      request.address.update(address_line:row['address'])
    end
  end

  desc 'update weight'
  task :update_weight => :environment do
    CSV.foreach("db/Updated_Weight.csv",:headers=> :first_row) do |row|
      request = PickupRequest.find_by(client_request_id:row['RequestID'])
      if request
        request.update(length:row['Length'],breadth:row['Breadth'],height:row['Height'],initial_weight:row['Pyhsical Vol Weight'])
      end
    end
  end

  desc 'generate requests'
  task :generate_requests => :environment do
    Parallel.each(20000000..20010000) do |code|
      GeneratedRequest.create(:code => code)
    end
  end

  desc 'Generate Client Warehouses'
  task :warehouses => :environment do
    Client.all.each do |client|
      ClientWarehouse.where(address_line:'Random address line',pincode:'122012', client_id:client.id).first_or_create!
    end
  end

  desc 'close requests'
  task :close_requests => :environment do
    PickupRequest.where(:aasm_state => ['in_transit','received']).where.not(:client => Client.where(:name => ['Koovs','Fabfurnish','Test']), :hub => Hub.where(:name => ['Jaipur','Test hub - Andaman']), :scheduled_date => '2015-03-23').each do |pickup_request|
      pickup_request.delivers!
    end
  end

  desc 'delete requests'
  task :delete_requests => :environment do
    CSV.foreach("db/requests.csv",:headers=> :first_row) do |row|
      request = PickupRequest.find_by(client_request_id:row['ID'])
      request.destroy
    end
  end

  desc 'Add Pincodes to Hub'
  task :add_pincodes => :environment do
    CSV.foreach("db/new_pincodes.csv",:headers=> :first_row) do |row|
      pc =Pincode.find_or_initialize_by(:code => row[0])
      pc.update(hub:Hub.find_by(:city => row[1]))
    end
  end

  desc 'Update Awb in Requests'
  task :update_awbs => :environment do
    CSV.foreach("db/update_awb.csv",:headers=> :first_row) do |row|
      pr = PickupRequest.find_by(client_request_id: row[0])
      awb = AwbNumber.find_by(code: row[1])
      if awb
        awb.update(pickup_request_id: pr.id) if pr
        awb.save
      end
    end
  end

  desc 'Add Seller to Requests'
  task :add_seller => :environment do
    CSV.foreach("db/seller_data.csv",:headers=> :first_row) do |row|
      begin
        pur = PickupRequest.find_by(client_request_id:row['Request Id'])
        pincode = Pincode.find_by(code:row['Seller Pincode'].to_i)
        pur.build_seller(name:row['Seller Name'], address_line:row['Seller Address'],phone:row['Seller Phone Number'].to_i.to_s,
                         pincode:pincode,city:row['Seller City']) if pur
        pur.save!
      rescue Exception => e
        puts "#{row[5]}" + e
      end
    end
  end

  desc 'Send SMS'
  task :send_sms => :environment do
    ids =[]
    hub = Hub.find_by(name:'jaipur')
    pur = hub.pickup_requests
    pur.each do |p|
      ids = ids.concat(p.pickup_request_state_histories.pluck(:id))
    end
    sms = Sm.includes(pickup_request_state_history:[pickup_request:[:client,:hub,:pickup_boy]]).where('pickup_request_state_history_id IN (?)', ids ).where("created_at > ? and sent = 0",Time.zone.now.to_date()+0.hours)
    puts sms.length
    sms.each do |sm|
      begin
        if sm.pickup_request_state_history.state == sm.pickup_request_state_history.pickup_request.aasm_state
          Api::Sms.send(sm.phone,sm.content)
          sm.update(sent:true)
        end
      rescue Exception => e
        Rollbar.error("Sms not sent.......#{sm.id}")
      end
    end
  end


  desc 'Send SMS'
  task :update_skus => :environment do
    CSV.foreach("db/Jabong.csv",:headers=> :first_row) do |row|
      p = PickupRequest.find_by(client_request_id:row['Reference No.'])
      names = row['Product Name'].to_s.split(/[,|]/)
      sku_ids = row['Sku'].to_s.split(/[,|]/)
      if names.size == sku_ids.size
        names.size.times do |i|
          p.skus.build(client_sku_id:sku_ids[i],name:names[i])
        end
      end
      p.save
    end
  end


end