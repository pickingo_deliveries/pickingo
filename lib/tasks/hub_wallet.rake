namespace :hub_wallet do
	desc "Add wallets to each user"
	task :add => :environment do
		Hub.all.each do |hub|
			Wallet.find_or_initialize_by(entity_id: hub.id, entity_type: "Hub", amount: 0).save
		end
	end
end