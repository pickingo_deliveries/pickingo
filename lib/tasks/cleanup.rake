namespace :cleanup do

  desc 'Clean local pngs and pdfs'
  task :files => :environment do
    FileUtils.rm_rf(Dir.glob(Rails.root.join('public/pdfs','*.pdf')))
    FileUtils.rm_rf(Dir.glob(Rails.root.join('public/pngs','*.png')))
  end

end