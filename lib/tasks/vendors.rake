namespace :vendors do
  desc 'Add initial designations'
  task :seed => :environment  do

    vendors = [
        {name: 'Temp'},
        {name: 'Adecco'},
        {name: 'AV Services'},
        {name: 'Buzz Works'},
        {name: 'Million Minds'},
        {name: 'RB Singh'}
    ]
    vendors.each do |vendor|
      Vendor.find_or_initialize_by(vendor).save
    end

  end
end