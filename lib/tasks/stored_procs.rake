namespace :stored_procs do
  desc 'Add initial designations'
  task :all => [:line_haul, :report_data]

  task :line_haul => :environment  do
    ActiveRecord::Base.connection.execute('call Create_Line_Haul_Data')
  end

  task :report_data => :environment  do
    ActiveRecord::Base.connection.execute('call Create_Report_Data')
  end
end

