namespace :dsp do

  DSPS = [
      {name:'BlueDart', identifier:'BD', alternate_name:'Blue Dart'},
      {name:'FirstFlight', identifier:'FF', alternate_name:'First Flight'},
      {name:'GoJavas', identifier:'GJ', alternate_name:'Javas'},
      {name:'FedEx', identifier:'FX', alternate_name:'FedEx'},
      {name:'DTDC', identifier:'DTDC', alternate_name:'DTDC'},
      {name:'Dhelivery', identifier:'DLH', alternate_name:'Dhelivery'},
      {name:'ECOM', identifier:'ECOM', alternate_name:'Ecom Express'}
  ]

  desc 'Seed DC'
  task :seed => :environment do
    DSPS.each do |dsp|
      ds = Dsp.find_or_initialize_by(identifier:dsp[:identifier])
      ds.update(name:dsp[:name],alternate_name:dsp[:alternate_name])
    end
  end
end