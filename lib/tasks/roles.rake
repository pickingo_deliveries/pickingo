namespace :roles do
  desc 'Add initial designations'
  task :seed => :environment  do

    roles = [
      {name: 'Hub In-charge', short_name: 'HI'},
      {name: 'Assistant Hub In-charge', short_name: 'AI'},
      {name: 'Field Executive', short_name: 'FE'},
      {name: 'Helper', short_name: 'HE'}
    ]
    roles.each do |role|
      Role.find_or_initialize_by(role).save
    end

  end
end