namespace :user_wallet do
	desc "Add wallets to each user"
	task :add => :environment do
		User.all.each do |user|
			Wallet.find_or_initialize_by(entity_id: user.id, entity_type: "User", amount: 0).save
		end
	end
end