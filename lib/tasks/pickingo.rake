namespace :pickingo do
  desc 'Seed data for pickingo'
  task :seed do
    Rake::Task['manifest:delivery_pincodes'].invoke
    Rake::Task['manifest:assign_final_destination'].invoke
    Rake::Task['manifest:routes'].invoke
    Rake::Task['manifest:via_locations'].invoke
    Rake::Task['roles:seed'].invoke
    Rake::Task['vendors:seed'].invoke
  end
end