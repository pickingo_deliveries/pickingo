namespace :manifest do

  desc 'Seed DC'
  task :dispatch_center => :environment do
    dcs = [
        {display_name:'GurgaonDC',name:'gurgaon_dc', address_line:'Address Line for gurgaon hub', city:'Gurgaon' },
        {display_name:'BangaloreDC',name:'bangalore_dc', address_line:'Address Line for bangalore hub', city:'Bangalore' },
    ]
    dcs.each do |dc|
      new_dc = DispatchCenter.find_or_initialize_by(name:dc[:name])
      new_dc.update(address_line:dc[:address_line],city:dc[:city],display_name:dc[:display_name])
    end
  end


  desc 'Assign Dispatch Centres'
  task :assign_final_destination => :environment do
    PickupRequest.all.each do |req|
      req.assign_final_destination
      req.update_cl
      req.save!
    end
  end

  desc 'Seed Routes'
  task :routes => :environment do
    Route.create(origin:Hub.find_by(identifier:'GGN01-HUB'), route_destination: DispatchCenter.find_by(identifier: 'GGN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'DEL01-HUB'), route_destination:DispatchCenter.find_by(identifier: 'GGN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'JPR01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'JPR01-DC'))
    Route.create(origin:Hub.find_by(identifier:'BLR01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'BLR01-DC'))
    Route.create(origin:Hub.find_by(identifier:'PNQ01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'MUM01-DC'))
    Route.create(origin:Hub.find_by(identifier:'HYD01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'HYD01-DC'))
    Route.create(origin:Hub.find_by(identifier:'MUM01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'MUM01-DC'))
    Route.create(origin:Hub.find_by(identifier:'CNN01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'CNN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'KOL01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'KOL01-DC'))
    Route.create(origin:Hub.find_by(identifier:'JPR01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'GGN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'BLR01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'GGN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'PNQ01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'GGN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'HYD01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'GGN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'MUM01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'GGN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'CNN01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'GGN01-DC'))
    Route.create(origin:Hub.find_by(identifier:'KOL01-HUB'),route_destination:DispatchCenter.find_by(identifier: 'GGN01-DC'))
  end

  desc 'Seed via locations'
  task :via_locations => :environment do
    Route.all.each_with_index do |route|
      ViaLocation.create(route:route,location:route.origin, is_origin:true, is_destination:false, sequence:0)
      ViaLocation.create(route:route,location:route.route_destination, is_origin:false,is_destination:true, sequence:1)
    end
  end

  desc 'Seed Delivery Pincodes'
  task :delivery_pincodes => :environment do
    Pincode.find_each do |pincode|
      if pincode.hub
        if pincode.hub.name != 'Gurgaon Hub' && pincode.hub.name != 'New Delhi Hub'
          DeliveryPincode.find_or_initialize_by(pincode:pincode, delivery_center:pincode.hub)
        else
          DeliveryPincode.find_or_initialize_by(pincode:pincode, delivery_center:DispatchCenter.find_by(name:'gurgaon_dc'))
        end
      end
    end
  end

end