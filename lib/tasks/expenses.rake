namespace :expenses do
	desc 'Add initial exps'
	task :seed => :environment  do

		expenses = [
			{exp_type: 'Mobile', exp_nature: 'hub'},
	  		{exp_type: 'Conveyance/Fuel', exp_nature: 'hub'},
	  		{exp_type: 'Stationary', exp_nature: 'hub'},
	  		{exp_type: 'Refreshments', exp_nature: 'hub'},
	  		{exp_type: 'House Keeping', exp_nature: 'hub'},
	  		{exp_type: 'Utility', exp_nature: 'hub'},
	  		{exp_type: 'Repair', exp_nature: 'hub'},
	  		{exp_type: 'Parking Slips', exp_nature: 'hub'},
	  		{exp_type: 'Normal Advance', exp_nature: 'fe'},
	  		{exp_type: 'Conveyance/Fuel Advance', exp_nature: 'fe'},
	  		{exp_type: 'Incentive', exp_nature: 'fe'}
	  	]
	  	expenses.each do |expense|
	  		Expense.create!(expense)
	  	end
	end
end