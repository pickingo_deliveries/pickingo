module Utilities
  class RequestDestinationDecider

    def initialize(request)
      @request = request
    end

    def final_destination
      return custom_final_location if @request.dispatch_flag == true
      return dispatch_center unless @request.seller_present?
      if @request.local?
        return @request.hub
      else
        return seller_dispatch_destination
      end
    end

    def dispatch_center
      pincode = Pincode.find_by(code:@request.address.pincode)
      cpd = ClientPincodeDestination.find_by(pincode:pincode,client:@request.client)
      if cpd
        return cpd.dispatch_center
      else
        return nil
      end
    end

    def seller_dispatch_destination
      cspd = ClientSellerPincodeDestination.find_by(client:@request.client, pincode:@request.seller.pincode)
      return cspd.destination if cspd
    end

    def custom_final_location
      begin
        raise Exceptions::InvalidFinalDestination, 'Invalid dispatch Location' unless @request.dispatch_location
        final_destination = CUSTOM_MAP.fetch(@request.dispatch_location)
      rescue Exception => e
        raise Exceptions::InvalidFinalDestination, 'Invalid dispatch Location'
      end
    end

  end
end