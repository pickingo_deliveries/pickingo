module Utilities
  class HubDestinationProvider

    def initialize(hub)
      @hub = hub
    end

    def get_destinations
      Array.new.tap do |arr|
        destinations.each do |vl|
          destination = arr.find{|des|  des[:id] == vl.location.id && des[:destination_type] == vl.location.class.name}
          if destination
            destination[:request_destinations] << {id:vl.route.route_destination.id,type:vl.route.route_destination.class.name}
          else
            arr << destination_hash(vl)
          end
        end
      end
    end

    private

    def destinations
      routes = @hub.originating_routes.default
      routes.collect{|route| route.via_locations.find_by(sequence:1)}
    end

    def destination_hash(vl)
      Hash.new.tap do |hash|
        location = vl.location
        hash[:id] = location.id
        hash[:name] = location.display_name
        hash[:display_name] = location.display_name
        hash[:destination_type] = location.class.name
        hash[:request_destinations] = [{id:vl.route.route_destination.id,type:vl.route.route_destination.class.name}]
      end
    end

  end
end