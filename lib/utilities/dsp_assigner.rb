module Utilities
  class DspAssigner

    def initialize(request)
      @request = request
    end

    def dsp
      if @request.local?
        awb = @request.awb_number
        awb.update(pickup_request_id: nil, used:false) if awb
        client_dsp_ids = Client.find_by(name:'Pickingo').client_dsps.pluck(:id)
      else
        client_dsp_ids = @request.client.client_dsps.pluck(:id)
      end
      find_dsp(client_dsp_ids)
    end

    def find_dsp(client_dsp_ids)
      temp_arr = []
      cdps = ClientDspPincode.includes(:pincode).where('client_dsp_id in (?) and pincode_id = ?', client_dsp_ids, @request.seller.pincode.id)
      cdps.uniq.each do |cdp|
        temp_arr << cdp unless cdp.priority.nil?
      end
      temp_arr.sort!{|a,b| a.priority <=> b.priority } if temp_arr.length > 1
      return temp_arr.first.client_dsp.dsp if cdps.any?
    end
  end
end