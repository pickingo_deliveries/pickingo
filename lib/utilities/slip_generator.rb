require 'prawn'
module Utilities
  class SlipGenerator

    FILE_PATH = Rails.root.join('public/pdfs', "slips_#{Time.now.getutc}.pdf")

    def initialize(requests)
      @requests = requests
      @files = []
    end

    def generate
      @requests.each { |request|
        path = generate_file(request)
        @files << path if path
      }
      collate_files
    end

    def generate_file(request)
      slip_pdf = Pdfs::SlipPdf.new(request)
      path = Rails.root.join('public/pdfs', "Slip_#{request.id}.pdf")
      slip_pdf.render_file(path)
      seller_slip = request.build_seller_slip(slip:File.open(path))
      if seller_slip.save
        return path
      else
        return nil
      end
    end

    def collate_files
      pdf = CombinePDF.new
      @files.each { |file| pdf << CombinePDF.new(file) }
      pdf.save FILE_PATH
      BundledSlip.create!(slip:File.open(FILE_PATH, 'r'))
    end

  end
end