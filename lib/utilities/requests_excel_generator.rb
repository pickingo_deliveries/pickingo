module Utilities
  class RequestsExcelGenerator
    def initialize(requests)
      @requests = requests
    end

    def generate
      filename = Rails.root.join('public/excels', "Inventory_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.xls")
      Excels::RequestsExcel.new(@requests, filename)
      InventoryExcel.create!(excel_file: File.open(filename))
    end
  end
end