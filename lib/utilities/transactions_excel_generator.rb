module Utilities
  class TransactionsExcelGenerator
    def initialize(transactions,name)
      @transactions = transactions
      @name = name
    end

    def generate
      filename = Rails.root.join('public/excels', "Transactions_#{@name}_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.xls")
      Excels::TransactionsExcel.new(@transactions, filename)
      TransactionExcel.create!(excel_file: File.open(filename))
    end
  end
end