require 'prawn'
require 'prawn/table'

module Pdfs
  class ManifestPdf < Prawn::Document

    def initialize(manifest)
      super(:left_margin => 20, :right_margin =>20)
      @requests = manifest.pickup_requests
      @client  = manifest.client
      @destination  = manifest.destination
      header
      text_content
      table_content
    end

    def header
      image "#{Rails.root}/app/assets/images/pickingo.png", width: 200, height: 50
    end

    def text_content
      y_position = cursor - 50
      bounding_box([0, y_position], :width => 540, :height => 30) do
        text "Manifest for #{@client.name} for #{@destination.name} [Total Requests: #{@requests.size}]", size: 15, style: :bold
      end
    end

    def table_content
      table request_rows do
        row(0).font_style = :bold
        self.header = true
        self.cell_style = { size: 7 }
        self.column_widths = [40,150,150,190,40]
      end
    end

    def request_rows
      [['#', 'Order No.','Request Id','Product Details', 'Items']] +
          @requests.each_with_index.map do |request,index|
            [index+1, request.client_order_number,request.client_request_id, request.product_details, request.picked_items]
          end
    end

    def export
      filename = Rails.root.join('public/pdfs', "#{@client.name}_#{Time.now.getutc}.pdf")
      render_file(filename)
      File.open(filename)
    end
  end
end