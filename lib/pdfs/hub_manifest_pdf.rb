require 'prawn'
require 'prawn/table'

module Pdfs
  class HubManifestPdf < Prawn::Document

    def initialize(hub_manifest)
      super(:left_margin => 20, :right_margin =>20)
      @hub_manifest = hub_manifest
      @requests = hub_manifest.pickup_requests
      @origin = hub_manifest.origin
      @destination = hub_manifest.destination
      header
      text_content
      table_content
    end

    def header
      image "#{Rails.root}/app/assets/images/pickingo.png", width: 200, height: 50
    end

    def text_content
      y_position = cursor - 50
      bounding_box([0, y_position], :width => 540, :height => 30) do
        text "Manifest from #{@origin.name} to #{@destination.name}", size: 15, style: :bold
      end
      text " [Total Requests: #{@requests.size}] [Weight: #{@hub_manifest.weight} kg]", size: 12, style: :bold
    end

    def table_content
      move_down 10
      table request_rows do
        row(0).font_style = :bold
        self.header = true
        self.cell_style = { size: 7 }
        self.column_widths = [30,80,80,160,40,70,70,40]
      end
    end

    def request_rows
      [['#', 'Order No.','Request Id','Product Details', 'Items', 'Origin', 'Destination', 'Action']] +
          @requests.each_with_index.map do |request,index|
            [index+1, request.client_order_number,request.client_request_id, request.product_details,
             request.picked_items, @origin.name, @destination.name, '']
          end
    end

    def export
      filename = Rails.root.join('public/pdfs', "#{@origin.name}_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.pdf")
      render_file(filename)
      File.open(filename)
    end
  end
end