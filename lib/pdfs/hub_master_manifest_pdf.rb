require 'prawn'
require 'prawn/table'

module Pdfs
  class HubMasterManifestPdf < Prawn::Document

    def initialize(hub_master_manifest)
      super(:left_margin => 20, :right_margin =>20)
      @manifests = hub_master_manifest.hub_manifests
      @origin = hub_master_manifest.origin
      @destination = hub_master_manifest.destination
      header
      text_content
      table_content
    end

    def header
      image "#{Rails.root}/app/assets/images/pickingo.png", width: 200, height: 50
    end

    def text_content
      y_position = cursor - 50
      bounding_box([0, y_position], :width => 540, :height => 30) do
        text "Master Manifest from #{@origin.name} to #{@destination.name} [Total Manifests: #{@manifests.size}]", size: 15, style: :bold
      end
    end

    def table_content
      table request_rows do
        row(0).font_style = :bold
        self.header = true
        self.cell_style = { size: 7 }
        self.column_widths = [50,100,100,150,100,70]
      end
    end

    def request_rows
      [['#', 'UID','Total Requests','Date Created', 'Weight(kg)', 'Action']] +
          @manifests.each_with_index.map do |manifest,index|
            [index+1, manifest.uid, manifest.pickup_requests.size, manifest.created_at.strftime('%a %d %b %Y %H:%M:%S'),
             manifest.weight, '']
          end
    end

    def export
      filename = Rails.root.join('public/pdfs', "#{@origin.name}_#{Time.now.getutc.strftime("%Y-%m-%d%H%M%S")}.pdf")
      render_file(filename)
      File.open(filename)
    end
  end
end