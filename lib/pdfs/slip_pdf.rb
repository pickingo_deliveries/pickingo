require 'prawn'
require 'prawn/table'
require 'barby'
require 'barby/barcode/code_128'
require 'barby/outputter/png_outputter'
require 'chunky_png'

module Pdfs
  class SlipPdf < Prawn::Document

    def initialize(request)
      super(:left_margin => 20, :right_margin =>20)
      @request = request
      @client = request.client
      @awb = request.awb_number
      text_content
      table_content
    end

    # def header
    #   image "#{Rails.root}/app/assets/images/pickingo.png", width: 200, height: 50
    # end

    def text_content
      y_position = cursor - (- 10)
      bounding_box([0, y_position], :width => 540, :height => 10) do

      end
    end

    def table_content
      table request_rows do
        row(3).columns(0..1).borders = [:right,:left]
        row(4).columns(0..1).borders = [:right,:left,:bottom]
      end
    end

    def request_rows
     [
        [{content:'Pickingo LogiXpress Pvt. Ltd.', :size => 13,:align => :center,:font_style => :bold, :padding => 20}, awb_table],
        [{content:'Name and Delivery Address', :size => 13,:align => :center,:font_style => :bold}, {content:@client.name,:align => :center,:font_style => :bold}],
        [{content:@request.seller.address_details,:align => :center,:font_style => :bold}, request_table],
        # [{content:'If Undelivered Please Return To',:font_style => :bold, :colspan => 2}],
        # [{content:'Pickingo LogiXpress Pvt. Ltd, 1262 P, Sector 45, Gurgaon, Haryana - 122009', :colspan => 2}],
        [{content:'PayTM Customer Support',:font_style => :bold, :colspan => 2}],
        [{content:'Dinesh : 9599701055 ,  Ashish : 9650761646',:font_style => :bold, :colspan => 2 }],
      ]
    end

    def awb_table
      make_table([ [{content:@request.dsp.name, :size => 15,:align => :center,:font_style => :bold}], [{image:File.open(generate_awb_barcode,'r'),:position => :center}],[{content:@awb.code,:align => :center,:font_style => :bold}]], :width => 285)
    end

    def request_table
      make_table([ ['OrderID   ' + @request.client_order_number], ['RequestID    ' + @request.client_request_id],['Items   ' + @request.product_details], ['Pickingo RequestId 3213455'],[{image:File.open(generate_request_barcode,'r'),:position => :center}]], :width => 285)
    end

    def generate_awb_barcode
      blob = Barby::Code128B.new(@awb.code)
      path = Rails.root.join('public/pngs', "awb_#{@request.client_request_id}.png")
      File.open(path, 'w'){|f| f.write blob.to_png(:xdim => 1, :height => 39) }
      return path.to_s
    end

    def generate_request_barcode
      blob = Barby::Code128B.new(@request.client_request_id)
      path = Rails.root.join('public/pngs', "awb_#{@request.client_request_id}.png")
      File.open(path, 'w'){|f| f.write blob.to_png(:xdim => 1, :height => 39) }
      return path.to_s
    end
  end
end