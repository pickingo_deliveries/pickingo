module Excels
  class HubManifestExcel

    def self.generate_excel(hub_manifest, file_path)
      requests = hub_manifest.pickup_requests
      origin = hub_manifest.origin
      destination = hub_manifest.destination
      begin
        #Spreadsheet.client_encoding = 'UTF-8'
        book = Spreadsheet::Workbook.new
        sheet1 = book.create_worksheet

        # Set worksheet name.
        sheet1.name = "Hub Manifest".titleize
        # Set sheet title
        title_format = Spreadsheet::Format.new :weight => :bold, :color => :grey, :size => 10
        sheet1.row(0).default_format = title_format # Set format of 1st row
        # Merge first 3 columns of first row
        sheet1.merge_cells(0, 0, 0, 4)
        sheet_title = "Manifest from #{origin.name} to #{destination.name} [Total Requests: #{requests.size}][Weight: #{hub_manifest.weight} kg]"
        sheet1.row(0).push(sheet_title)

        # Set header row of worksheet
        header = ['#', 'Order No.','Request Id','Product Details', 'Items', 'Origin', 'Destination']
        header_format = Spreadsheet::Format.new(:weight => :bold, :color => :red, :horizontal_align => :center, :vertical_align => :center)
        sheet1.row(1).default_format = header_format # Set format of 2nd row
        header.each_with_index do |name,i|
          sheet1.row(1).push(name)
          # Set width of each header column
          sheet1.column(i).width = name.length + 5
        end

        # Set data of worksheet
        requests.each_with_index do |request, index|
          sheet1.row(index+2).push(index+1)
          row_data = get_row_data(request, origin, destination)
          row_data.each_with_index do |data,i|
            sheet1.row(index+2).push(data)
          end
        end

        book.write file_path
        return File.open(file_path)
      rescue => exception
        return nil
      end
    end

    private
    def self.get_row_data(request, origin, destination)
      [request.client_order_number,request.client_request_id, request.product_details.to_s,
       request.picked_items, origin.name, destination.name]
    end

  end
end
