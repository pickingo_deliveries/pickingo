module Excels
  class SellerDispatchManifestExcel

    def initialize(seller_dispatch_manifest, file_path)
      @seller_dispatch_manifest = seller_dispatch_manifest
      @requests = seller_dispatch_manifest.pickup_requests
      @origin = seller_dispatch_manifest.origin
      @dsp = seller_dispatch_manifest.dsp
      begin
        Spreadsheet.client_encoding = 'UTF-8'
        book = Spreadsheet::Workbook.new
        sheet = book.create_worksheet
        set_worksheet_name(sheet)
        set_header(sheet)
        set_row_data(sheet)

        book.write file_path
      rescue => exception
        pass
      end
    end

    def set_worksheet_name(sheet)
      # Set worksheet name.
      sheet.name = "Seller Dispatch Manifest".titleize
      # Set sheet title
      title_format = Spreadsheet::Format.new :weight => :bold, :color => :grey, :size => 10
      sheet.row(0).default_format = title_format # Set format of 1st row
      # Merge first 3 columns of first row
      sheet.merge_cells(0, 0, 0, 4)
      sheet_title = "Seller Dispatch Manifest from #{@origin.name} [DSP: #{@dsp.name}] [Total Requests: #{@requests.size}] [Weight: #{@seller_dispatch_manifest.weight} kg]"
      sheet.row(0).push(sheet_title)
    end

    def set_header(sheet)
      # Set header row of worksheet
      header = ['#', 'Order No.','Request Id','Product Details', 'Items', 'Origin', 'DSP', 'Awb No.','Weight',
                'Seller Name', 'Address', 'City', 'Pincode', 'Seller Phone No.']
      header_format = Spreadsheet::Format.new(:weight => :bold, :color => :red, :horizontal_align => :center, :vertical_align => :center)
      sheet.row(1).default_format = header_format # Set format of 2nd row
      header.each_with_index do |name,i|
        sheet.row(1).push(name)
        # Set width of each header column
        sheet.column(i).width = name.length + 5
      end
    end

    def set_row_data(sheet)
      # Set data of worksheet
      @requests.each_with_index do |request, index|
        sheet.row(index+2).push(index+1)
        row_data = get_row_data(request)
        row_data.each_with_index do |data,i|
          sheet.row(index+2).push(data)
        end
      end
    end

    def get_row_data(request)
      address = request.address
      [request.client_order_number,request.client_request_id, request.product_details.to_s,
       request.picked_items, @origin.name, @dsp.name, request.awb_number.code,request.weight,request.seller.name, request.seller.address_line,
       request.seller.city, request.seller.pincode.code, request.seller.phone]
    end

  end
end
