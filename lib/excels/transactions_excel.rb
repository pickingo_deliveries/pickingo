module Excels
  class TransactionsExcel

    def initialize(transactions, file_path)
      @transactions = transactions
      begin
        Spreadsheet.client_encoding = 'UTF-8'
        book = Spreadsheet::Workbook.new
        sheet = book.create_worksheet
        set_worksheet_name(sheet)
        set_header(sheet)
        set_row_data(sheet)

        book.write file_path
      rescue => exception
        puts exception
      end
    end

    def set_worksheet_name(sheet)
      # Set worksheet name.
      sheet.name = "Transactions".titleize
      # Set sheet title
      title_format = Spreadsheet::Format.new :weight => :bold, :color => :grey, :size => 10
      sheet.row(0).default_format = title_format # Set format of 1st row
      # Merge first 3 columns of first row
      sheet.merge_cells(0, 0, 0, 4)
      sheet_title = "[Total Transactions: #{@transactions.size}]"
      sheet.row(0).push(sheet_title)
    end

    def set_header(sheet)
      # Set header row of worksheet
      header = ['#', 'Payment Date','Paid for Date', 'Paid by', 'Recieved by', 'Transaction type', 'Amount','Expense Type','Expense Sub Type','Remarks']
      header_format = Spreadsheet::Format.new(:weight => :bold, :color => :red, :horizontal_align => :center, :vertical_align => :center)
      sheet.row(1).default_format = header_format # Set format of 2nd row
      header.each_with_index do |name,i|
        sheet.row(1).push(name)
        # Set width of each header column
        sheet.column(i).width = name.length + 5
      end
    end

    def set_row_data(sheet)
      # Set data of worksheet
      @transactions.each_with_index do |transaction, index|
        sheet.row(index+2).push(index+1)
        row_data = get_row_data(transaction)
        row_data.each_with_index do |data,i|
          sheet.row(index+2).push(data)
        end
      end
    end

    def get_row_data(transaction)
      puts transaction.id
      data = [transaction.payment_date.in_time_zone.strftime('%a %d %b %Y')]
      data += [transaction.paid_for_date.in_time_zone.strftime('%a %d %b %Y'),
        transaction.sender.full_name, transaction.reciever.full_name,transaction.transaction_type, transaction.amount,
      transaction.expense_type, transaction.expense_sub_type,transaction.remark] if transaction.transaction_type == 'Expense'
      data += [transaction.sender_id,transaction.sender_name,transaction.reciever.full_name,
        transaction.transaction_type, transaction.amount] if transaction.transaction_type == 'Advance'
      data
    end

    def export(filename)
      File.open(filename)
    end
  end
end
