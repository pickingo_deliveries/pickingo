module Excels
  class HubMasterManifestExcel

    def self.generate_excel(hub_master_manifest, file_path)
      manifests = hub_master_manifest.hub_manifests
      origin = hub_master_manifest.origin
      destination = hub_master_manifest.destination
      begin
        Spreadsheet.client_encoding = 'UTF-8'
        book = Spreadsheet::Workbook.new
        sheet1 = book.create_worksheet

        # Set worksheet name.
        sheet1.name = "Hub Master Manifest".titleize
        # Set sheet title
        title_format = Spreadsheet::Format.new :weight => :bold, :color => :grey, :size => 10
        sheet1.row(0).default_format = title_format # Set format of 1st row
        # Merge first 3 columns of first row
        sheet1.merge_cells(0, 0, 0, 4)
        sheet_title = "Master Manifest from #{origin.name} to #{destination.name} [Total Manifests: #{manifests.size}]"
        sheet1.row(0).push(sheet_title)

        # Set header row of worksheet
        header = ['#', 'UID','Total Requests','Date Created', 'Weight(kg)']
        header_format = Spreadsheet::Format.new(:weight => :bold, :color => :red, :horizontal_align => :center, :vertical_align => :center)
        sheet1.row(1).default_format = header_format # Set format of 2nd row
        header.each_with_index do |name,i|
          sheet1.row(1).push(name)
          # Set width of each header column
          sheet1.column(i).width = name.length + 5
        end

        # Set data of worksheet
        manifests.each_with_index do |manifest, index|
          sheet1.row(index+2).push(index+1)
          row_data = get_row_data(manifest)
          row_data.each_with_index do |data,i|
            sheet1.row(index+2).push(data)
          end
        end

        book.write file_path
        return File.open(file_path)
      rescue => exception
        return nil
      end
    end

    private
    def self.get_row_data(manifest)
      [manifest.uid, manifest.pickup_requests.size, manifest.created_at.strftime('%a %d %b %Y %H:%M:%S'), manifest.weight]
    end

  end
end
