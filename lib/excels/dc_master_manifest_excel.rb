module Excels
  class DcMasterManifestExcel

    def initialize(master_manifest, file_path)
      @master_manifest = master_manifest
      @manifests = master_manifest.dispatch_center_manifests
      @origin = master_manifest.origin
      @destination = master_manifest.destination
      begin
        Spreadsheet.client_encoding = 'UTF-8'
        book = Spreadsheet::Workbook.new
        sheet = book.create_worksheet
        set_worksheet_name(sheet)
        set_header(sheet)
        set_row_data(sheet)

        book.write file_path
      rescue => exception
        pass
      end
    end

    def set_worksheet_name(sheet)
      # Set worksheet name.
      sheet.name = "Dispatch Center Master Manifest".titleize
      # Set sheet title
      title_format = Spreadsheet::Format.new :weight => :bold, :color => :grey, :size => 10
      sheet.row(0).default_format = title_format # Set format of 1st row
      # Merge first 3 columns of first row
      sheet.merge_cells(0, 0, 0, 4)
      sheet_title = "Master Manifest from #{@origin.name} to #{@destination.name}] [Total Manifests: #{@manifests.size}]"
      sheet.row(0).push(sheet_title)
    end

    def set_header(sheet)
      # Set header row of worksheet
      header = ['#', 'UID','Total Requests','Date Created', 'Weight(kg)']
      header_format = Spreadsheet::Format.new(:weight => :bold, :color => :red, :horizontal_align => :center, :vertical_align => :center)
      sheet.row(1).default_format = header_format # Set format of 2nd row
      header.each_with_index do |name,i|
        sheet.row(1).push(name)
        # Set width of each header column
        sheet.column(i).width = name.length + 5
      end
    end

    def set_row_data(sheet)
      # Set data of worksheet
      @manifests.each_with_index do |manifest, index|
        sheet.row(index+2).push(index+1)
        row_data = get_row_data(manifest)
        row_data.each_with_index do |data,i|
          sheet.row(index+2).push(data)
        end
      end
    end

    def get_row_data(manifest)
      [manifest.uid, manifest.pickup_requests.size, manifest.created_at.strftime('%a %d %b %Y %H:%M:%S'), manifest.weight]
    end

  end
end
