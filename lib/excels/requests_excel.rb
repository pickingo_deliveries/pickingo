module Excels
  class RequestsExcel

    def initialize(requests, file_path)
      @requests = requests
      begin
        Spreadsheet.client_encoding = 'UTF-8'
        book = Spreadsheet::Workbook.new
        sheet = book.create_worksheet
        set_worksheet_name(sheet)
        set_header(sheet)
        set_row_data(sheet)

        book.write file_path
      rescue => exception
        puts exception
      end
    end

    def set_worksheet_name(sheet)
      # Set worksheet name.
      sheet.name = "Inventory".titleize
      # Set sheet title
      title_format = Spreadsheet::Format.new :weight => :bold, :color => :grey, :size => 10
      sheet.row(0).default_format = title_format # Set format of 1st row
      # Merge first 3 columns of first row
      sheet.merge_cells(0, 0, 0, 4)
      sheet_title = "Inventory [Total Requests: #{@requests.size}]"
      sheet.row(0).push(sheet_title)
    end

    def set_header(sheet)
      # Set header row of worksheet
      header = ['#', 'Order No.','Request Id', 'Status', 'Date Created', 'Received Date', 'Client']
      header << 'Final Destination' if @requests.first && @requests.first.current_location_type == 'Hub'
      header += ['Product Details', 'Weight', 'Request Type']
      header_format = Spreadsheet::Format.new(:weight => :bold, :color => :red, :horizontal_align => :center, :vertical_align => :center)
      sheet.row(1).default_format = header_format # Set format of 2nd row
      header.each_with_index do |name,i|
        sheet.row(1).push(name)
        # Set width of each header column
        sheet.column(i).width = name.length + 5
      end
    end

    def set_row_data(sheet)
      # Set data of worksheet
      @requests.each_with_index do |request, index|
        sheet.row(index+2).push(index+1)
        row_data = get_row_data(request)
        row_data.each_with_index do |data,i|
          sheet.row(index+2).push(data)
        end
      end
    end

    def get_row_data(request)
      data = [request.client_order_number,request.client_request_id, request.aasm_state.titleize,
       request.created_at.in_time_zone.strftime('%a %d %b %Y %H:%M:%S'),
       request.pickup_request_state_histories.where(state: 'received').last.try(:created_at).try(:in_time_zone).try(:strftime,'%a %d %b %Y %H:%M:%S'),
       request.client.try(:name)]
      data << request.final_destination.try(:display_name) if request.current_location_type == 'Hub'
      data += [request.product_details.to_s, request.weight]
      if request.local?
        data << 'Seller Local'
      elsif request.dispatch?
        data << 'Seller Dispatch'
      else
        data << 'Normal'
      end
      data
    end

    def export(filename)
      File.open(filename)
    end
  end
end
