module HistoryManagers
  class HubMasterManifestHistoryManager

    def initialize(hub_master_manifest)
      @hub_master_manifest = hub_master_manifest
    end

    def create
      HubMasterManifestHistory.create!(state:@hub_master_manifest.state,hub_master_manifest:@hub_master_manifest, remarks:'Master Manifest Created')
    end

    def transit
      HubMasterManifestHistory.create!(state:@hub_master_manifest.state,hub_master_manifest:@hub_master_manifest, remarks:'Master Manifest is made in transit')
    end

    def partially_receive
      HubMasterManifestHistory.create!(state:@hub_master_manifest.state,hub_master_manifest:@hub_master_manifest, remarks:'Master Manifest is partially received. Some child manifests are missing')
    end

    def close
      HubMasterManifestHistory.create!(state:@hub_master_manifest.state,hub_master_manifest:@hub_master_manifest, remarks:'Master Manifest received with all the manifests')
    end

    def cancel
      HubMasterManifestHistory.create!(state:@hub_master_manifest.state,hub_master_manifest:@hub_master_manifest, remarks:'Master Manifest cancelled')
    end

    def lost
      HubMasterManifestHistory.create!(state:@hub_master_manifest.state,hub_master_manifest:@hub_master_manifest, remarks:'Master Manifest lost')
    end
  end
end