module HistoryManagers
  class SellerLocalHistoryManager

    def initialize(seller_local_manifest)
      @manifest = seller_local_manifest
    end


    def create
      SellerLocalManifestHistory.create!(state:@manifest.state, remarks:'Seller Local Manifest created',seller_local_manifest_id:@manifest.id)
    end

    def deliver
      SellerLocalManifestHistory.create!(state:@manifest.state, remarks:"Seller Local Manifest delivered to #{@manifest.dsp.name}",seller_local_manifest_id:@manifest.id)
    end

    def close
      SellerLocalManifestHistory.create!(state:@manifest.state, remarks:'Seller Local Manifest Closed',seller_local_manifest_id:@manifest.id)
    end

  end
end