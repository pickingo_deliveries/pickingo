module HistoryManagers
  class ManifestHistoryManager

    def initialize(manifest)
      @manifest = manifest
    end

    def create_new
      ManifestHistory.create!(state:@manifest.state,start:@manifest.start,
                              halt:@manifest.halt,manifest:@manifest, remarks:'Manifest Created')
    end

    def transit_complete
      ManifestHistory.create!(state:@manifest.state,start:@manifest.start,
                              halt:@manifest.halt,manifest:@manifest, remarks:@manifest.remarks)
    end

    def pause_complete
      ManifestHistory.create!(state:@manifest.state,halt:@manifest.halt,
                              manifest:@manifest, remarks:@manifest.remarks)
    end

    def close_complete
      ManifestHistory.create!(state:@manifest.state,halt:@manifest.destination,
                              manifest:@manifest,remarks:@manifest.remarks)
    end

  end
end