module HistoryManagers
  class DCManifestHistoryManager

    def initialize(dc_manifest)
      @dc_manifest = dc_manifest
    end

    def create
      DispatchCenterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_manifest:@dc_manifest, remarks:'Manifest Created Successfully', dc_vehicle_id:nil)
    end

    def assign_master
      DispatchCenterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_manifest:@dc_manifest, remarks:"Master Manifest assigned with Uid #{@dc_manifest.dispatch_center_master_manifest.uid}")
    end

    def master_transition
      DispatchCenterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_manifest:@dc_manifest, remarks:'Master Manifest is made in transit')
    end

    def cancel
      DispatchCenterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_manifest:@dc_manifest, remarks:'Manifest cancelled')
    end

    def master_cancel
      DispatchCenterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_manifest:@dc_manifest, remarks:'Master Manifest cancelled')
    end

    def close
      DispatchCenterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_manifest:@dc_manifest, remarks:'Manifest closed')
    end

  end
end
