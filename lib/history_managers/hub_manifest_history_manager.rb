module HistoryManagers
  class HubManifestHistoryManager

    def initialize(hub_manifest)
      @hub_manifest = hub_manifest
    end

    def create
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:'Manifest Created Successfully')
    end

    def assign_master
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:"Master Manifest assigned with Uid #{@hub_manifest.hub_master_manifest.uid}")
    end

    def master_transition
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:'Master Manifest is made in transit')
    end

    def pause
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:'Master Manifest is halted')
    end

    def receive_without_checking_requests
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:'Manifest Received with unchecked requests. Some Dockets are not received')
    end

    def receive
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:'Manifest Received Completely')
    end

    def cancel
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:'Manifest cancelled')
    end

    def master_cancel
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:'Master Manifest cancelled')
    end

    def lost
      HubManifestHistory.create!(state:@hub_manifest.state,hub_manifest:@hub_manifest, remarks:'Manifest lost')
    end
  end
end
