module HistoryManagers
  class TicketHistoryManager

    def initialize(ticket,crm_user)
      @ticket = ticket
      @crm_user = crm_user
    end


    def assign_complete
      @ticket.comments.create!(title:"assigned ticket to #{@ticket.assigned_to.name}", crm_user_id:@crm_user.id, status:@ticket.status)
    end

    def hold_complete
      @ticket.comments.create!(title:'has put the ticket on hold',comment:@ticket.remarks, crm_user_id:@crm_user.id, status:@ticket.status)

    end

    def progress_complete
      @ticket.comments.create!(title:'marked ticket in progress',comment:@ticket.remarks, crm_user_id:@crm_user.id, status:@ticket.status)
    end

    def close_complete
      @ticket.comments.create!(title:'closed the ticket',comment:@ticket.remarks, crm_user_id:@crm_user.id, status:@ticket.status)
    end

  end
end