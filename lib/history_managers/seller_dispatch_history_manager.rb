module HistoryManagers
  class SellerDispatchHistoryManager

    def initialize(seller_dispatch_manifest)
      @manifest = seller_dispatch_manifest
    end


    def create
      puts '*'*100
      puts 'create'
      SellerDispatchManifestHistory.create(state:@manifest.state, remarks:'Seller Dispatch Manifest created',seller_dispatch_manifest_id:@manifest.id)
    end

    def deliver
      puts '*'*100
      puts 'deliver'
      SellerDispatchManifestHistory.create(state:@manifest.state, remarks:"Seller Dispatch Manifest delivered to #{@manifest.dsp.name}",seller_dispatch_manifest_id:@manifest.id)
    end

    def close
      SellerDispatchManifestHistory.create(state:@manifest.state, remarks:'Seller Dispatch Manifest Closed',seller_dispatch_manifest_id:@manifest.id)
    end

  end
end