module HistoryManagers
  class DcMasterManifestHistoryManager

    def initialize(dc_manifest)
      @dc_manifest = dc_manifest
    end

    def create
      DispatchCenterMasterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_master_manifest:@dc_manifest, remarks:'Manifest Created Successfully', dc_vehicle_id:nil)
    end

    def transit
      DispatchCenterMasterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_master_manifest:@dc_manifest, remarks:"Manifest made in transit by #{@dc_manifest.dc_vehicle.reg_number}", dc_vehicle_id:@dc_manifest.dc_vehicle_id)
    end

    def cancel
      DispatchCenterMasterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_master_manifest:@dc_manifest, remarks:'Master Manifest cancelled', dc_vehicle_id:nil)
    end

    def close
      DispatchCenterMasterManifestHistory.create!(state:@dc_manifest.state,dispatch_center_master_manifest:@dc_manifest, remarks:'Master Manifest closed', dc_vehicle_id:nil)
    end

  end
end
