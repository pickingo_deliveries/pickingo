module Finders
  class ManifestRequestFinder

    def initialize(params,location)
      @location = location
      @params = params
    end

    def find_request
      request = PickupRequest.includes(:final_destination).received_at(@location).find_by(client_request_id:@params[:client_request_id])
      if request.nil?
         {message:"No recieved request found for #{@location.display_name}. Please Enter a valid Barcode"}
      elsif @params[:destination_id] && request.final_destination_id != @params[:destination_id].to_i
        {message:'This request is not destined to the selected destination'}
      elsif  @params[:client_id] && request.client_id != @params[:client_id].to_i
        {message:'This request is not destined to the selected warehouse'}
      elsif @params[:next_location_id] && next_location_match?(request,@params[:next_location_id].to_i, @params[:next_location_type])
        {message:'This request is not destined to the selected destination'}
      elsif !request.returned_to_client? && request.local?
         {message:'This request should be added to seller Local Manifest not Pickingo Manifests'}
      elsif request.dispatch? && is_dec?
         {message:'This request should be added to Seller Dispatch Manifest not Pickingo Manifest'}
      else
         ManifestRequestSerializer.new(request)
      end
    end

    def is_dec?
      return true if @location.class.name == 'DispatchCenter'
      return true if @location.class.name == 'Hub' && @location.dec
      return false
    end

    def next_location_match?(request, next_location_id, next_location_type)
      route = Route.find_by(origin_id: request.current_location_id, route_destination_id: request.final_destination_id, default: true)
      location = route.via_locations.find_by(sequence:1)
      location.location_id != next_location_id || location.location_type != next_location_type
    end

  end
end