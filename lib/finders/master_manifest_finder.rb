module Finders
  class MasterManifestFinder

    def initialize(request, params)
      @request = request
      @params = params
    end

    def find_master
      manifest = @request.try(:manifest)
      if @request.try(:manifest_type) == 'HubManifest'
        master_manifest = manifest.try(:hub_master_manifest)
      else
        master_manifest = manifest.try(:dispatch_center_master_manifest)
      end

      if master_manifest.nil?
        return nil
      end
      if @params[:state] && !@params[:state].include?(master_manifest.state)
        return nil
      end
      if @params[:destination_id] && (master_manifest.destination_id != @params[:destination_id].to_i ||
          master_manifest.destination_type != @params[:destination_type])
        return nil
      end
      if @params[:origin_id] && master_manifest.origin_id != @params[:origin_id].to_i
        return nil
      end
      master_manifest
    end

  end
end
