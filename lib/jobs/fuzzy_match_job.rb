module Jobs
  class FuzzyMatchJob < Struct.new(:address_id)

    def perform
      @address = Address.find(address_id)
      fz = FuzzyMatch.new(Location.where(:pincode =>@address.pincode), :read => :sub_string)
      @address.sub_locality =  fz.find(@address.address_line).try(:sub_locality)
      @address.locality =  fz.find(@address.address_line).try(:locality)
      @address.save
    end

  end
end