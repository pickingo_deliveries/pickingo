module Jobs
  class SellerLocalManifestExcelJob < Struct.new(:seller_local_manifest_id)

    def perform
      seller_local_manifest = SellerLocalManifest.find(seller_local_manifest_id)
      filename = Rails.root.join('public/excels', "#{seller_local_manifest.origin.name}_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.xls")
      Excels::SellerLocalManifestExcel.new(seller_local_manifest, filename)
      seller_local_manifest.update(excel_file: File.open(filename))
    end

    def success(job)
      seller_local_manifest = SellerLocalManifest.find(seller_local_manifest_id)
      data = ManifestAttachmentSerializer.new(seller_local_manifest).to_json
      Pusher.trigger("seller-local-manifest-#{seller_local_manifest.origin_id}", 'created', data)
    end

  end
end