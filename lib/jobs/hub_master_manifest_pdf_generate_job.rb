module Jobs
  class HubMasterManifestPdfGenerateJob < Struct.new(:hub_master_manifest_id)

    def perform
      hub_master_manifest = HubMasterManifest.find(hub_master_manifest_id)
      pdf = Pdfs::HubMasterManifestPdf.new(hub_master_manifest)
      hub_master_manifest.update(pdf_file: pdf.export)
    end

    def success(job)
      hub_master_manifest = HubMasterManifest.find(hub_master_manifest_id)
      data = ManifestAttachmentSerializer.new(hub_master_manifest).to_json
      Pusher.trigger("master-manifest-#{hub_master_manifest.origin_id}", 'created', data)
    end

  end
end