module Jobs
  class HubMasterManifestExcelGenerateJob < Struct.new(:hub_master_manifest_id)

    def perform
      hub_master_manifest = HubMasterManifest.find(hub_master_manifest_id)
      filename = Rails.root.join('public/excels', "#{hub_master_manifest.origin.name}_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.xls")
      file = Excels::HubMasterManifestExcel.generate_excel(hub_master_manifest, filename)
      hub_master_manifest.update(excel_file: file)
    end

    def success(job)
      hub_master_manifest = HubMasterManifest.find(hub_master_manifest_id)
      data = ManifestAttachmentSerializer.new(hub_master_manifest).to_json
      Pusher.trigger("master-manifest-#{hub_master_manifest.origin_id}", 'created', data)
    end

  end
end