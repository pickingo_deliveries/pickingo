module Jobs
  class SellerDispatchManifestExcelJob < Struct.new(:seller_dispatch_manifest_id)

    def perform
      seller_dispatch_manifest = SellerDispatchManifest.find(seller_dispatch_manifest_id)
      filename = Rails.root.join('public/excels', "#{seller_dispatch_manifest.origin.name}_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.xls")
      Excels::SellerDispatchManifestExcel.new(seller_dispatch_manifest, filename)
      seller_dispatch_manifest.update(excel_file: File.open(filename))
    end

    def success(job)
      seller_dispatch_manifest = SellerDispatchManifest.find(seller_dispatch_manifest_id)
      data = ManifestAttachmentSerializer.new(seller_dispatch_manifest).to_json
      Pusher.trigger("seller-dispatch-manifest-#{seller_dispatch_manifest.origin_id}", 'created', data)
    end

  end
end