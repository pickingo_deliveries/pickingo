module Jobs
  class HubManifestPdfGenerateJob < Struct.new(:hub_manifest_id)

    def perform
      hub_manifest = HubManifest.find(hub_manifest_id)
      pdf = Pdfs::HubManifestPdf.new(hub_manifest)
      hub_manifest.update(pdf_file: pdf.export)
    end

    def success(job)
      hub_manifest = HubManifest.find(hub_manifest_id)
      data = ManifestAttachmentSerializer.new(hub_manifest).to_json
      Pusher.trigger("manifest-#{hub_manifest.origin_id}", 'created', data)
    end

  end
end