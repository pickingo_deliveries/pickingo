module Jobs
  class DcManifestPdfGenerateJob < Struct.new(:manifest_id)

    def perform
      manifest = DispatchCenterManifest.find(manifest_id)
      pdf = Pdfs::DcManifestPdf.new(manifest)
      manifest.update(pdf_file: pdf.export)
    end

    def success(job)
      manifest = DispatchCenterManifest.find(manifest_id)
      data = ManifestAttachmentSerializer.new(manifest).to_json
      Pusher.trigger("dc-manifest-#{manifest.origin_id}", 'created', data)
    end

  end
end