module Jobs
  class SellerDispatchManifestPdfJob < Struct.new(:seller_dispatch_manifest_id)

    def perform
      seller_dispatch_manifest = SellerDispatchManifest.find(seller_dispatch_manifest_id)
      pdf = Pdfs::SellerDispatchManifestPdf.new(seller_dispatch_manifest)
      seller_dispatch_manifest.update(pdf_file: pdf.export)
    end

    def success(job)
      seller_dispatch_manifest = SellerDispatchManifest.find(seller_dispatch_manifest_id)
      data = ManifestAttachmentSerializer.new(seller_dispatch_manifest).to_json
      Pusher.trigger("seller-dispatch-manifest-#{seller_dispatch_manifest.origin_id}", 'created', data)
    end

  end
end