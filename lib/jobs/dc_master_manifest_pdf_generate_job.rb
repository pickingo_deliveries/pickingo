module Jobs
  class DcMasterManifestPdfGenerateJob < Struct.new(:master_manifest_id)

    def perform
      master_manifest = DispatchCenterMasterManifest.find(master_manifest_id)
      pdf = Pdfs::DcMasterManifestPdf.new(master_manifest)
      master_manifest.update(pdf_file: pdf.export)
    end

    def success(job)
      master_manifest = DispatchCenterMasterManifest.find(master_manifest_id)
      data = ManifestAttachmentSerializer.new(master_manifest).to_json
      Pusher.trigger("dc-master-manifest-#{master_manifest.origin_id}", 'created', data)
    end

  end
end