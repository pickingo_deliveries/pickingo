module Jobs
  class SellerLocalManifestPdfJob < Struct.new(:seller_local_manifest_id)

    def perform
      seller_local_manifest = SellerLocalManifest.find(seller_local_manifest_id)
      pdf = Pdfs::SellerLocalManifestPdf.new(seller_local_manifest)
      seller_local_manifest.update(pdf_file: pdf.export)
    end

    def success(job)
      seller_local_manifest = SellerLocalManifest.find(seller_local_manifest_id)
      data = ManifestAttachmentSerializer.new(seller_local_manifest).to_json
      Pusher.trigger("seller-local-manifest-#{seller_local_manifest.origin_id}", 'created', data)
    end

  end
end