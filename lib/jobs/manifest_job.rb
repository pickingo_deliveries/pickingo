module Jobs
  class ManifestJob < Struct.new(:manifest_id)

    def perform
      @manifest = Manifest.find(manifest_id)
      pdf = Pdfs::ManifestPdf.new(@manifest)
      @manifest.update(file:pdf.export)
    end

    def success(job)
      manifest = Manifest.find(manifest_id)
      data =  ManifestSerializer.new(manifest).to_json
      Pusher.trigger("manifest-#{manifest.origin_id}", 'created', data)
    end

  end
end