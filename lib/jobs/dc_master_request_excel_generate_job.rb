module Jobs
  class DcMasterRequestExcelGenerateJob < Struct.new(:master_manifest_id)

    def perform
      master_manifest = DispatchCenterMasterManifest.find(master_manifest_id)
      filename = Rails.root.join('public/excels', "#{master_manifest.origin.name}_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.xls")
      Excels::DcMasterRequestExcel.new(master_manifest, filename)
      master_manifest.update(request_excel_file: File.open(filename))
    end

    def success(job)
      master_manifest = DispatchCenterMasterManifest.find(master_manifest_id)
      data = ManifestAttachmentSerializer.new(master_manifest).to_json
      Pusher.trigger("dc-master-manifest-#{master_manifest.origin_id}", 'created', data)
    end

  end
end