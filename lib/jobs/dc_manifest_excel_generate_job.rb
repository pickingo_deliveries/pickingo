  module Jobs
    class DcManifestExcelGenerateJob < Struct.new(:manifest_id)

      def perform
        manifest = DispatchCenterManifest.find(manifest_id)
        filename = Rails.root.join('public/excels', "#{manifest.origin.name}_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.xls")
        Excels::DcManifestExcel.new(manifest, filename)
        manifest.update(excel_file: File.open(filename))
      end

      def success(job)
        manifest = DispatchCenterManifest.find(manifest_id)
        data = ManifestAttachmentSerializer.new(manifest).to_json
        Pusher.trigger("dc-manifest-#{manifest.origin_id}", 'created', data)
      end

    end
  end