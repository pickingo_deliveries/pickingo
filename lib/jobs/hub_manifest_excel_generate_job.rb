  module Jobs
    class HubManifestExcelGenerateJob < Struct.new(:hub_manifest_id)

      def perform
        hub_manifest = HubManifest.find(hub_manifest_id)
        filename = Rails.root.join('public/excels', "#{hub_manifest.origin.name}_#{Time.now.getutc.strftime("%Y-%m-%d-%H%M%S")}.xls")
        file = Excels::HubManifestExcel.generate_excel(hub_manifest, filename)
        hub_manifest.update(excel_file: file)
      end

      def success(job)
        hub_manifest = HubManifest.find(hub_manifest_id)
        data = ManifestAttachmentSerializer.new(hub_manifest).to_json
        Pusher.trigger("manifest-#{hub_manifest.origin_id}", 'created', data)
      end

    end
  end