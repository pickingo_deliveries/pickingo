class UserProfile < ActiveRecord::Base
  belongs_to :user

  def full_name
    str = ''
    str = self.first_name + ' ' unless self.first_name.nil?
    str += self.middle_name + ' ' unless self.middle_name.nil?
    str += self.last_name unless self.last_name.nil?
    str
  end

  def phone
    self.company_number
  end
end