class Pincode < ActiveRecord::Base
  belongs_to :hub
  has_many :client_dsp_pincodes
  has_many :client_pincode_destinations

  validates_length_of :code, is: 6
  validates_numericality_of :code
  validates_uniqueness_of :code
end
