module Metrics
  extend ActiveSupport::Concern

  def strike_rate
    new_pickups = pickup_request_state_histories.where(:state => :new ).to_sql
    same_day_pickups = pickup_request_state_histories.where(:state => :received ).joins("inner join (#{new_pickups}) new_pickups on pickup_request_state_histories.pickup_request_id = new_pickups.pickup_request_id").where("Date(pickup_request_state_histories.created_at) - Date(new_pickups.created_at) <= 3 ").count
    total_pickups = pickup_request_state_histories.where(:state => :received).size
    (same_day_pickups.to_f*100/total_pickups).round(2)
  end

  def closing_time
    new_pickups = pickup_request_state_histories.where(:state => :new ).to_sql
    pickup_request_state_histories.where(:state => [:received,:cancelled] ).joins("inner join (#{new_pickups}) new_pickups on pickup_request_state_histories.pickup_request_id = new_pickups.pickup_request_id").select("avg(TIMESTAMPDIFF(HOUR, new_pickups.created_at,pickup_request_state_histories.created_at)) as value").to_a.first.value
  end

  def summary(date)
    pickup_request_state_histories.where("pickup_request_state_histories.created_at >= ? and pickup_request_state_histories.created_at < ?", Time.zone.at(date.to_i/1000).to_date + 0.hours, Time.zone.at(date.to_i/1000).to_date + 1+ 0.hours).select("state, client_id,count(state) as count").group(:state, :client_id).to_a
  end
end