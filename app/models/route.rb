class Route < ActiveRecord::Base

  belongs_to :origin, :class_name => 'Hub'

  belongs_to :route_destination, polymorphic: true

  has_many :via_locations

  has_many :hub_master_manifests

  has_many :hub_destination_routes

  scope :default, -> {where(default: true)}


  def self.search_with_params(params)
    scope = self.all
    scope = scope.where(origin_id:params[:origin_id]) if params[:origin_id].present?
    scope = scope.where(destination_id:params[:destination_id]) if params[:destination_id].present?
    scope
  end


  def display_name
    str =''
    len = via_locations.length
    via_locations.each_with_index do |vl, index|
      str = str + vl.location.display_name
      str = str + ' -> ' if len > index+1
    end
    str
  end
end
