class DispatchCenter < ActiveRecord::Base

  has_many :client_pincode_destinations

  has_many :pickup_requests, as: :final_destination

  has_many :pickup_requests, as: :current_location

  has_many :pickup_request_state_histories, as: :current_location

  has_many :client_seller_pincode_destinations, as: :destination

  has_many :via_locations, as: :location

  has_many :routes, as: :route_destination

  has_many :originating_manifests, :foreign_key => 'origin_id', :class_name => 'DispatchCenterManifest'

  has_many :originating_master_manifests, :foreign_key => 'origin_id', :class_name => 'DispatchCenterMasterManifest'

  has_many :ending_manifests, :foreign_key => 'destination_id', :class_name => 'HubManifest'

  has_many :ending_hub_master_manifests, :foreign_key => 'destination_id', :class_name => 'HubMasterManifest'

  has_many :hub_destination_routes, :foreign_key => 'destination_id', :class_name => 'HubDestinationRoute'

  has_many :originating_local_manifests, as: :origin , :class_name => 'SellerLocalManifest'

  has_many :originating_dispatch_manifests, as: :origin, :class_name => 'SellerDispatchManifest'

  has_many :dc_vehicles

  has_many :delivery_pincodes, as: :delivery_center

  has_many :assets, as: :entity

  def dec
    return true
  end

  def asset_summary
    {
        total_assets: self.assets.size,
        assigned_count: self.assets.where.not(user_id: nil).size,
        unassigned_count: self.assets.where(user_id: nil).size,
        damaged_assets_count: self.assets.where(state: 'damaged').size,
        under_repair_assets_count: self.assets.where(state: 'under repair').size,
        items: asset_count
    }
  end

  def asset_count
    items_list = []
    ['phone','bag','t-shirt','sim','other'].each do |asset_type|
      asset = assets.asset_type_filter(asset_type)
      items_list << { item_type: asset_type, total_count:asset.size, assigned_count:asset.assigned.size,
                      unassigned_count:asset.unassigned.size }

    end
    return items_list
  end

end
