class InventoryExcel < ActiveRecord::Base

  has_attached_file :excel_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol    => 'https',
                    :url =>':s3_domain_url',
                    :path => "inventory_excel/:id/:basename-:style.:extension"

  do_not_validate_attachment_file_type :excel_file
end
