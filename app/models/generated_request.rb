class GeneratedRequest < ActiveRecord::Base
  belongs_to :client

  validates :code, presence: true
  validates_uniqueness_of :code

  attr_accessor :request_count, :for_client

  def initialize(options={})
    super
    @request_count = options[:request_count]
    @for_client = options[:for_client]
  end

  def get_requests
     request_ids = random_ids
     self.class.where(:id => request_ids).update_all(:client_id => @for_client.id)
     self.class.includes(:client).where(:id => request_ids)
  end

  private

  def generate
    Parallel.each(1..@request_count) do
      code = rand(40000000..99999999)
      if PickupRequest.find_by(client_request_id: 'P'+code.to_s).nil?
        GeneratedRequest.create(:code => 'P'+code.to_s)
      end
    end
  end

  def random_ids
    generate
    GeneratedRequest.where(:client_id => nil).limit(@request_count).order("Rand()").pluck(:id)
  end


end
