class HubDestinationRoute < ActiveRecord::Base
  belongs_to :hub
  belongs_to :destination, :class_name => 'DispatchCenter'
  belongs_to :route
end
