class Wallet < ActiveRecord::Base

	belongs_to :entity, polymorphic: true

	has_many :transactions, :class_name => "Transaction", :foreign_key => "hub_id"

	has_many :transactions, :class_name => "Transaction", :foreign_key => "reciever_code"


end
