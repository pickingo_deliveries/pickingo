class DeliveryPincode < ActiveRecord::Base
  belongs_to :pincode
  belongs_to :delivery_center, polymorphic: true
end
