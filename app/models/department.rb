class Department < ActiveRecord::Base

  has_many :crm_users
  has_many :tickets

  has_one :role

  before_save :sanitize_name

  def sanitize_name
    self.display_name = self.name.titleize
  end
end
