require 'geocoder'

class Address < ActiveRecord::Base
  belongs_to :pickup_request, :touch => true
  # after_create :geocode
  after_create :get_fuzzy_match

  validates :name , presence: true
  validates :pincode , presence: true
  validates :city , presence: true
  validates :address_line , presence: true
  validates :phone_number , presence: true
  # validates_length_of :phone_number, :is => 10, :message => 'length greater than 10'

  validates_length_of :pincode, :is => 6, :message => 'not valid'

  validate :pincode_presence

  # def geocode
  #   results = Geocoder.search(address_line+","+city+","+country)
  #
  #   if results.length > 0
  #     # CSV.open("results.csv", "a+") do |csv|
  #     #   csv << [address_line, results[0].formatted_address, results[0].types
  #     # end
  #
  #     self.is_ambiguous = true if results.length > 1
  #     self.lat = results[0].geometry['location']['lat']
  #     self.lon = results[0].geometry['location']['lng']
  #     results[0].address_components.each do |address_component|
  #       if address_component['types'].include? "sublocality_level_1"
  #         self.locality = address_component['long_name']
  #       end
  #       if address_component['types'].include? "sublocality_level_2"
  #         self.sub_locality = address_component['long_name']
  #       end
  #     end
  #     self.formatted_address = results[0].formatted_address
  #     save
  #     if locality.nil?
  #       get_fuzzy_match
  #     end
  #   else
  #     get_fuzzy_match
  #   end
  # end

  def get_fuzzy_match
    Delayed::Job.enqueue(Jobs::FuzzyMatchJob.new(id))
  end

  def pincode_presence
    errors.add(:pincode, 'not in servicable area') if Pincode.find_by(code:pincode).nil?
  end

end

