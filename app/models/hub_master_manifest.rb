class HubMasterManifest < ActiveRecord::Base
  include AASM

  belongs_to :origin , :class_name => 'Hub'
  belongs_to :destination, polymorphic: true
  has_many :hub_manifests
  has_many :hub_master_manifest_histories
  belongs_to :coloader

  has_attached_file :pdf_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  validates_attachment_content_type :pdf_file, :content_type => ['application/pdf']

  has_attached_file :excel_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  #validates_attachment_content_type :excel_file, :content_type => ['application/xlsx', 'application/vnd.ms-excel']
  do_not_validate_attachment_file_type :excel_file

  validates :origin_id, :destination_id, presence: true
  validate :hub_manifests_presence
  after_create :update_hub_manifests

  before_save :generate_uid, :if => :new_record?

  def hub_manifests_presence
    errors.add(:hub_manifests, 'No Hub Manifests present') unless hub_manifests.length > 0
  end


  scope :incoming_manifests, ->  (destination){ where(destination:destination, state:['in_transit','partially_received']) }


  aasm :column => :state , :no_direct_assignment => true do
    state :new, :initial => true
    state :in_transit
    state :lost
    state :partially_received
    state :received
    state :cancelled

    event :transit, :after => :hub_manifests_transit do
      transitions :from => [:new], :to => [:in_transit]
    end

    event :partially_receive, :after => :log_history do
      transitions :from => [:in_transit], :to => [:partially_received], guard: :can_partially_receive?
    end

    event :close , :after => :log_history do
      transitions :from => [:partially_received,:in_transit], :to => [:received], guard: :can_receive?
    end

    event :lost, :before => :hub_manifests_lost, :after => :log_history do
      transitions :from => :in_transit, :to => :lost
    end

    event :cancel, :before => :hub_manifests_cancel, :after => :log_history do
      transitions :from => [:new], :to => [:cancelled]
    end
  end

  def self.search_with_params(params)
    scope = self.all
    if params[:state].present?
      scope = scope.where(state:params[:state])
    end
    scope
  end

  def mark_received(location)
    hub_manifests.each{ |manifest| manifest.mark_received(location)}
    close!
  end

  private

  def update_hub_manifests
    ActiveRecord::Base.transaction do
      Delayed::Job.enqueue(Jobs::HubMasterManifestPdfGenerateJob.new(id))
      Delayed::Job.enqueue(Jobs::HubMasterManifestExcelGenerateJob.new(id))
      begin
        hub_manifests.update_all(hub_master_manifest_id:id)
        hub_manifests.each(&:assign_master!)
      rescue Exception => ex
        if ex.message =~ /Deadlock found when trying to get lock/
          retries += 1
          raise ex if retries > 3  ## max 3 retries
          sleep 10
          retry
        else
          raise ex
        end
      end
      manager.create
    end
  end

  def can_partially_receive?
    true
  end

  def can_receive?
    true
  end

  def hub_manifests_cancel
    hub_manifests.each(&:master_cancel!)
  end

  def hub_manifests_transit
    hub_manifests.each(&:master_transition!)
    log_history
  end

  def hub_manifests_lost
    hub_manifests.each do |manifest|
      manifest.lost! if manifest.master_transit?
    end
  end

  def log_history
    manager.send(logger_method)
  end

  def logger_method
    event = aasm.current_event.to_s
    event.scan(/[a-zA-Z_]/).join('')
  end

  def manager
    HistoryManagers::HubMasterManifestHistoryManager.new(self)
  end

  def generate_uid
    generated_uid = origin.identifier+'-Master-'+(100_000 + Random.rand(1_000_000 - 100_000)).to_s
    if HubManifest.exists?(uid:generated_uid)
      generate_uid
    else
      self.uid = generated_uid
    end
  end


end
