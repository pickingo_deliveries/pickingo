class TicketType < ActiveRecord::Base


  validates :name, presence: true

  has_many :tickets
  has_many :ticket_sub_types

  before_save :sanitize_name

  belongs_to :department

  def sanitize_name
    self.display_name = name.titleize
  end

end
