class DispatchCenterManifest < ActiveRecord::Base

  include AASM

  serialize :request_ids

  belongs_to :origin, :class_name => 'DispatchCenter'
  belongs_to :destination, :class_name => 'ClientWarehouse'

  belongs_to :client
  belongs_to :dc_vehicle
  belongs_to :dispatch_center_master_manifest
  has_many :dispatch_center_manifest_histories
  has_many :pickup_requests , as: :manifest

  before_save :generate_uid, :if => :new_record?
  validates :origin_id, :destination_id, :client_id, presence: true
  validate :pickup_requests_presence

  after_create :update_requests

  has_attached_file :pdf_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  validates_attachment_content_type :pdf_file, :content_type => ['application/pdf']

  has_attached_file :excel_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  #validates_attachment_content_type :excel_file, :content_type => ['application/xlsx', 'application/vnd.ms-excel']
  do_not_validate_attachment_file_type :excel_file


  aasm :column => :state , :no_direct_assignment => true do
    state :new, :initial => true
    state :in_master
    state :master_transit
    state :cancelled
    state :closed

    event :assign_master, :after => :log_history do
      transitions :from => [:new], :to => [:in_master], guard: :can_assign_master?
    end

    event :master_transition, :after => :request_transit do
      transitions :from => [:in_master], :to => [:master_transit], guard: :can_transit?
    end

    event :close, :after => :receive_callback do
      transitions :from => [:master_transit], :to => [:closed]
    end

    event :cancel, :before => :pickup_requests_cancel, :after => :log_history do
      transitions :from => [:new, :in_master], :to => [:cancelled]
    end

    event :master_cancel, :before => :remove_master, :after => :log_history do
      transitions :from => :in_master, :to => :new
    end

  end

  def self.search_with_params(params)
    scope = self.all
    scope = scope.where(state:params[:state]) if params[:state].present?
    scope
  end

  def self.create_dup_manifest(origin, hub_manifests)
    client = hub_manifests.first.pickup_requests.first.client
    warehouse = client.client_warehouses.first
    dc_manifest_ids = []
    hub_manifests.each do |manifest|
      dc_manifest = self.new(origin_id: origin, destination_id: warehouse.id, client_id: client.id,
          weight: manifest.weight, length: manifest.length, breadth: manifest.breadth, height: manifest.height,
          pdf_file: manifest.pdf_file, excel_file: manifest.excel_file, pickup_request_ids: manifest.pickup_request_ids)
      dc_manifest.save
      dc_manifest_ids << dc_manifest.id
    end
    return client.id, warehouse.id, dc_manifest_ids
  end

  def request_delivered(exclude_requests)
    pickup_requests.where('pickup_requests.client_request_id NOT IN (?)', exclude_requests || ['']).each(&:delivers!)
    log_history
  end

  def request_damaged(requests)
    pickup_requests.where(client_request_id: requests).each(&:damaged!)
    log_history
  end

  def receive_callback
    log_history
    pickup_request_ids = pickup_requests.pluck(:id)
    self.update_columns(request_ids: pickup_request_ids)
  end

  private

  def update_requests
    ActiveRecord::Base.transaction do
      Delayed::Job.enqueue(Jobs::DcManifestPdfGenerateJob.new(id))
      Delayed::Job.enqueue(Jobs::DcManifestExcelGenerateJob.new(id))
      begin
        pickup_requests.each(&:manifest_assign!)
      rescue Exception => ex
        if ex.message =~ /Deadlock found when trying to get lock/
          retries += 1
          raise ex if retries > 3  ## max 3 retries
          sleep 10
          retry
        else
          raise ex
        end
      end
      manager.create
    end
  end

  def pickup_requests_cancel
    pickup_requests.each(&:manifest_cancel!)
    dispatch_center_master_manifest.cancel! if in_master?
  end

  def remove_master
    self.dispatch_center_master_manifest = nil
  end

  def can_assign_master?
    dispatch_center_master_manifest.present?
  end

  def can_transit?
    dispatch_center_master_manifest.state == 'in_transit'
  end

  def request_transit
    pickup_requests.each(&:manifest_transition!)
    log_history
  end

  def log_history
    manager.send(logger_method)
  end

  def logger_method
    event = aasm.current_event.to_s
    event.scan(/[a-zA-Z_]/).join('')
  end

  def manager
    HistoryManagers::DCManifestHistoryManager.new(self)
  end

  def generate_uid
    generated_uid = client.initials+(100_000 + Random.rand(1_000_000 - 100_000)).to_s
    if DispatchCenterManifest.exists?(uid:generated_uid)
      generate_uid
    else
      self.uid = generated_uid
    end
  end

  def pickup_requests_presence
    errors.add(:pickup_requests,'No Pickup Requests Present' ) unless pickup_requests.length > 0
  end

end
