class Dsp < ActiveRecord::Base

  has_many :client_dsps
  has_many :clients, through: :client_dsps

  has_many :seller_local_manifests
  has_many :seller_dispatch_manifests

end
