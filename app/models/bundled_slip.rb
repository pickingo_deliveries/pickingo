class BundledSlip < ActiveRecord::Base

  has_attached_file :slip,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol    => 'https',
                    :url =>':s3_domain_url',
                    :path => "seller_slips/:id/:basename-:style.:extension"

  validates_attachment_content_type :slip, :content_type => ['application/pdf']
end
