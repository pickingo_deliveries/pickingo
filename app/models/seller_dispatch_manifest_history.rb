class SellerDispatchManifestHistory < ActiveRecord::Base

  belongs_to :seller_dispatch_manifest

  validates :remarks, :state, :seller_dispatch_manifest_id, presence: true
end
