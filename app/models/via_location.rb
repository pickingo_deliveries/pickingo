class ViaLocation < ActiveRecord::Base

  belongs_to :route

  belongs_to :location, polymorphic: true


end
