require 'open-uri'
require 'httparty'
module Api
  class Sms
    include HTTParty
    base_uri 'http://www.cilsms.com'

    URL = 'http://www.cilsms.com/'
    USERNAME = 'pickingo'
    PASSWORD = 'pic123'

    def self.send(mobile, message)
      response = open(URL+'MoreSms.aspx?uid='+USERNAME+'&pwd='+PASSWORD+'&mobiles='+mobile+'&message='+CGI::escape(message)).read
      return response.split('TransID')[1][1..-3]
    end

    def self.status(trans_id)
      response = open(URL+'transRpt.aspx?UID='+USERNAME+'&PWD='+PASSWORD+'&transid='+trans_id)
    end

    def self.send_multiple(xml)
      self.post('/postXml.aspx',body:xml)
    end


  end
end