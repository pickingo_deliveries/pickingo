require 'prawn'
require 'prawn/measurements'
require 'prawn/measurement_extensions'
require 'prawn/table'
require 'barby'
require 'barby/barcode/code_128'
require 'barby/outputter/png_outputter'
require 'chunky_png'

class BarcodePdf < Prawn::Document

  def initialize(requests,name)
    super(:left_margin => 20, :right_margin =>0, :top_margin =>10,:bottom_margin => -5)
    @requests = requests
    @name = name
    table_content
  end

  def table_content
    # This makes a call to request_rows and gets back an array of data that will populate the columns and rows of a table
    # I then included some styling to include a header and make its text bold. I made the row background colors alternate between grey and white
    # Then I set the table column widths
    table barcode_content do
      self.header = false
      self.cell_style = {border_width:0, size: 7}
      self.column_widths = [210,210,210]
    end
  end

  def barcode_content
    row_arr = []
    table_arr =[]

    @requests.each_with_index.map do |request,index|
      if index%3==0 && index!=0
        table_arr << row_arr
        row_arr = []
      end

      row_arr << new_pdf(request)
    end
    table_arr << row_arr
  end

  def encode_1252(string)
    if string
      string.encode('windows-1252', :invalid => :replace, :undef => :replace, :replace => '')
    else
      string
    end
  end

  def new_pdf(request)
    if (request.client.name == 'Shopclues' || request.client.name == 'Snapdeal' || request.client.name == 'PayTM')
      make_table([[{content:encode_1252("Pickingo-#{request.client.name}"),font_style: :bold, borders: [],:align => :center, padding: [20,0,0,0]}],
                  [{content:encode_1252("Request Id: #{request.client_request_id}"), borders: [],:align => :center, padding: [0,0,0,0]}],
                  [{content:encode_1252("Order Id: #{request.client_order_number}"), borders: [],:align => :center, padding: [0,0,0,0]}],
                  [{image:generate_barcode(request.client_request_id, request), padding_left: -5, padding_bottom: -12, borders: []}],
                  [{image:generate_barcode(request.client_order_number, request), padding_left: -5, padding_bottom: 0, borders: []}]],
                 :width => 180, cell_style: {size: 7})
    else
      make_table([[{content:encode_1252("Pickingo-#{request.client.name}"),font_style: :bold, borders: [],:align => :center, padding: [20,0,0,0]}],
                  [{content:encode_1252("Order Id: #{request.client_order_number}"), borders: [],:align => :center, padding: [0,0,0,0]}],
                  [{content:encode_1252("Request Id: #{request.client_request_id}"), borders: [],:align => :center, padding: [0,0,0,0]}],
                  [{image:generate_barcode(request.client_request_id, request), padding_left: -5, padding_bottom: 0, borders: []}]],
                 :width => 180, cell_style: {size: 7})
    end
  end

  def generate_barcode(code, request)
    blob = Barby::Code128B.new(code.squish)
    path = Rails.root.join('public/pngs', "barcode_#{code}_#{request.id}.png")
    height = (request.client.name == 'Shopclues' ||  request.client.name == 'Snapdeal' ||  request.client.name == 'PayTM' ) ? 10 : 30
    File.open(path, 'w'){|f| f.write blob.to_png(:xdim => 1, :height => height) }
    return File.open(path.to_s,'r')
  end

  def is_number?(object)
    true if Float(object) rescue false
  end

end