class ClientCustomer < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User

  after_initialize :ensure_authentication_token, :if => :new_record?
  before_save -> { skip_confirmation! }
  validates :client, presence:true

  belongs_to :client

  has_one :client_token

  def ensure_authentication_token
    self.uid = email if uid.blank?
    self.provider = 'email' if provider.blank?
  end

end
