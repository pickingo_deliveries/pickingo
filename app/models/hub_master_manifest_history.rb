class HubMasterManifestHistory < ActiveRecord::Base

  belongs_to :hub_master_manifest

  validates :state, :hub_master_manifest, :remarks, presence: true
end
