class Asset < ActiveRecord::Base

  ASSET_TYPE_OPTIONS = {'sim' => 'Sim', 'bag' => 'Bag','t-shirt' => 'T-shirt', 'phone' => 'Phone','fan/cooler'=>"Fan/Cooler", 'plastic_chair'=>"Plastic Chair", 'chair'=>"Chair", 'computer_table'=>"Computer Table", 'trolly'=>"Trolly", 'almirah'=>"Almirah", 'moniter'=>"Moniter", 'ups'=>"Ups", 'system'=>"System", 'camera'=>"Camera",'camera_dvr'=> "Camera Dvr", 'camera_led'=>"Camera Led", 'weight_machine'=>"Weight Machine", 'scanner'=>"Scanner", 'big_table'=>"Big Table", 'fire_ext'=>"Fire Ext.", 'barcode_printer'=>"Barcode Printer",'laser_printer'=> "Laser Printer", 'data_card'=>"Data Card", 'others' => 'Others'}

  # validates_inclusion_of :asset_type, :in => ASSET_TYPE_OPTIONS.values, :message => 'choose from the available options'

  validates_uniqueness_of :serial_no,:reference_no, allow_blank: true

  belongs_to :entity, :polymorphic => true

  has_many :asset_transitions, dependent: :destroy
  has_many :asset_users, dependent: :destroy

  belongs_to :user, -> { where(assets: {entity_type: 'User'}) }, foreign_key: 'entity_id'

  scope :assigned, -> { where.not(user_id: nil) }

  scope :unassigned, -> { where(user_id: nil)}

  scope :asset_type_filter,-> (asset_type){where(asset_type: asset_type)}

  scope :order_scope, ->(related_attribute,direction,column) {
    if related_attribute.blank? && column.present?
      order("assets.#{column}" + ' ' + direction)
    else related_attribute=="user"
      joins(:user).order("users.#{column} #{direction}") unless column.nil?
    end
  }

  def self.search_asset(params,entity)
    unless entity.nil?
      scope = self.where(entity_type: entity.class.name).order_scope(params[:relatedAttribute],params[:sortDir],params[:sortedBy])
    else
      scope = self.all.order_scope(params[:relatedAttribute],params[:sortDir],params[:sortedBy])
    end

    user = User.find_by(uid: params[:emp_id]) unless params[:emp_id].blank?
    scope = scope.where('assets.entity_id = ?', user.id) unless user.nil?

    scope = scope.where('assets.serial_no LIKE :term', term: "%#{params[:serial_no]}%") unless params[:serial_no].blank?
    scope = scope.where('assets.reference_no LIKE :term', term: "%#{params[:ref_no]}%") unless params[:ref_no].blank?
    scope = scope.where('assets.asset_type LIKE :term', term: "%#{params[:asset_type]}%") unless params[:asset_type].blank?
    scope = scope.where('assets.state LIKE :term', term: "%#{params[:state]}%") unless params[:state].blank?
    scope
  end

  def self.asset_list(user,params)
    all_assets = []

    scope = Asset.where(entity_id: user.id,entity_type: user.class.name).order_scope(params[:relatedAttribute],params[:sortDir],params[:sortedBy])

    scope = scope.where('assets.serial_no LIKE :term', term: "%#{params[:serial_no]}%") unless params[:serial_no].blank?
    scope = scope.where('assets.reference_no LIKE :term', term: "%#{params[:reference_no]}%") unless params[:reference_no].blank?
    scope = scope.where('assets.asset_type LIKE :term', term: "%#{params[:asset_type]}%") unless params[:asset_type].blank?

    scope.each do |asset|
      all_assets << {id: asset.id, asset_type: asset.asset_type, reference_no: asset.reference_no.nil? ? 'N/A' : asset.reference_no ,
                     serial_no: asset.serial_no.nil?  ? 'N/A' : asset.serial_no, state: asset.state,quantity: asset.quantity}

    end
    all_assets
  end

  def get_asset_users
    user_ids = asset_users.pluck(:user_id)
    users = []
    User.where(id: user_ids).each do |user|
      users << {id: user.id,name: user.full_name,uid: user.uid,designation:user.role_name,asset_quantity: AssetUser.find_by(asset_id:self.id,user_id:user.id).quantity}
    end
    return users
  end

  def self.get_asset_details params
    asset_details = []
    assets_with_entitity_details = self.where(asset_type: params).pluck(:entity_id,:entity_type,:quantity)
      assets_with_entitity_details.each do |asset|
        if asset[1] == "Hub"
          entity_name = Hub.find(asset[0]).name
        elsif asset[1] == "DispatchCenter"
          entity_name = DispatchCenter.find(asset[0]).name
        else
          entity_name = 'Admin'
        end
        asset_details << { asset_type: params,entity_name:entity_name,quantity:asset[2] }
      end
    return asset_details
  end

end
