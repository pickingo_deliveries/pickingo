class SellerSlip < ActiveRecord::Base
  belongs_to :pickup_request

  has_attached_file :slip,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol    => 'https',
                    :url =>':s3_domain_url',
                    :path => "seller_slips/:pickup_request_id/:basename-:style.:extension"

  validates_attachment_content_type :slip, :content_type => ['application/pdf']

end
