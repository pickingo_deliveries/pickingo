module PickupRequestUtils

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row-1).each do |i|
      begin
        row = Hash[[header, spreadsheet.row(i)].transpose]
        process_request(row)
      rescue Exception => e
        puts i
        puts e
      end
    end
  end


  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
      when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
      else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def self.process_request(row)
    order_number = row['Order No.'].to_i
    pickup_request = PickupRequest.find_by(client_order_number:order_number)||PickupRequest.new

    if pickup_request.id
      address = update_address(pickup_request,row)
    else
      address = create_address(row)
    end

    pickup_request.client_order_number = order_number
    pickup_request.client_request_id = row['Reference No.'].to_i
    pickup_request.request_type = row['Request Type']
    pickup_request.preferred_date = row['Preferred Date']
    pickup_request.preferred_time = row['Preferred Time']
    pickup_request.save!
    address.pickup_request_id = pickup_request.id
    address.save!
  end

  def self.update_address(pickup_request,row)
    address = pickup_request.address
    address.update(address_obj(row))
    address.save!
    address
  end

  def self.create_address(row)
    address = Address.new(address_obj(row))
    address.save!
    address
  end

  def self.address_obj(row)
    {
        name:row['Customer Name'],
        address_line:row['Address'],
        city:row['City Name'],
        pincode:row['Pin Code'].to_i,
        phone_number:row['Customer Phone No.'].to_i.to_s

    }
  end
end
