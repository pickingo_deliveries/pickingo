class RequestImport
  # switch to ActiveModel::Model in Rails 4
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :file, :success_count, :fail_count, :fail_data, :fail_errors

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def save
    requests = imported_requests.reject(&:nil?)
    valid_requests = []
    invalid_requests = []
    duplicates = []
    invalid_rows = []
    duplicate_rows = []
    requests.each_with_index do |req,index|
      if !req.valid? || req.final_destination.nil?
        invalid_requests << req
        invalid_rows << index+2
      elsif valid_requests.any?{|a| a.client_request_id == req.client_request_id}
        duplicates << req
        duplicate_rows << index+2
      else
        valid_requests << req
      end
    end
    process_valid_requests(valid_requests)
    process_invalid_requests(invalid_requests,invalid_rows)
    process_duplicate_requests(duplicates,duplicate_rows)
    return true

  end

  def process_valid_requests(requests)
    Parallel.each(requests) do |request|
      request.save!
    end
    self.success_count = requests.size
  end

  def process_invalid_requests(requests,invalid_rows)
    requests.each_with_index do |request,index|
      messages = []
      self.fail_data << {row:invalid_rows[index], request:get_invalid_request_object(request)}
      self.fail_count = self.fail_count + 1
      if request.hub_id.nil?
        messages << 'Request is for non-servicable pincode.'
      elsif request.final_destination.nil?
        messages << 'Final Destination is not assigned to request.'
      else
        request.errors.full_messages.each do |message|
          messages << message
        end
      end
      self.fail_errors << {invalid_rows[index] => messages}
    end
  end

  def process_duplicate_requests(requests,duplicate_rows)
    requests.each_with_index do |request,index|
      messages = []
      self.fail_data << {row:duplicate_rows[index], request:get_invalid_request_object(request)}
      self.fail_count = self.fail_count + 1
      messages << 'Duplicate Entry present in excel file'
      self.fail_errors << {duplicate_rows[index] => messages}
    end
  end

  def get_invalid_request_object(request)
    request_obj = request.attributes
    request_obj[:address_attributes] = request.address.attributes
    request_obj[:skus_attributes] = []
    request.skus.each do |sku|
      request_obj[:skus_attributes] << sku.attributes
    end
    request_obj
  end

  def imported_requests
    @imported_requests ||= load_imported_requests
  end

  def load_imported_requests
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      request = PickupRequest.find_or_initialize_by(client_request_id:row['Reference No.'].to_s.split('.')[0])
      begin
        if !request.id || (request.id && request.aasm_state=='new')
          request.attributes = request_obj(request,row)
          request.address.attributes = address_obj(request,row)
          build_skus(request,row)
          build_seller(request,row) if row['Seller Present'].to_i.to_bool
          request.assign_final_destination
          request.assign_awb_and_dsp
          request
        else
          error_messages = []
          error_messages << "Request already exists"
          request.address_attributes = address_obj(request,row)
          request.attributes = request_obj(request,row)
          request = ({address_attributes:request.address.attributes}).merge(request.attributes)
          self.fail_data << {row: i-1, request: request}
          self.fail_count = self.fail_count + 1
          self.fail_errors << {i-1 => error_messages}
          nil
        end
      rescue Exceptions::InvalidFinalDestination => e
        error_messages = []
        error_messages << e.to_s
        request.address_attributes = address_obj(request,row)
        request.attributes = request_obj(request,row)
        request = ({address_attributes:request.address.attributes}).merge(request.attributes)
        self.fail_data << {row: i-1, request: request}
        self.fail_count = self.fail_count + 1
        self.fail_errors << {i-1 => error_messages}
        next
      rescue Exception => e
        raise e
        Airbrake.notify(e,parameters:request)
      end
    end
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
      when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
      else raise "Unknown file type: #{file.original_filename}"
    end
  end


  def request_obj(request,row)
    obj = {
        client_order_number: row['Order No.'].to_s.split('.')[0],
        client_request_id: row['Reference No.'].to_s.split('.')[0],
        request_type: row['Request Type'],
        preferred_date: row['Preferred Date'],
        preferred_time: row['Preferred Time'],
        client: Client.find_by(name:row['Client Name']),
        initial_weight:row['weight'].to_f,
        length:row['length'].to_f,
        breadth:row['breadth'].to_f,
        height:row['height'].to_f,
        remarks:row['remarks'],
        price: row['price'].to_f,
        return_reason: row['Return Reason'],
        is_urgent: row['is_urgent'].to_i,
        dispatch_flag: row['dispatch_flag'].to_i,
        dispatch_location: row['dispatch_location']
    }
    if request.id
      obj[:id] = request.id
    end
    obj
  end

  def address_obj(request,row)
    obj = {
        name:row['Customer Name'],
        address_line:row['Address'],
        city:row['City Name'],
        pincode:row['Pin Code'].to_i,
        phone_number:clean_phone(row['Customer Phone No.'])
    }
    if request.id
      obj[:id] = request.address.id
    end
    obj
  end

  def clean_phone(phno)
    phno.to_i.to_s.squish[-10..-1]

  end

  def build_skus(request,row)
    request.skus.delete_all
    names = row['Product Name'].to_s.split(/[,|]/)
    sku_ids = row['Sku'].to_s.split(/[,|]/)
    prices = row['Sku_price'].to_s.split(/[,|]/)
    if names.size == sku_ids.size
      names.size.times do |i|
        request.skus.build(client_sku_id:sku_ids[i],name:names[i],price: prices[i].to_f)
      end
    end
  end

  def build_seller(request, row)
    pincode = Pincode.find_by(code:row['Seller Pincode'].to_i)
    request.build_seller(name:row['Seller Name'], address_line:row['Seller Address'],phone:row['Seller Phone Number'].to_i.to_s,
                         pincode:pincode,city:row['Seller City'])
  end

end