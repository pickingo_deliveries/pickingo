class HubManifest < ActiveRecord::Base

  include AASM

  serialize :request_ids

  belongs_to :origin, :class_name => 'Hub'
  belongs_to :destination, polymorphic: true
  has_many :pickup_requests, as: :manifest
  belongs_to :hub_master_manifest
  has_many :hub_manifest_histories

  has_attached_file :pdf_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  validates_attachment_content_type :pdf_file, :content_type => ['application/pdf']

  has_attached_file :excel_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  #validates_attachment_content_type :excel_file, :content_type => ['application/xlsx', 'application/vnd.ms-excel']
  do_not_validate_attachment_file_type :excel_file

  before_save :generate_uid, :if => :new_record?

  after_create :update_requests

  validates :origin_id, :destination_id, :weight, presence: true

  validate :pickup_requests_presence



  scope :hub_related, ->(hub_id) { where('origin_id = ? or destination_id = ?', hub_id, hub_id) }

  aasm :column => :state, :no_direct_assignment => true do
    state :new, :initial => true
    state :in_master
    state :master_transit
    state :received_unchecked_requests
    state :received
    state :cancelled
    state :lost

    event :assign_master, :after => :log_history do
      transitions :from => [:new], :to => [:in_master], guard: :can_assign_master?
    end

    event :master_transition, :after => :pickup_requests_transit do
      transitions :from => [:in_master], :to => [:master_transit], guard: :can_master_transition?
    end

    event :receive_without_checking_requests, :after => :log_history do
      transitions :from => [:master_transit], :to => [:received_unchecked_requests]
    end

    event :receive, :after => :receive_callback do
      transitions :from => [:master_transit,:received_unchecked_requests], :to => [:received]
    end

    event :cancel, :before => :pickup_requests_cancel, :after => :log_history do
      transitions :from => [:new, :in_master], :to => [:cancelled]
    end

    event :lost, :before => :pickup_requests_lost, :after => :log_history do
      transitions :from => :master_transit, :to => :lost
    end

    event :master_cancel, :before => :remove_master, :after => :log_history do
      transitions :from => :in_master, :to => :new
    end
  end

  def self.search_with_params(params)
    scope = self.all
    if params[:state].present?
      scope = scope.where(state:params[:state])
    end
    scope
  end

  def mark_received(location)
    pickup_requests.each do |request|
      request.update(current_location: location)
      request.manifest_receive!
    end
    receive!
  end

  def receive_callback
    log_history
    pickup_request_ids = pickup_requests.pluck(:id)
    self.update_columns(request_ids: pickup_request_ids)
  end

  private

  def can_assign_master?
    hub_master_manifest.present?
  end

  def can_master_transition?
    hub_master_manifest.state == 'in_transit'
  end

  def update_requests
    ActiveRecord::Base.transaction do
      Delayed::Job.enqueue(Jobs::HubManifestPdfGenerateJob.new(id))
      Delayed::Job.enqueue(Jobs::HubManifestExcelGenerateJob.new(id))
      begin
        pickup_requests.each(&:manifest_assign!)
        manager.create
      rescue Exception => ex
        if ex.message =~ /Deadlock found when trying to get lock/
          retries += 1
          raise ex if retries > 3  ## max 3 retries
          sleep 10
          retry
        else
          raise ex
        end
      end
    end
  end


  def pickup_requests_transit
    pickup_requests.each(&:manifest_transition!)
    log_history
  end

  def pickup_requests_cancel
    pickup_requests.each(&:manifest_cancel!)
    hub_master_manifest.cancel! if in_master?
  end

  def remove_master
    self.hub_master_manifest = nil
  end

  def pickup_requests_lost
    pickup_requests.each do |request|
      request.lost! if request.manifest_transit?
    end
  end

  def log_history
    manager.send(logger_method)
  end

  def logger_method
    event = aasm.current_event.to_s
    event.scan(/[a-zA-Z_]/).join('')
  end

  def manager
    HistoryManagers::HubManifestHistoryManager.new(self)
  end

  def generate_uid
    generated_uid = origin.identifier+'-HubManifest-'+(100_000 + Random.rand(1_000_000 - 100_000)).to_s
    if HubManifest.exists?(uid:generated_uid)
      generate_uid
    else
      self.uid = generated_uid
    end
  end

  def pickup_requests_presence
    errors.add(:pickup_requests, 'No Pickup Requests Present') unless pickup_requests.length > 0
  end

end
