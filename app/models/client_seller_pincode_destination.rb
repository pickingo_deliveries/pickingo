class ClientSellerPincodeDestination < ActiveRecord::Base
  belongs_to :client
  belongs_to :pincode
  belongs_to :destination, polymorphic: true


  validates :client_id, :pincode_id, :destination_id, presence: true

end
