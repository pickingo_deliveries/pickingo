class Runsheet < ActiveRecord::Base
  belongs_to :user

  has_many :pickup_requests
  has_attached_file :file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol    => 'https',
                    :url =>':s3_domain_url',
                    :path => "runsheets/files/:basename-:style.:extension"

  validates_attachment_content_type :file, :content_type => ['application/pdf']

  do_not_validate_attachment_file_type :file

  after_create :update_requests


  def update_requests
    pickup_requests.update_all(runsheet_id:id)
  end
end
