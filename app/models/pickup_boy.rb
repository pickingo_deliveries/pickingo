class PickupBoy < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User
  include AASM

  after_initialize :ensure_authentication

  before_save -> { skip_confirmation! }

  belongs_to :hub, touch: true
  has_many :pickup_requests
  has_many :pickup_request_state_histories, :through => :pickup_requests
  has_many :runsheets
  has_many :barcode_sheets
  has_many :attendances
  validates :name,:phone,:hub_id, presence: true
  validates_numericality_of :phone
  validates_length_of :phone, is: 10
  validates_uniqueness_of :phone

  aasm do
    state :not_checked_in, :initial => true
    state :checked_in
    state :checked_out
    state :not_checked_out # if boy do not checkout before midnight
    state :started_trip
    state :finished_trip

    event :check_in do
      transitions :from => [:not_checked_in, :checked_out], :to => :checked_in
    end

    event :check_out do
      transitions :from => [:checked_in,:not_checked_out, :finished_trip], :to => :checked_out
    end

    event :trip_begin do
      transitions :from =>  :checked_in , :to => :started_trip
    end

    event :trip_end do
      transitions :from => :started_trip , :to => :finished_trip
    end
  end



  def generate_runsheet(requests)
    pdf = RunsheetPdf.new(requests,name)
    filename = Rails.root.join('public/pdfs', "#{name}_#{Time.now.getutc}.pdf")
    pdf.render_file(filename)
    pdf_file = File.open(filename)
    runsheets.build(file:pdf_file,pickup_requests:requests)
    File.delete(filename)
  end

  def generate_barcode_sheet(requests)
    pdf = BarcodePdf.new(requests,name)
    filename = Rails.root.join('public/pdfs', "#{name}_barcode_#{Time.now.getutc}.pdf")
    pdf.render_file(filename)
    pdf_file = File.open(filename)
    barcode_sheets.build(file:pdf_file,pickup_requests:requests)
    File.delete(filename)
  end

  private

  def ensure_authentication
    self.uid = phone if uid.blank?
    self.email = phone if email.blank?
    self.password = 'pickingo'if password.blank?
  end


end
