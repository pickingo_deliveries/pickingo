class PickupRequest < ActiveRecord::Base
  include AASM

  has_many :skus, dependent: :destroy
  has_one :address, dependent: :destroy
  has_one :awb_number
  has_one :seller, dependent: :destroy
  has_one :seller_slip, dependent: :destroy
  has_many :pickup_request_state_histories, dependent: :destroy
  has_many :sms, :through => :pickup_request_state_histories

  has_many :tickets
  belongs_to :user
  belongs_to :hub
  belongs_to :client
  belongs_to :manifest, :polymorphic => true
  belongs_to :dsp
  belongs_to :final_destination, polymorphic: true
  belongs_to :current_location, polymorphic: true

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :skus
  accepts_nested_attributes_for :seller

  validates :client_order_number, :address, :client_id,  presence:true
  validates :hub_id, presence: {message:'Not a Serviceable pincode'}
  validates :final_destination_id, :final_destination_type, presence: {message:'Not a Serviceable pincode'}
  validates :client_request_id , presence:true, uniqueness: {message: 'Id provided already exists'}
  validate :sku_presence


  after_initialize :init_address
  before_validation :assign_hub
  after_save :add_state_history
  after_create :update_cl
  after_commit :set_schedule_date, :only => :create


  has_attached_file :signature, :styles => {:medium => "300x300>", :thumb => "100x100>" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol  => 'https',
                    :url =>':s3_domain_url',
                    :default_url => "/images/:style/missing.png",
                    :path => 'pickup_requests/:id/signature/:basename-:style.:extension'

  # validates_attachment_content_type :signature, :content_type => /\Aimage\/.*\Z/

  do_not_validate_attachment_file_type :signature

  attr_accessor :remarkz,:receiving_remarks,:dispatch_location

  scope :order_scope, ->(related_attribute,direction,column) {
    if related_attribute.blank? && column.present?
      order("pickup_requests.#{column}" + ' ' + direction)
    elsif related_attribute=="address"
      joins(:address).order("addresses.#{column} #{direction}")
    elsif related_attribute=="hub"
      joins(:hub).order("hubs.#{column} #{direction}")
    end
  }

  scope :received, -> { where(aasm_state: ['received']) }

  scope :received_at, -> (entity) { where(aasm_state:['received','returned_to_client'],current_location_id: entity.id, current_location_type: entity.class.name) }

  scope :no_seller, -> { includes(:seller).where( :sellers => { :id => nil } ) }

  scope :seller_requests, -> { includes(:seller).where.not( :sellers => { :id => nil } ) }

  scope :hub_seller_requests, -> (hub) {seller_requests.where(hub:hub)}

  scope :current_location_requests, -> (hub) {seller_requests.where(current_location:hub)}



  aasm do
    state :new, :initial => true
    state :assigned
    state :out_for_pickup
    state :not_contactable
    state :cid
    state :not_attempted
    state :cancelled
    state :on_hold
    state :picked
    state :received
    state :in_manifest
    state :manifest_transit
    state :manifest_received
    state :damaged
    state :closed
    state :lost
    state :returned_to_client
    state :undelivered


    event :assign do
      transitions :from => [:new, :not_contactable, :cid, :on_hold,:not_attempted], :to => [:assigned], :guard => :user_exists?
    end

    event :unassign do
      transitions :from => [:assigned], :to => [:new]
    end

    event :out do
      transitions :from => [:assigned,:not_contactable,:cid,:on_hold,:not_attempted], :to => [:out_for_pickup], :guard => :user_exists?
    end

    event :pick do
      transitions :from => [:out_for_pickup], :to => [:picked]
    end

    event :receive do
      transitions :from => [:out_for_pickup, :picked], :to => [:received]
    end

    event :manifest_cancel, before: :remove_manifest do
      transitions :from => [:in_manifest], :to => [:received]
    end

    event :manifest_assign do
      transitions :from => [:received, :returned_to_client] , :to => [:in_manifest]
    end

    event :manifest_transition do
      transitions :from => [:in_manifest] , :to => [:manifest_transit]
    end

    event :manifest_receive, before: :assign_dsp_local do
      transitions :from => [:manifest_transit] , :to => [:received]
    end

    event :hold do
      transitions :from => [:assigned, :not_contactable,:out_for_pickup,:cid,:not_attempted], :to => [:on_hold]
    end

    event :no_contact do
      transitions :from => [:assigned, :on_hold,:out_for_pickup], :to => [:not_contactable]
    end

    event :cancel do
      transitions :from => [:new,:assigned,:cid, :not_contactable, :on_hold,:not_attempted], :to => [:cancelled]
    end

    event :delivers do
      transitions :from => [:received, :manifest_received, :manifest_transit], :to => [:closed]
    end

    event :lost do
      transitions :from => [:out_for_pickup, :received, :manifest_transit, :manifest_received], :to => :lost
    end

    event :damaged do
      transitions :from => :manifest_transit, :to => :damaged
    end

    event :req_close do
      transitions :from => [:manifest_transit,:undelivered], :to => :closed
    end

    event :return_to_client do
      transitions :from => [:manifest_transit,:undelivered], :to => :returned_to_client
    end

    event :undeliver do
      transitions :from => :manifest_transit, :to => :undelivered
    end

    event :closed  do
      transitions :from => :manifest_transit, :to => :damaged
    end

    event :claim do
      transitions :from => :lost, :to => :received
    end

  end


  searchable do
    text :client_request_id, :boost => 5
    text :client_order_number
    text :name do
      address.name
    end
    text :phone_number do
      address.phone_number
    end
    text :pincode do
      address.pincode
    end
    text :address_line do
      address.address_line
    end
    text :product do
      skus.map(&:name)
    end
    time :created_at
  end

  def user_exists?
    user.present?
  end

  def self.search_with_params(params)
    scope = self.all

    if !params[:hubId].blank?
      scope =scope.where(hub_id:params[:hubId])
    end

    unless params[:dcId].nil?
      scope = scope.joins('INNER JOIN hub_manifests ON hub_manifests.id = pickup_requests.manifest_id').
          where("(pickup_requests.current_location_id = :dc_id and
            pickup_requests.current_location_type = :class_name) or
            (hub_manifests.destination_id = :dc_id and hub_manifests.destination_type = :class_name and
            hub_manifests.state IN (:state))", {dc_id: params[:dcId], class_name: 'DispatchCenter',
                                                state: ['master_transit', 'received_unchecked_requests']})
    end

    if !params[:hub_id].blank?
      scope =scope.where(hub_id:params[:hub_id])
    end

    if !params[:is_urgent].blank?
      scope =scope.where(is_urgent:params[:is_urgent].to_i)
    end

    if !params[:client_order_number].blank?
      scope =scope.where('client_order_number LIKE ?', "%#{params[:client_order_number]}%")
    end

    if !params[:client_request_id].blank?
      scope =scope.where('client_request_id LIKE ?', "%#{params[:client_request_id]}%")
    end

    if !params[:aasm_state].blank?
      scope =scope.where('pickup_requests.aasm_state in (?)', params[:aasm_state])
    end

    if !params[:customer_name].blank?
      scope = scope.joins(:address).where('addresses.name Like ?', "%#{params[:customer_name]}%")
    end

    if !params[:sub_locality].blank?
      scope = scope.joins(:address).where('addresses.sub_locality Like ?', "%#{params[:sub_locality]}%")
    end

    if !params[:locality].blank?
      scope = scope.joins(:address).where('addresses.locality Like ?', "%#{params[:locality]}%")
    end

    if !params[:date].blank?
      scope = scope.where('pickup_requests.created_at > ?', "#{Date.parse(params[:date])}")
    end

    if !params[:scheduled_date_to].blank?
      scope = scope.where('pickup_requests.scheduled_date <= ?', Time.zone.at(params[:scheduled_date_to].to_i/1000).to_date)
    end

    if !params[:scheduled_date_from].blank?
      scope = scope.where('pickup_requests.scheduled_date >= ?', Time.zone.at(params[:scheduled_date_from].to_i/1000).to_date)
    end

    if !params[:out_for_pickup].blank?
      scope = scope.joins(:pickup_request_state_histories).where('pickup_request_state_histories.state = "out_for_pickup" and pickup_request_state_histories.created_at > ? and pickup_request_state_histories.created_at < ? ', Time.zone.at(params[:out_for_pickup].to_i/1000).to_date + 0.hours, Time.zone.at(params[:out_for_pickup].to_i/1000).to_date+24.hours)
    end

    if !params['pickup_boy_id'].blank?
      scope = scope.where('pickup_requests.user_id = ?', params['pickup_boy_id'].to_i)
    end

    if !params[:user_id].blank?
      scope = scope.where('pickup_requests.user_id = ?', params[:user_id])
    end

    if !params[:client_id].blank?
      scope = scope.where('pickup_requests.client_id = ?', get_client_id(params[:client_id]))
    end

    if !params[:hub_id].blank?
      scope = scope.where('pickup_requests.hub_id = ?', params[:hub_id])
    end

    if !params[:remarks].blank?
      scope = scope.where('pickup_requests.remarks like ?', "%#{params[:remarks]}%")
    end

    if !params[:startDate].blank? && !params[:endDate].blank?
      scope = scope.where(created_at: (Date.parse(params[:startDate]) + 1 + 0.hours)..(Date.parse(params[:endDate]) + 2+0.hours))
    end

    if  (params.has_key?('paginate') && params[:paginate].to_bool)
      scope = scope.page(params[:page]|| 1).order_scope(params[:relatedAttribute],params[:sortDir],params[:sortedBy])
    end

    scope
  end

  def self.get_client_id(client_id)
    if client_id.is_i?
      client = Client.find(client_id)
    else
      client = Client.find_by(api_name:client_id)
    end
    client.id
  end

  def add_state_history
    if aasm_state_changed?
      if aasm_state == 'out_for_pickup'
        pickup_request_state_histories.build(:state => aasm_state,current_location_id: current_location_id,current_location_type: current_location_type, :user_id => user_id, manifest:manifest,comment:receiving_remarks).save
      elsif aasm_state == 'undelivered'
        pickup_request_state_histories.build(:state => aasm_state,current_location_id: current_location_id,current_location_type: current_location_type, :comment => remarkz).save
      else
        pickup_request_state_histories.build(state: aasm_state,current_location_id: current_location_id,current_location_type: current_location_type, manifest: manifest,comment:receiving_remarks).save
      end
    end
  end

  def customer_details
    return "#{address.name}-(#{client_request_id})"+"\n"+"#{address.phone_number}"
  end

  def address_details
    return "#{address.address_line}"+"\n"+"#{address.city},Pincode: #{address.pincode}"
  end

  def complete_address
    return "#{address.name}"+"\n"+ address_details + "\n" + "#{address.phone_number}"
  end

  def product_details
    str = ''
    skus.each do |sku|
      str = str + "#{sku.name}"+"\n"
    end
    str
  end

  def items
    skus.size
  end

  def seller_present?
    seller.present? && seller.valid?
  end

  def local?
    if seller
      dp = DeliveryPincode.find_by(pincode:seller.pincode,delivery_center:current_location)
      if dp
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def dispatch?
    if seller
      return !local? && final_destination == current_location
    else
      return false
    end
  end

  def picked_items
    skus.count(picked:true)
  end

  def init_address
    build_address if new_record?
  end

  def assign_final_destination
    self.final_destination = Utilities::RequestDestinationDecider.new(self).final_destination
    self
  end


  def assign_awb_and_dsp
    if seller_present?
      assign_dsp
      assign_awb
    end
  end

  def assign_dsp_local
    assign_dsp if seller_present?
    assign_awb if seller_present?
  end

  def update_cl
    if current_location.nil? && hub.present?
      update_column(:current_location_id,hub.id)
      update_column(:current_location_type,hub.class.name)
    end
  end


  def assign_hub
    local_pincode = Pincode.find_by(code:address.pincode)
    self.hub_id = local_pincode.hub.id if local_pincode && local_pincode.hub
  end

  def assign_awb
    if dsp
      client_dsp  = ClientDsp.where(client:get_client,dsp:dsp).first
      if client_dsp
        awb = AwbNumber.where(client_dsp:client_dsp,used: false).first
        if awb
          awb.update(used:true)
          self.awb_number = awb
        end
      end
    end
  end

  def get_client
    local? ? Client.find_by(name: 'Pickingo') : client
  end

  def assign_dsp
    self.dsp = Utilities::DspAssigner.new(self).dsp
  end


  def picked_date
    if aasm_state == 'picked'
      status_history = pickup_request_state_histories.find_by(state:'picked')
      (status_history.created_at.to_date.to_time.to_i)*1000
    else
      nil
    end
  end

  def attempts
    pickup_request_state_histories.where(state:'out_for_pickup').size - pickup_request_state_histories.where(state:'not_attempted').size
  end

  def sku_presence
    errors.add(:skus,'No SKU\'s Present' ) unless skus.length > 0
  end

  def set_schedule_date
    update(:scheduled_date => Time.zone.now.to_date) unless scheduled_date
  end

  def revert_status
    if ['assigned','picked','received','cancelled','not_attempted','cid','not_contactable'].include? aasm_state
      transaction do
        pickup_request_state_histories.order(:id).last.destroy
        if aasm_state == 'received'
          skus.update_all(:picked => false)
        end
        update_columns(:aasm_state => pickup_request_state_histories.order(:id).last.state, :updated_at => Time.now, :scheduled_date => Time.zone.now.to_date,:picked_on => nil)
      end
    end
  end

  def self.strike_rate
    new_pickups = PickupRequestStateHistory.where(:state => :new ).to_sql
    same_day_pickups = PickupRequestStateHistory.where(:state => :received ).joins("inner join (#{new_pickups}) new_pickups on pickup_request_state_histories.pickup_request_id = new_pickups.pickup_request_id").where("Date(new_pickups.created_at) = Date(pickup_request_state_histories.created_at)").count
    total_pickups = PickupRequestStateHistory.where(:state => :received).size
    (same_day_pickups.to_f*100/total_pickups).round(2)
  end

  def self.closing_time
    new_pickups = PickupRequestStateHistory.where(:state => :new ).to_sql
    PickupRequestStateHistory.where(:state => :closed ).joins("inner join (#{new_pickups}) new_pickups on pickup_request_state_histories.pickup_request_id = new_pickups.pickup_request_id").select("avg(TIMESTAMPDIFF(HOUR, new_pickups.created_at,pickup_request_state_histories.created_at)) as value").to_a.first.value
  end

  def self.summary(date)
    PickupRequestStateHistory.joins(:pickup_request).where("pickup_request_state_histories.created_at >= ? and pickup_request_state_histories.created_at < ?", Time.zone.at(date.to_i/1000).to_date + 0.hours, Time.zone.at(date.to_i/1000).to_date + 1+ 0.hours).select("state, client_id,count(state) as count").group(:state, :client_id).to_a
  end

  def self.get_requests_type(requests, type)
    selected_requests = []
    requests.each do |request|
      if !request.returned_to_client? && request.local?
        selected_requests << request if type == 'local'
      elsif request.dispatch?
        selected_requests << request if type == 'dispatch'
      else
        selected_requests << request if type == 'ship'
      end
    end
    selected_requests
  end

  private
  def remove_manifest
    self.manifest = nil
  end


end
