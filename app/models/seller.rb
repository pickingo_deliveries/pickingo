class Seller < ActiveRecord::Base

  belongs_to :pickup_request
  belongs_to :pincode
  validates :name, :pincode_id, :address_line, presence: true


  def address_details
    #h = {phone => "", name => "", city =>"", pincode.code => "" }
    #short_address = address_line.gsub(/\w+/) { |m| h.fetch(m,m)}
    short_address = address_line.gsub(phone,"").gsub(name,"").gsub(city,"").gsub(pincode.code,"")
    return "#{name}"+"\n"+"#{short_address}"+"\n"+"#{city},Pincode: #{pincode.code},"+"\n"+"Contact No. : #{phone}"
  end

end
