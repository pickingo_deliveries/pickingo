class Attendance < ActiveRecord::Base
  belongs_to :user

  has_attached_file :start_pic,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol    => 'https',
                    :url =>':s3_domain_url',
                    :path => "pickup_boy/activity/files/:attachment/:id/:basename-:style.:extension"

  has_attached_file :end_pic,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol    => "https",
                    :url =>':s3_domain_url',
                    :path =>"pickup_boy/activity/files/:attachment/:id/:basename-:style.:extension"

  #validates_attachment_content_type :start_pic, :content_type => /\Aimage/
  #validates_attachment_content_type :end_pic, :content_type => /\Aimage/
  #validates_attachment_file_name :start_pic, :matches => [/png\Z/, /jpe?g\Z/]
  #validates_attachment_file_name :end_pic, :matches => [/png\Z/, /jpe?g\Z/]

  do_not_validate_attachment_file_type :start_pic
  do_not_validate_attachment_file_type :end_pic

  def check_in
    if self.check_out_time.nil? && self.check_in_time.nil?
      if self.update(check_in_time: Time.zone.now)
        self.user.check_in!
        return {message:'Checked in Successfully', status: 200}
      else
        return {message:'There were some problems, please try again', status: 500}
      end
    elsif !self.check_in_time.nil? && self.check_out_time.nil?
      return {message:'You have already checked in!!', status: 400}
    elsif !self.check_in_time.nil? && !self.check_out_time.nil?
      return {message:'You have already checked out for today', status: 400}
    else
      return {message:'You need to check out before checking in!!', status: 400}
    end
  end

  def check_out
    if !self.check_in_time.nil? && self.check_out_time.nil? && (self.start_km.nil? || !self.end_km.nil?)
      if self.update(:check_out_time=>Time.zone.now)
        self.user.check_out!
        return {message: 'Checked out Successfully', status: 200}
      else
        return {message: 'There were some problems, please try again', status: 500}
      end
    elsif !self.check_out_time.nil?
      return {message: 'You have already checked out for today!!', status: 400}
    elsif self.check_in_time.nil?
      return {message: 'You need to check in before checking out!!', status:400}
    else
      return {message: 'You need to end the trip before checking out!!', status:400}
    end
  end

  def start_trip(params)
    if self.check_in_time.nil?
      return {message: 'You need to check in before the trip!!', status: 400}
    end

    if self.start_km.nil? && self.end_km.nil?
      if self.update(start_km: params[:start_km].to_f, start_pic: params[:start_pic])
        self.user.trip_begin!
        return {message: 'Start km entered successfully!'}
      else
        return {message: 'There were some problems, please try again', status: 500}
      end
    elsif self.start_km.nil?
      return {message: 'Please reset your trip to continue!!', status: 400}
    elsif self.end_km.nil?
      return {message: 'You have already started your trip!!', status: 400}
    else
      return {message: 'You have already finished your trip for today!!', status: 400}
    end
  end

  def end_trip(params)
    if self.check_in_time.nil?
      return {message: 'You need to check in before the trip!!', status: 400}
    end

    if !self.start_km.nil? && self.end_km.nil?
      if self.start_km <= params[:end_km].to_f && self.update(end_km: params[:end_km].to_f, end_pic: params[:end_pic])
        self.user.trip_end!
        return {message: 'End km entered successfully!', status: 200}
      elsif self.start_km > params[:end_km].to_f
        return {message: "End km can't be less than start km", status: 400}
      else
        return {message: 'There were some problems, please try again', status: 500}
      end
    elsif self.start_km.nil?
      return {message: 'You need to enter the start km before ending the trip', status: 400}
    elsif !self.end_km.nil?
      return {message: 'You have already finsihed your trip for today!!', status: 400}
    end
  end

end
