class DispatchCenterMasterManifest < ActiveRecord::Base
  include AASM

  belongs_to :origin , :class_name => 'DispatchCenter'
  belongs_to :destination, :class_name => 'ClientWarehouse'
  belongs_to :client
  belongs_to :dc_vehicle
  has_many :dispatch_center_manifests
  has_many :dispatch_center_master_manifest_histories

  has_attached_file :pdf_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  validates_attachment_content_type :pdf_file, :content_type => ['application/pdf']

  has_attached_file :excel_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  #validates_attachment_content_type :excel_file, :content_type => ['application/xlsx', 'application/vnd.ms-excel']
  do_not_validate_attachment_file_type :excel_file

  has_attached_file :request_excel_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  #validates_attachment_content_type :excel_file, :content_type => ['application/xlsx', 'application/vnd.ms-excel']
  do_not_validate_attachment_file_type :request_excel_file

  validates :origin_id, :destination_id, presence: true
  validate :dispatch_center_manifests_presence
  after_create :update_dc_manifests

  before_save :generate_uid, :if => :new_record?

  scope :incoming_manifests, ->  (destination){ where(destination:destination, state:'in_transit') }


  def dispatch_center_manifests_presence
    errors.add(:dispatch_center_manifests, 'No Dispatch Center Manifests Present') unless dispatch_center_manifests.length > 0
  end

  aasm :column => :state , :no_direct_assignment => true do
    state :new, :initial => true
    state :in_transit
    state :cancelled
    state :closed

    event :transit, :after => :dc_manifests_transit do
      transitions :from => [:new], :to => [:in_transit]
    end

    event :cancel, :before => :dc_manifests_cancel, :after => :log_history do
      transitions :from => [:new], :to => [:cancelled]
    end

    event :close, :after => :dc_manifests_close do
      transitions :from => [:in_transit], :to => [:closed]
    end
  end

  def self.search_with_params(params)
    scope = self.all
    scope = scope.where(state: params[:state]) if params[:state].present?
    scope = scope.where('uid LIKE ?', "%#{params[:uid]}%") if params[:uid].present?
    scope
  end

  def dc_requests_close(exclude_requests)
    dispatch_center_manifests.each do |manifest|
      manifest.request_delivered(exclude_requests)
      manifest.request_damaged(exclude_requests)
    end
  end

  def self.create_dup_master(origin, hub_master_manifest)
    client_id, destination_id, dc_manifest_ids = DispatchCenterManifest.create_dup_manifest(origin, hub_master_manifest.hub_manifests)
    self.create(origin_id: origin, destination_id: destination_id, client_id: client_id,
         pdf_file: hub_master_manifest.pdf_file, excel_file: hub_master_manifest.excel_file,
         dispatch_center_manifest_ids: dc_manifest_ids)
  end

  private

  def update_dc_manifests
    ActiveRecord::Base.transaction do
      Delayed::Job.enqueue(Jobs::DcMasterManifestPdfGenerateJob.new(id))
      Delayed::Job.enqueue(Jobs::DcMasterManifestExcelGenerateJob.new(id))
      Delayed::Job.enqueue(Jobs::DcMasterRequestExcelGenerateJob.new(id))
      begin
        dispatch_center_manifests.update_all(dispatch_center_master_manifest_id: id)
        dispatch_center_manifests.each(&:assign_master!)
      rescue Exception => ex
        if e.message =~ /Deadlock found when trying to get lock/
          retries += 1
          raise ex if retries > 3  ## max 3 retries
          sleep 10
          retry
        else
          raise ex
        end
      end
    end

  end

  def dc_manifests_transit
    dispatch_center_manifests.each(&:master_transition!)
    log_history
  end

  def dc_manifests_close
    dispatch_center_manifests.each(&:close!)
    log_history
  end


  def dc_manifests_cancel
    dispatch_center_manifests.each(&:master_cancel!)
  end

  def log_history
    manager.send(logger_method)
  end

  def logger_method
    event = aasm.current_event.to_s
    event.scan(/[a-zA-Z_]/).join('')
  end

  def manager
    HistoryManagers::DcMasterManifestHistoryManager.new(self)
  end

  def generate_uid
    generated_uid = origin.identifier+'-Master-'+(100_000 + Random.rand(1_000_000 - 100_000)).to_s
    if DispatchCenterMasterManifest.exists?(uid:generated_uid)
      generate_uid
    else
      self.uid = generated_uid
    end
  end


end
