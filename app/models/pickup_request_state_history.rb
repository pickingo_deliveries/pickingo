class PickupRequestStateHistory < ActiveRecord::Base
  belongs_to :user
  belongs_to :pickup_request
  belongs_to :current_location, polymorphic: true
  belongs_to :manifest , polymorphic: true
  has_one :sm, :dependent => :destroy

  after_save :create_sms

  def create_sms
    if ['picked','cancelled','out_for_pickup','received'].include? state
      sms = build_sm(:phone => pickup_request.address.phone_number,:content => sms_content)
      sms.save
    end
  end

  def sms_content
    # case state
    #   when 'picked'
    #     return "Hi, your #{pickup_request.client.name} return pickup was successfully done from your premises today. Pickingo Customer support: 8800226694"
    #   when 'cancelled'
    #     return "Hi, as per your consent, your #{pickup_request.client.name} reverse pickup has been cancelled. Pickingo Customer support: 8800226694"
    #   when 'out_for_pickup'
    #     return "Hi, your #{pickup_request.client.name} return pickup (ID: #{pickup_request.client_request_id}) is scheduled for pickup up today by #{pickup_request.user.try(:user_name)} (#{pickup_request.user.try(:phone)}). Pickingo Customer support: 8800226694"
    #   else
    #     return ""
    # end

    case state
      when 'received'
        return "Hi! Your Order #{pickup_request.client_order_number} from #{pickup_request.client.name} has been successfully picked up by #{pickup_request.user.try(:user_name)}"
      when 'picked'
        return "Hi! Your Order #{pickup_request.client_order_number} from #{pickup_request.client.name} has been successfully picked up by #{pickup_request.user.try(:user_name)}"
      when 'cancelled'
        return "Hi! Pickup of your Order #{pickup_request.client_order_number} from #{pickup_request.client.name} has been cancelled. Feel free to call us at +91 #{pickup_request.user.try(:phone)} (#{pickup_request.user.try(:user_name)})"
      when 'out_for_pickup'
        return "Hi! We will pick up your Order #{pickup_request.client_order_number} from #{pickup_request.client.name} today. Please keep the item ready.  You may call us at +91 #{pickup_request.user.try(:phone)} (#{pickup_request.user.try(:user_name)}). Pickingo Customer support: 8800226694"
      else
        return ""
    end
  end

end
