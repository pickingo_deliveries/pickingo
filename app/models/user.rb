class User < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User
  include AASM

  belongs_to :role
  belongs_to :entity, polymorphic: true, :touch => true
  has_one :user_profile
  belongs_to :vendor
  has_many :pickup_requests
  has_many :pickup_request_state_histories, :through => :pickup_requests
  has_many :runsheets
  has_many :barcode_sheets
  has_many :attendances
  has_one :wallet, as: :entity
  has_many :transactions_as_reciever,:class_name => "Transaction", :foreign_key => "reciever_id"
  has_many :transactions_as_sender, :class_name => "Transaction", :foreign_key => "sender_id"

  has_many :asset_transitions
  has_many :assets, as: :entity

  before_validation :ensure_authentication_token
  before_save -> { skip_confirmation! }
  after_save :add_employee_code

  validates :role, presence: true
  validates :entity, presence: true, unless: :admin?

  validate :unique_email_user
  accepts_nested_attributes_for :user_profile

  delegate :role_name, :to => :role, :allow_nil => true
  delegate :phone, :to => :user_profile, :allow_nil => true
  delegate :full_name, :to => :user_profile, :allow_nil => true

  scope :role_users, -> (role_name) {joins('roles').where('roles.name = ?', role_name)}


  default_scope { where("users.status != 'Resigned'") }

  scope :role_users, -> (role_name) {joins('roles').where('roles.name = ?', role_name)}

  STATUS = {active: 'active', inactive: 'inactive', resigned: 'resigned'}

  STATUS.each do |k, v|
    define_method("#{k}?") do
      attributes['status'] == v
    end
  end

  Role.all.each do |role|
    define_method("#{role.name.downcase.split.join('_')}?") do
      role_name == role.name
    end
  end

  aasm do
    state :not_checked_in, :initial => true
    state :checked_in
    state :checked_out
    state :not_checked_out # if boy do not checkout before midnight
    state :started_trip
    state :finished_trip

    event :check_in do
      transitions :from => [:not_checked_in, :checked_out], :to => :checked_in
    end

    event :check_out do
      transitions :from => [:checked_in,:not_checked_out, :finished_trip, :started_trip], :to => :checked_out
    end

    event :trip_begin do
      transitions :from =>  :checked_in , :to => :started_trip
    end

    event :trip_end do
      transitions :from => :started_trip , :to => :finished_trip
    end
  end

  def generate_runsheet(requests)
    pdf = RunsheetPdf.new(requests, full_name)
    filename = Rails.root.join('public/pdfs', "#{full_name}_#{Time.now.getutc}.pdf")
    pdf.render_file(filename)
    pdf_file = File.open(filename)
    runsheets.build(file:pdf_file,pickup_requests:requests)
    File.delete(filename)
  end

  def generate_barcode_sheet(requests)
    pdf = BarcodePdf.new(requests,full_name)
    filename = Rails.root.join('public/pdfs', "#{full_name}_barcode_#{Time.now.getutc}.pdf")
    pdf.render_file(filename)
    pdf_file = File.open(filename)
    barcode_sheets.build(file:pdf_file,pickup_requests:requests)
    File.delete(filename)
  end

  def active_for_authentication?
    super && self.active? && self.active
  end

  def unique_email_user
    if provider == 'email' and self.class.where.not(id: self.id).where(provider: 'email', email: email).size > 0
      errors.add(:email, "is already in use")
    end
  end

  def reset_requests
    requests = pickup_requests.where(aasm_state: ['cid', 'not_contactable','not_attempted'])
    requests.each do |request|
      request.update_columns(user_id: nil, aasm_state: 'new')
      request.pickup_request_state_histories.build(state: 'new', comment: 'Pickup boy transferred to another hub').save
    end
  end

  def asset_summary
    {
        total_assets: Asset.all.size,
        assigned_count: Asset.where.not(user_id: nil).size,
        unassigned_count: Asset.where(user_id: nil).size,
        damaged_assets_count: Asset.where(state: 'damaged').size,
        under_repair_assets_count: Asset.where(state: 'under repair').size,
        items: asset_count
    }
  end

  def asset_count
    items_list = []
    ['phone','bag','t-shirt','sim','other'].each do |asset_type|
      asset = Asset.asset_type_filter(asset_type)
      items_list << { item_type: asset_type, total_count:asset.size, assigned_count:asset.assigned.size,
                      unassigned_count:asset.unassigned.size }

    end
    return items_list
  end

  private

  def ensure_authentication_token
    unless id
      self.uid = email if uid.blank?
      self.password = 'pickingo' if self.password.nil?
    end
    self.status = self.status.downcase if self.status
  end

  def add_employee_code
    if self.code.nil? && self.entity_id && role.short_name
      self.code = entity.identifier.first(5).upcase + role.short_name + sprintf("%05d",self.id)
      self.save
    end
  end

  def self.search_with_params(params)
    scope = self.all
    scope = scope.where(entity_type: params[:entity_type], entity_id: params[:entity_id]) if params[:entity_id].present?
    scope = scope.joins(:user_profile).where('user_profiles.first_name LIKE :name or
        user_profiles.middle_name LIKE :name or user_profiles.last_name LIKE :name',
                                             {name: "%#{params[:name]}%"}) if params[:name].present?
    scope = scope.joins(:user_profile).where('user_profiles.company_number LIKE :phone',
                                             {phone: "%#{params[:phone]}%"}) if params[:phone].present?
    scope = scope.where(role_id: params[:role_id]) if params[:role_id].present?
    scope = scope.joins(:role).where("roles.name IN (?)", params[:role]) if params[:role].present?
    scope = scope.where(status: params[:status].downcase) if params[:status].present?
    scope = scope.joins(:user_profile).order("user_profiles.#{params[:sort_col]} desc") if params[:sort_col].present?
    scope
  end

end