class Role < ActiveRecord::Base

  has_many :crm_users

  has_many :users

  belongs_to :department

  def role_name
    self.name
  end
end
