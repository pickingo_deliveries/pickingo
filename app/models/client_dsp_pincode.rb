class ClientDspPincode < ActiveRecord::Base
  belongs_to :client_dsp
  belongs_to :pincode

  validates :client_dsp_id, :pincode_id, presence:true
end
