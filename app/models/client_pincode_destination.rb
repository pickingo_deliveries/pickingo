class ClientPincodeDestination < ActiveRecord::Base

  belongs_to :client
  belongs_to :pincode
  belongs_to :dispatch_center


  validates :client_id, :pincode_id, :dispatch_center_id, presence: true

end
