class SellerLocalManifestHistory < ActiveRecord::Base
  belongs_to :seller_local_manifest

  validates :state, :seller_local_manifest_id, :remarks, presence: true
end
