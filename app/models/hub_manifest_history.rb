class HubManifestHistory < ActiveRecord::Base

  belongs_to :hub_manifest

  validates :state, :hub_manifest, :remarks, presence: true

end
