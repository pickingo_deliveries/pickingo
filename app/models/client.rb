class Client < ActiveRecord::Base
  include Metrics

  has_many :client_warehouses

  has_many :dispatch_center_manifests

  has_many :pickup_requests
  has_many :pickup_request_state_histories, :through => :pickup_requests

  has_many :client_pincode_destinations

  has_many :client_dsps
  has_many :dsps, :through => :client_dsps

  validates :name,:initials, presence: true

  before_save -> { self.api_name = self.name.underscore }


end
