require 'prawn'
require 'prawn/table'
class RunsheetPdf < Prawn::Document
  
  def initialize(requests,name)
    super(:left_margin => 20, :right_margin =>20 )
    @requests = requests
    @name = name
    header
    text_content
    table_content
  end

  def header
    image "#{Rails.root}/app/assets/images/pickingo.png", width: 200, height: 50
  end

  def text_content
    y_position = cursor - 50
    bounding_box([0, y_position], :width => 580, :height => 30) do
      text "Runsheet for #{@name} #{Date.today.strftime("%B %d, %Y")} #{Time.now.in_time_zone('Asia/Kolkata').strftime("%I:%M%p")}", size: 13, style: :bold
    end
  end

  def table_content
    # This makes a call to request_rows and gets back an array of data that will populate the columns and rows of a table
    # I then included some styling to include a header and make its text bold. I made the row background colors alternate between grey and white
    # Then I set the table column widths
    table request_rows do
      row(0).font_style = :bold
      self.header = true
      self.row_colors = ['DDDDDD', 'FFFFFF']
      self.cell_style = { size: 9 }
      self.column_widths = [20,60,80,120,110,20,50,60,50]
      # self.column_widths = [20,60,100,130,110,20,60,70]
    end
  end

  def request_rows
    [['#', 'Client', 'Customer Details','Address', 'Products', 'Items','Remarks','CID Remarks','Acknowledgement']] +
        @requests.each_with_index.map do |request,index|
          cid_remarks = get_cid_remarks(request)
          [index+1, request.client.name+"\n"+request.client_order_number ,encode_1252(request.customer_details), encode_1252(request.address_details), encode_1252(request.product_details), item_count(request),encode_1252(request.remarks),encode_1252(cid_remarks),'']
        end
  end

  def get_cid_remarks request
    if request.pickup_request_state_histories.present?
      cid_states = request.pickup_request_state_histories.where(state: 'cid')
      return (cid_states.last.comment.present? ? cid_states.last.comment : 'N/A')  if cid_states.any? #if request.pickup_request_state_histories.last.comment.present?
    end
    return 'N/A'
  end

  def encode_1252(string)
    if string
      string.encode('windows-1252', :invalid => :replace, :undef => :replace, :replace => '')
    else
      string
    end
  end

  def item_count(request)
    if request.skus.first.client_sku_id == 'Nil'
      'NA'
    else
      request.items
    end
  end


end