class Transaction < ActiveRecord::Base
	belongs_to :wallet
	belongs_to :reciever, :class_name => "User", :foreign_key => "reciever_id"
	belongs_to :sender, :class_name => "User", :foreign_key => "sender_id"
	belongs_to :hub
	belongs_to :expense

	validate :id_is_allowed

	validates :amount, numericality: true


	def id_is_allowed
		if sender_id.present?
			unless User.find(sender_id).present? && User.find(reciever_id).present?
				errors.add(:reciever_id, "reciever is not found")
			end
		else
			unless User.find(reciever_id).present?
				errors.add(:reciever_id, "reciever is not found")
			end
		end
	end

	def self.search_with_params(params)
	    scope = self.all
	    scope = scope.where(reciever_id: params[:reciever_id]) if params[:reciever_id].present?
	    scope = scope.where(hub_id: params[:hub_id]) if params[:hub_id].present?
	    scope = scope.where(transaction_type: params[:transaction_type]) if params[:transaction_type].present?
	    scope
	end

	def wallet_add(wallet_from_id, from_type, wallet_to_id, to_type, tr_amount)
		wallet_from = Wallet.where(entity_id: wallet_from_id, entity_type: from_type).first if wallet_from_id.present?
		wallet_to = Wallet.where(entity_id: wallet_to_id, entity_type: to_type).first if wallet_to_id.present?
		wallet_from.amount = wallet_from.amount - tr_amount.to_i if wallet_from_id.present?
        wallet_to.amount = wallet_to.amount + tr_amount.to_i if wallet_to_id.present?
        if wallet_from_id.present? && wallet_from.amount < -5000
        	raise 'error'
        end
        if wallet_to_id.present? && wallet_to.amount < -5000
        	raise 'error'
        end
    	Wallet.update(wallet_from.id, :amount => wallet_from.amount) if wallet_from_id.present?
    	Wallet.update(wallet_to.id, :amount => wallet_to.amount) if wallet_to_id.present?
    end
end
