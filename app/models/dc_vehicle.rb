class DcVehicle < ActiveRecord::Base

  belongs_to :dispatch_center
  has_many :dispatch_center_manifests
  validates :reg_number, presence: true

end
