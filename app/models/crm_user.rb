class CRMUser < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  belongs_to :role
  belongs_to :department

  has_many :tickets, :foreign_key => 'owner_id', :class_name => 'Ticket'

  has_many :tickets, :foreign_key => 'assigned_to_id', :class_name => 'Ticket'

  validates :role_id, presence: true

  before_validation :sanitize_role

  def sanitize_role
    if new_record?
      self.uid = email if uid.blank?
      self.role = self.department.role
      self.role_string = role.name
    end
  end
end
