class ClientWarehouse < ActiveRecord::Base

  has_many :ending_manifests, :foreign_key => 'destination_id', :class_name => 'DispatchCenterManifest'

  belongs_to :client

  validates :client_id, presence: true

  before_create :assign_name

  private

  def assign_name
    self.name = client.name + ' Warehouse' if name.blank?
  end
end
