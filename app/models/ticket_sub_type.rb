class TicketSubType < ActiveRecord::Base

  belongs_to :ticket_type

  has_many :tickets

  validates :name, presence: true
  validates :ticket_type, presence: true

  before_save :sanitize_name

  def sanitize_name
    self.display_name = name.titleize
  end
end
