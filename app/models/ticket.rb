class Ticket < ActiveRecord::Base
  include AASM

  acts_as_commentable

  belongs_to :ticket_type
  belongs_to :ticket_sub_type

  belongs_to :owner, :class_name => 'CRMUser'
  belongs_to :assignee, :class_name => 'Department'
  belongs_to :assigned_to, :class_name => 'CRMUser'

  belongs_to :pickup_request

  validates :title ,:ticket_type_id, :ticket_sub_type_id ,:priority, presence: true

  before_validation :generate_title

  before_save :generate_uid

  attr_accessor :remarks


  enum state: {
           open: 1,
           assigned: 2,
           in_progress: 3,
           on_hold: 4,
           closed: 5
       }
  aasm :column => :status , :no_direct_assignment => true do
    state :open, :initial => true, :after_enter =>:set_state
    state :assigned, :after_enter =>:set_state
    state :in_progress, :after_enter =>:set_state
    state :on_hold, :after_enter =>:set_state
    state :closed, :after_enter =>:set_state

    event :assign do
      transitions :from => [:open], :to => [:assigned], guard: :can_assign?
    end

    event :progress do
      transitions :from => [:assigned, :on_hold], :to => [:in_progress]
    end

    event :hold do
      transitions :from => [:in_progress,:assigned], :to => [:on_hold]
    end

    event :close do
      transitions :from => [:open,:on_hold,:in_progress], :to => [:closed]
    end
  end

  def set_state
    self.statefield = Ticket.states[self.status]
  end

  def can_assign?
    assigned_to.present?
  end

  def add_comment
    comments.create!(title:'created the ticket', crm_user_id:owner.id, status:status)
  end


  searchable do
    text :title
    text :description
    text :uid
    text :ticket_type do
      ticket_type.name
    end
    text :owner do
      owner.name
    end
    time :created_at
    time :updated_at
    text :status
    string :status
    integer :statefield
    time(:priority_to_time){DateTime.new(2014, 1, 1, 0, 0, 0) + self.statefield.to_f}
  end

  def self.ticket_summary params
    if (!params[:start_date].nil? && !params[:end_date].nil?)
        start_date = params[:start_date].to_date
        end_date = params[:end_date].to_date
        tickets = self.where(created_at: start_date..end_date)
        {
            tickets: tickets.collect {|t| TicketSerializer.new(t)},
            total_count: tickets.count,
            open_tickets_count: self.where(created_at: start_date..end_date,status:"open").count,
            closed_tickets_count: self.where(created_at: start_date..end_date,status:"closed").count
        }

    else
        {
            total_count: self.count,
            open_tickets_count: self.where(status:"open").count,
            closed_tickets_count: self.where(status: "closed").count
        }
    end
  end


  private

  def generate_uid
    generated_uid = self.ticket_type.identifier+(100_000_000 + Random.rand(1_000_000_000 - 100_000_000)).to_s
    if Ticket.exists?(uid:generated_uid)
      generate_uid
    else
      self.uid = generated_uid
    end
  end

  def generate_title
    if self.pickup_request
      self.title = "Request Against #{self.pickup_request.client_request_id} raised by #{self.owner.name}"
    else
      self.title = "Query Request raised by #{self.owner.name}"
    end
  end




end
