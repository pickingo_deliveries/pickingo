class AwbNumber < ActiveRecord::Base
  belongs_to :client_dsp
  belongs_to :pickup_request
  validates :code, presence: true ,uniqueness: {message: 'already exists'}
end
