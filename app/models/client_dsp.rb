class ClientDsp < ActiveRecord::Base
  belongs_to :client
  belongs_to :dsp

  validates :client_id, :dsp_id, presence: true
end
