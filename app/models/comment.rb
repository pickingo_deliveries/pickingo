class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment

  belongs_to :commentable, :polymorphic => true

  default_scope -> { order('created_at ASC') }

  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_voteable

  # NOTE: Comments belong to a user
  belongs_to :crm_user

  # after_create :push_notification
  #
  # def push_notification
  #   Pusher.trigger('comments', 'created', CommentSerializer.new(self).to_json)
  # end
end
