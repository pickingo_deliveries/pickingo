class SellerLocalManifest < ActiveRecord::Base

  include AASM

  has_many :pickup_requests, as: :manifest
  has_many :seller_local_manifest_histories
  belongs_to :dsp
  belongs_to :origin, polymorphic: true

  has_attached_file :pdf_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  validates_attachment_content_type :pdf_file, :content_type => ['application/pdf']

  has_attached_file :excel_file,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/secrets.yml",
                    :s3_protocol => 'https',
                    :url =>':s3_domain_url',
                    :path => 'manifests/files/:basename-:style.:extension'
  #validates_attachment_content_type :excel_file, :content_type => ['application/xlsx', 'application/vnd.ms-excel']
  do_not_validate_attachment_file_type :excel_file

  validates :weight, presence: true
  validate :pickup_requests_presence

  def pickup_requests_presence
    errors.add(:pickup_requests, 'No Pickup Requests Present') unless pickup_requests.length > 0
  end

  before_save :generate_uid, :if => :new_record?

  after_create :update_requests


  aasm :column => :state , :no_direct_assignment => true do
    state :new, :initial => true
    state :with_dsp
    state :closed

    event :deliver, :after => :manifest_deliver do
      transitions :from => [:new], :to => [:with_dsp], :guard => :can_deliver?
    end

    event :close, :after => :manifest_close do
      transitions :from => [:with_dsp], :to => [:closed]
    end

    event :shut do
      transitions :from => [:with_dsp], :to => [:closed]
    end

    event :revert, :before => :revert_requests do
      transitions :from => [:closed], :to => [:with_dsp]
    end

  end

  def revert_requests
    pickup_requests.each do |pr|
      pr.update_columns(aasm_state: pr.pickup_request_state_histories.last.state)
    end
  end


  def update_requests
    Delayed::Job.enqueue(Jobs::SellerLocalManifestPdfJob.new(id))
    Delayed::Job.enqueue(Jobs::SellerLocalManifestExcelJob.new(id))
    pickup_requests.each(&:manifest_assign!)
    manager.create
  end

  def manifest_deliver
    pickup_requests.each(&:manifest_transition!)
    log_history
  end

  def manifest_close
    pickup_requests.each(&:delivers)
    log_history
  end

  def can_deliver?
    tracking_id.present?
  end

  def log_history
    manager.send(logger_method)
  end

  def logger_method
    close_requests if state=='closed'
    event = aasm.current_event.to_s
    event.scan(/[a-zA-Z]/).join('')
  end

  def close_requests
    pickup_requests.update_all(aasm_state:'closed')
  end

  def manager
    HistoryManagers::SellerLocalHistoryManager.new(self)
  end

  def generate_uid
    generated_uid = origin.identifier + '-local-' + (100_000 + Random.rand(1_000_000 - 100_000)).to_s
    if SellerLocalManifest.exists?(uid:generated_uid)
      generate_uid
    else
      self.uid = generated_uid
    end
  end

  def check_closed_req
    temp = true

    pickup_requests.each do |req|
      temp = false  unless req.closed?
    end

    return temp
  end

end
