require 'nokogiri/xml/builder'

class SmsXml

  def initialize(smses)
    @smses = smses
    @username = Rails.application.secrets.sms_username
    @password = Rails.application.secrets.sms_password
  end

  def build_xml
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.cil {
        xml.uid @username
        xml.pwd @password
        xml.senderid 'pickingo'
        xml.smses{
          @smses.each do |sms|
            xml.sms{
              xml.mobile sms.phone
              xml.message sms.content
            }
          end
        }
      }
    end
    builder.to_xml
  end


end