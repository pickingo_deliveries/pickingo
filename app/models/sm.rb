class Sm < ActiveRecord::Base
  belongs_to :pickup_request_state_history

  # delegate :pickup_request, :to => :pickup_request_state_history, :allow_nil => true

  def self.auto_trigger_sms history_ids
    count = 0
    failed_ids = []
    # smses = Sm.where({id:params[:sms_ids]})
    smses = Sm.where({pickup_request_state_history_id: history_ids })
    xml = SmsXml.new(smses)
    xml_file = xml.build_xml
    begin
      Api::Sms.send_multiple(xml_file)
      smses.update_all(sent:true)
    rescue Exception => e
      Airbrake.notify_or_ignore(e, parameters: {message:"SMS message failed"})
      #failed_ids << request.id
      count = count+1
      puts e
    end
    # render json: {message:"#{smses.count - count} sms sent", failed: failed_ids}
  end

end
