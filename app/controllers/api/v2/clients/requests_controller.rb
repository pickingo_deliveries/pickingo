module API
  module V2
    class Clients::RequestsController < ClientTokenAuthController

      before_action :set_client

      set_pagination_headers :requests, only: [:index]

      def mark_cancel

        @request = PickupRequest.find_by(client_request_id: request_params[:request_id])

        @request.receiving_remarks = params[:cancel_remarks].present? ? params[:cancel_remarks] :  ''

        states = ["new","assigned", "not_contactable", "on_hold", "not_attempted"]

        if @request && states.include?(@request.aasm_state) && (@current_client == @request.client)
          @request.cancel!
          @request.update_columns(cancelled_by: "client")
          render json: {message: "Request has been marked closed " }, :status => 200
        else
          render json: {message: "Invalid Request ID OR Previous state doesn't match " }, :status => 400
        end

      end

      def index
        @requests = PickupRequest.includes(:address,:pickup_request_state_histories,:skus).where(:client_id => @client.id).search_with_params(params.merge({paginate:true}))
        render json: @requests, :each_serializer => get_serializer
      end

      def bulk_query
        @requests = PickupRequest.includes(:address,:pickup_request_state_histories,:skus).where(:client_id => @client.id).where('client_request_id in (?)', params[:request_ids])
        group_requests = @requests.collect{|a| ClientRequestSerializer.new(a)}.group_by(&:client_request_id)
        params[:request_ids].each do |req_id|
          if group_requests[req_id].blank?
            group_requests[req_id]= 'Invalid request Id'
          end
        end
        render json: group_requests
      end


      def create
        begin
          sanitized_params = request_params
          @request = @client.pickup_requests.build(sanitized_params)
          @request.address.attributes = sanitized_params[:address_attributes]
          @request.assign_hub
          @request.assign_final_destination
          @request.assign_awb_and_dsp
          if @request.save
            render json: @request, serializer: ClientRequestSerializer
          else
            render json: {errors:@request.errors.full_messages.first}, status:400
          end
        rescue Exceptions::InvalidFinalDestination => e
          render json: {message:e.message}
        rescue Exception => e
          render json: {message:e.message}
        end
      end

      def show
        @request = @client.pickup_requests.find_by(client_request_id:params[:id])
        if @request
          render json: @request, :serializer => ClientRequestDetailsSerializer
        else
          render json: {message: 'Invalid Request ID'}, :status => :not_found
        end
      end


      def awbs
        @gr = GeneratedRequest.new({request_count:params[:count].to_i||20,for_client:@client}).get_requests
        render json: @gr, :each_serializer => GeneratedRequestSerializer
      end

      private

      def request_params
        allowed_params = params.permit(:client_order_number,:request_type,:preferred_date,:is_urgent,:cancelled_by,:request_id,:receiving_remarks,
                                       :preferred_location, :remarks,:weight,:length,:height,:breadth,:dispatch_flag,:price,:dispatch_location,
                                       address_attributes: [:city,:country,:phone_number,:name,:pincode,:address_line],
                                       skus_attributes:[:name,:price,:client_sku_id],
                                       seller_attributes:[:name,:address_line,:city,:pincode,:country,:phone] )
        allowed_params[:client_request_id] = params[:client_request_id] if params[:client_request_id]
        allowed_params[:via_api] = true
        allowed_params[:seller_attributes][:pincode] = Pincode.find_by(code: allowed_params[:seller_attributes][:pincode]) if allowed_params[:seller_attributes]
        allowed_params[:client_request_id] = GeneratedRequest.new({request_count:1,for_client:@client}).get_requests.first.code if !params[:client_request_id]
        allowed_params
      end

      def set_client
        @client = current_client
      end

      def sort_column
        PickupRequest.column_names.include?(params[:sortedBy]) ? params[:sortedBy] : 'client_request_id'
      end

      def get_serializer
        if params[:csv].to_bool
          return ClientCsvPickupRequestSerializer
        else
          return ClientRequestSerializer
        end
      end


    end
  end
end
