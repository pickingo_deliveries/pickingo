module API
  module V1
    class PincodesController < AuthorizationController
      before_action :set_hub

      def index
        @pincodes = @hub.pincodes

        render json: @pincodes
      end

      def show
        @pincode = Pincode.find(params[:id])

        render json: @pincode
      end

      def create
        @pincode = @hub.pincodes.build(pincode_params)

        if @pincode.save
          render json: @pincode, status: :created
        else
          render json: @pincode.errors, status: 400
        end
      end

      def update
        @pincode = Pincode.find(params[:id])

        if @pincode.update(pincode_params)
          head :no_content
        else
          render json: @pincode.errors, status: 400
        end
      end

      def destroy
        @pincode = Pincode.find(params[:id])
        @pincode.destroy

        head :no_content
      end

      private

      def pincode_params
        params.permit(:code)
      end

      def set_hub
        @hub = Hub.includes(:pincodes).find_by(:id => params[:hub_id])
      end
    end
  end
end
