module API
  module V1
    class ClientDspPincodesController < AuthorizationController

      skip_before_filter :authenticate_user!, :only => :upload


      def index

      end

      def upload
        @client_dsp_pincode = Uploaders::ClientDspPincodeUploader.new(file:params[:file],success_count:0,fail_count:0,fail_data:[],fail_errors:[])
        if @client_dsp_pincode.save
          render json: {success_count:@client_dsp_pincode.success_count, fail_count:@client_dsp_pincode.fail_count, fail_data:@client_dsp_pincode.fail_data,errors:@client_dsp_pincode.fail_errors}
        else
          render json: {message: 'Uploaded File has error in more than 20 request. Please check and re-upload'}, status: 400
        end
      end

    end
  end
end
