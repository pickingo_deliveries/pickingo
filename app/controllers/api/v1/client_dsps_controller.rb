module API
  module V1
    class ClientDspsController < AuthorizationController

      skip_before_filter :authenticate_user!, :only => :upload


      def index

      end

      def upload
        @client_dsp_import = Uploaders::ClientDspUploader.new(file:params[:file],success_count:0,fail_count:0,fail_data:[],fail_errors:[])
        if @client_dsp_import.save
          render json: {success_count:@client_dsp_import.success_count, fail_count:@client_dsp_import.fail_count, fail_data:@client_dsp_import.fail_data,errors:@client_dsp_import.fail_errors}
        else
          render json: {message: 'Uploaded File has error in more than 20 request. Please check and re-upload'}, status: 400
        end
      end

    end
  end
end
