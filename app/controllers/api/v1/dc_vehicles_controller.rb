module API
  module V1
    class DcVehiclesController < AuthorizationController

      before_action :set_dispatch_center


      def index
        @dc_vehicles = @dispatch_center.dc_vehicles
        render json: @dc_vehicles
      end

      def show
        @dc_vehicle = DcVehicle.find(params[:id])
        render json: @dc_vehicle
      end

      def create
        @dc_vehicle = DcVehicle.new(dc_vehicle_params)
        if @dc_vehicle.save
          render json: @dc_vehicle, :status => 201
        else
          render json: @dc_vehicles.errors.full_messages, status:400
        end
      end

      def destroy
        @dc_vehicle = DcVehicle.find(params[:id])
        @dc_vehicle.destroy
        head :no_content
      end

      private

      def dc_vehicle_params
        params.permit(:dispatch_center_id, :reg_number, :driver_name, :vehicle_model, :driver_phone)
      end

      def set_dispatch_center
        @dispatch_center = DispatchCenter.find(params[:dispatch_center_id])
        return render json: {message:'Not a Dispatch Center'} if @dispatch_center.blank?
      end

    end
  end
end
