module API
  module V1
    class AttendancesController < AuthorizationController

      def index

      end

      def show
      end

      def create
        begin
        attendance_params[:attendances].each do |att|
          @attendance = Attendance.find_or_create_by(user_id: att[:user_id], date: Time.zone.parse(att[:date]).to_date)
          @attendance.update(start_km: att[:start_km], end_km: att[:end_km], start_time: att[:start_time],
                             end_time: att[:end_time], status: att[:status])
        end
        rescue Exception => e
          return render json: {message:"Some Error Occurred!!"}, :status => 400
        end
        render json: {message:"Attendance updated Successfully"}
      end

      def update
      end

      def destroy
      end

      private

      def attendance_params
        params.permit!
      end
    end
  end
end
