module API
  module V1
    class DspsController < AuthorizationController
      def index
        @dsps = Dsp.all
        render json: @dsps
      end
    end
  end
end
