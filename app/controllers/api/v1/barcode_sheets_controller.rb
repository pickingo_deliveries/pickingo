module API
  module V1
    class BarcodeSheetsController < AuthorizationController
      def index
       user = User.find_by(id: params[:user_id])
        if params[:runsheet_date]
          @barcode_sheets = user.barcode_sheets.where("created_at > ? and created_at < ? ", (Time.zone.at(params[:runsheet_date].to_i/1000).to_date() +0.hours), (Time.zone.at(params[:runsheet_date].to_i/1000).to_date() + 1 + 0.hours))
        else
          @barcode_sheets = user.barcode_sheets
        end
        render json: @barcode_sheets
      end

      private

    end
  end
end
