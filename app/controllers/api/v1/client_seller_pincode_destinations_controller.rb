module API
  module V1
    class ClientSellerPincodeDestinationsController < AuthorizationController

      skip_before_filter :authenticate_user!, :only => :upload

      def upload
        @cspd_import = Uploaders::ClientSellerPincodeDestinationUploader.new(file:params[:file],success_count:0,fail_count:0,fail_data:[],fail_errors:[])
        if @cspd_import.save
          render json: {success_count:@cspd_import.success_count, fail_count:@cspd_import.fail_count, fail_data:@cspd_import.fail_data,errors:@cspd_import.fail_errors}
        else
          render json: {message: 'Uploaded File has error in more than 20 request. Please check and re-upload'}, status: 400
        end
      end

    end
  end
end
