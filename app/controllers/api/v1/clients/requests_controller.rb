module API
  module V1
    class Clients::RequestsController < ClientCustomerAuthorizationController

      before_action :set_client

      set_pagination_headers :requests, only: [:index]


      def index
        @requests = PickupRequest.includes(:address,:pickup_request_state_histories,:skus).where(:client_id => @client.id).search_with_params(params)
        render json: @requests, :each_serializer => get_serializer
      end


      def create
        @request = @client.pickup_requests.build(request_params)
        @request.address.attributes = request_params[:address_attributes]
        @request.assign_final_destination
        @request.assign_awb_and_dsp
        if @request.save
          render json: @request, serializer: ClientRequestSerializer
        else
          render json: {errors:@request.errors.full_messages}, status:400
        end
      end

      def show
        @request = @client.pickup_requests.find_by(client_request_id:params[:id])
        if @request
        render json: @request, :serializer => ClientRequestDetailsSerializer
        else
          render json: {message: 'Invalid Request ID'}, :status => :not_found
        end
      end

      private

      def request_params
        allowed_params = params.permit(:client_order_number,:request_type,:preferred_date,
                      :preferred_location, :remarks,:weight,:length,:height,:breadth,
                      address_attributes: [:city,:country,:phone_number,:name,:pincode,:address_line],
                      skus_attributes:[:name,:client_sku_id])
        allowed_params.merge({:client_request_id => GeneratedRequest.new({request_count:1,for_client:@client}).get_requests.first.code})
      end

      def set_client
        if params[:client_id].is_i?
          @client = Client.find(params[:client_id])
        else
          @client = Client.find_by(api_name:params[:client_id])
        end
        return render json: {message:'Not a Valid Customer'} if @client.blank?

        return render json: {message:'Not Authorised'}, :status => 401 if current_client_customer.client_id != @client.id
      end

      def sort_column
        PickupRequest.column_names.include?(params[:sortedBy]) ? params[:sortedBy] : 'client_request_id'
      end

      def get_serializer
        if params[:csv].to_bool
          return ClientCsvPickupRequestSerializer
        else
          return ClientRequestSerializer
        end
      end


    end
  end
end
