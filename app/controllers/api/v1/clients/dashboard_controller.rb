module API
  module V1
    class Clients::DashboardController < ClientCustomerAuthorizationController

      before_action :set_client

      def metrics

          response = {
              :total_pickups => @client.pickup_requests.count,
              :closing_time => @client.closing_time,
              :strike_rate => @client.strike_rate,
              :summary => @client.summary(params[:performance_date])
          }


        render json: response
      end

      private
      def set_client
        @client = Client.find(params[:client_id])
        return render json: {message:'Not a Valid Customer'} if @client.blank?
      end
    end
  end
end
