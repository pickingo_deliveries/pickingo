module API
  module V1
    class PickupRequestsController < AuthorizationController

      skip_before_filter :authenticate_user!, :only => :upload

      set_pagination_headers :pickup_requests, only: [:index]

      def index
        @pickup_requests = PickupRequest.includes(:address,:skus,:client,:user,:hub).search_with_params(params)
        render json: @pickup_requests, :each_serializer => get_serializer
      end

      def show
        @request = PickupRequest.find_by(client_request_id:params[:id])
        render json: @request, :serializer => ClientRequestDetailsSerializer
      end

      def create
        @pickup_request = PickupRequest.new
        if @pickup_request.update(pickup_request_params)
          render json: @pickup_request
        else
          render json: @pickup_request.errors.full_messages, status: 400
        end
      end

      def update
        @pickup_request = PickupRequest.find(params[:id])
        if @pickup_request.update(pickup_request_params)
          send_single_sms(@pickup_request,pickup_request_params) if @pickup_request.client.name == "Snapdeal"
          render json: @pickup_request
        else
          render json: @pickup_request.errors.full_messages, status: 400
        end
      end

      def send_single_sms(request, pickup_request_params)
        history_ids = []
        if (pickup_request_params[:aasm_state] == "received" && pickup_request_params[:current_location_type] == "Hub")
          history_ids << request.pickup_request_state_histories.last.id
          InstantMessageWorker.perform_async(history_ids)
        elsif pickup_request_params[:aasm_state] == "cancelled"
          history_ids << request.pickup_request_state_histories.last.id
          InstantMessageWorker.perform_async(history_ids)
        end
      end

      def assign
        count = 0
        failed_ids = []
        pickup_requests = PickupRequest.where({id:params[:request_ids]})
        pickup_requests.each do |request|
          begin
            request.user_id = params[:user_id]
            request.assign unless request.assigned?
            request.save
          rescue Exception => e
            failed_ids << request.id
            count = count+1
            puts e
          end
        end
        render json: {message:"#{pickup_requests.count - count} requests updated", failed: failed_ids}
      end

      def unassign
        count = 0
        pickup_requests = PickupRequest.where({id:params[:request_ids]})
        pickup_requests.each do |request|
          begin
            if request.assigned?
              request.update_columns(user_id:nil)
              request.revert_status
            else
              request.update_columns(user_id:nil,updated_at:Time.now)
            end
          rescue Exception => e
            count = count+1
            puts e
          end
        end

        render json: {message:"#{pickup_requests.count - count} requests updated"}
        # TODO send all the requests and update their status on the frontend
      end

      def change_state
        pickup_request = PickupRequest.find(params[:id])
        if pickup_request.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      def out_for_pickup
        begin
          out_for_pickup_requests = []
          history_ids = []
          user = User.find_by(id: params[:user_id])
          PickupRequest.transaction do
            User.transaction do
              params[:request_ids].each do |id|
                request = PickupRequest.where({id:id}).first
                request.out! do
                  request.scheduled_date = Time.zone.now.to_date
                  request.save!
                end
                out_for_pickup_requests << request

                if request.client.name == "Snapdeal"
                 history_ids << request.pickup_request_state_histories.last.id
                end
              end
              user.generate_runsheet(out_for_pickup_requests)
              user.generate_barcode_sheet(out_for_pickup_requests)
              user.save!
              @runsheet = user.runsheets.order("created_at").last
              @barcode_sheet = user.barcode_sheets.order("created_at").last
              begin
                #InstantMessageWorker.perform_async(history_ids)
                Sm.auto_trigger_sms(history_ids)
              rescue Exception => e
                puts e
                return render json: {message:'Some Error Occurred while sending message'} , :status => 400
              end
            end
          end
        rescue Exception => e
          puts e
          return render json: {message:'Some Error Occurred!! Reload and try Again'} , :status => 400
        end
        render json: {message:'Requests are Out for Pickup!',runsheet_file:@runsheet.file.url,barcode_file:@barcode_sheet.file.url}
      end

      def close
        begin
          closed_requests = []
          pickup_requests = PickupRequest.where({id:params[:request_ids]})
          PickupRequest.transaction do
            Manifest.transaction do
              pickup_requests.each do |request|
                request.close!
                closed_requests << request
              end
              @manifest = Manifest.new(hub_id:params[:hub_id],client_id:params[:client_id])
              @manifest.pickup_requests = closed_requests
              @manifest.generate_sheet
              @manifest.save!
            end
          end
        rescue Exception => e
          return render json: {message:'Some Error Occurred!! Reload and try Again'}, :status => 400
        end
        render json: {message:'Requests Closed Successfully',file:@manifest.file.url}
      end

      def upload
        @request_import = RequestImport.new(file:params[:file],success_count:0,fail_count:0,fail_data:[],fail_errors:[])
        begin
          @request_import.save
          render json: {success_count:@request_import.success_count, fail_count:@request_import.fail_count, fail_data:@request_import.fail_data,errors:@request_import.fail_errors}
        rescue Exception => e
          Airbrake.notify(e,parameters:{message: e.message})
          render json: {message: @request_import.errors.full_messages, status: 400}
        end
      end

      def schedule_later
        if params[:scheduled_date] == 'kal'
          params[:scheduled_date] = (Time.zone.now.to_date+1).strftime('%d-%m-%Y')
        end
        count = 0
        pickup_requests = PickupRequest.where({id:params[:request_ids]})
        pickup_requests.each do |request|
          begin
            request.update(:scheduled_date => params[:scheduled_date])
          rescue Exception => e
            count = count+1
            puts e
          end
        end
        render json: {message:"#{pickup_requests.count - count} requests scheduled later"}
      end

      def metrics
        @hub = Hub.find(params[:hub_id]) if params[:hub_id]
        if @hub
          response = {
              :total_pickups => @hub.pickup_requests.count,
              :closing_time => @hub.closing_time,
              :strike_rate => @hub.strike_rate,
              :summary => @hub.summary(params[:performance_date])
          }
        else
          response = {
              :total_pickups => PickupRequest.all.count,
              :closing_time => PickupRequest.closing_time,
              :strike_rate => PickupRequest.strike_rate,
              :summary => PickupRequest.summary(params[:performance_date])
          }
        end

        render json: response
      end

      def revert
        @pickup_request = PickupRequest.find(params[:request_id])
        if @pickup_request.revert_status
          render json: @pickup_request
        else
          render json: @pickup_request.errors.full_messages, status: 400
        end
      end

      def claim
        pickup_request = PickupRequest.find(params[:id])
        if pickup_request.claim! && pickup_request.update(current_location_type: params[:current_location_type],
                                                          current_location_id: params[:current_location_id])
          render json: {message:'Request Successfully Claimed'}
        else
          render json: pickup_request.errors.full_messages, status: 400
        end
      end

      def find_master
        request = PickupRequest.find_by(client_request_id: params[:client_request_id])
        master_manifest = Finders::MasterManifestFinder.new(request,params).find_master
        if master_manifest
          render json: {manifest_id: request.manifest_id, master_manifest_id: master_manifest.id}
        else
          render json: {message:'Request does not exist'}, :status => 400
        end
      end


      private

      def pickup_request_params
        params.permit(:client_order_number,:client_request_id,:request_type,:preferred_date,:current_location_id,:current_location_type,:picked_on,:receiving_remarks,
            :preferred_location,:aasm_state, :remarks, :user_id,:hub_id,:scheduled_date,:client_id,:weight,:length,:height,:breadth,:price,:initial_weight,:return_reason,:barcode,
            address_attributes: [:locality, :sub_locality,:city,:country,:phone_number,:name,:pincode,:address_line,:id],
            skus_attributes:[:name,:client_sku_id,:id,:picked])
      end

      def get_serializer
        if params[:csv].to_bool
          return CsvPickupRequestSerializer
        else
          return PurSerializers::PurMinSerializer
        end
      end


    end
  end
end
