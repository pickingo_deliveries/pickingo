module API
  module V1
    class ManifestsController < AuthorizationController

      before_action :set_hub

      before_action :set_client, :except => [:index,:show,:destroy,:change_state,:find_request]

      def index
        @manifests = Manifest.includes(:origin,:destination,:client,:start,:halt).hub_related(@hub.id)
        render json: @manifests
      end

      def show
        @manifest = Manifest.includes(:origin,:destination,:client,:manifest_histories).find(params[:id])
        render json: @manifest, :serializer => HubManifestDetailsSerializer
      end

      def destroy
        @manifest = Manifest.find(params[:id])
        @manifest.destroy
        head :no_content
      end

      def find_request
        @request = PickupRequest.received.find_by(client_request_id:params[:client_request_id],hub_id:params[:hub_id])
        if @request
          render json: @request
        elsif params[:client_id]
          client = Client.find(params[:client_id])
          render json: {message:"No recieved request found for #{@hub.name} for #{client.name}. Please Enter a valid Barcode"}
        else
          render json: {message:"No recieved request found for #{@hub.name}. Please Enter a valid Barcode"}
        end
      end

      def change_state
        @manifest = Manifest.find(params[:id])
        @manifest.assign_attributes(change_params)
        if @manifest.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      private

      def manifest_params
        params.permit(:origin_id,:destination_id,pickup_request_ids:[])
      end

      def change_params
        params.permit(:start_id,:halt_id,:halt_type,:remarks)
      end

      def set_hub
        @hub = Hub.find_by(:id => params[:hub_id])
        return render json: {message:'Not a Valid Hub'} if @hub.blank?
      end

      def set_client
        @client = Client.find(params[:client_id])
        return render json: {message:'Not a Valid Customer'} if @client.blank?
      end
    end
  end
end
