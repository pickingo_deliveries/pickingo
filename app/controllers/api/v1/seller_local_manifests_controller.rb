module API
  module V1
    class SellerLocalManifestsController < ApplicationController

      before_action :set_hub

      def index
        @seller_local_manifests = @hub.originating_local_manifests.includes(:origin,:dsp)
        render json: @seller_local_manifests
      end

      def create
        @seller_local_manifest = SellerLocalManifest.new(seller_local_manifest_params)
        if @seller_local_manifest.save
          render json: @seller_local_manifest
        else
          render json: @seller_local_manifest.errors.full_messages, status:400
        end
      end

      def show
        @seller_local_manifest = SellerLocalManifest.find(params[:id])
        render json: @seller_local_manifest, :serializer => SellerLocalManifestDetailSerializer
      end


      def pickup_requests
        @requests = []
        seller_requests = PickupRequest.includes(:address,:skus,:client,:user,:hub,:pickup_request_state_histories,:seller).received.current_location_requests(@hub.id)
        seller_requests.each do |req|
          @requests << req if !req.returned_to_client? && req.local?
        end
        render json: @requests, :each_serializer => PickupRequestSerializer
      end

      def seller_slips
        @requests = PickupRequest.where('id in (?)',params[:request_ids])
        @requests.where(manifest_id: params[:manifest_id]) if params[:manifest_id]
        begin
          bundled_slip = Utilities::SlipGenerator.new(@requests).generate
          render json: {file:bundled_slip.slip.url}
        rescue Exception => e
          Airbrake.notify(e,parameter:{request_ids:params[:request_ids]})
          render json: {message:'Some Error ocurred'}, status: 400
        end
      end

      def change_state
        @seller_local_manifest = SellerLocalManifest.find(params[:id])
        @seller_local_manifest.assign_attributes(change_params)
        if @seller_local_manifest.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      private

      def seller_local_manifest_params
        params.permit(:origin_id,:dsp_id,:length,:breadth,:height,:weight,:origin_type,pickup_request_ids:[])
      end

      def change_params
        params.permit(:tracking_id)
      end

      def set_hub
        @hub = Hub.find(params[:hub_id])
        return render json: [] if @hub.blank?
      end

    end
  end
end
