module API
  module V1
    class HubDestinationRoutesController < AuthorizationController

      before_action :set_hub

      def index
        @hub_destination_routes = @hub.hub_destination_routes
        render json: @hub_destination_routes
      end

      def update
        @hub_destination_route = HubDestinationRoute.find(params[:id])
        if @hub_destination_route.update(route_id:params[:route_id])
          render json: @hub_destination_route
        else
          render json: @hub_destination_route.errors, status: 400
        end
      end

      private

      def set_hub
        @hub =  Hub.find(params[:hub_id])
        render json: {message:'Not a valid hub'} if @hub.blank?
      end
    end
  end
end
