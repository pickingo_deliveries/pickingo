module API
  module V1
    class CRM::TicketTypesController < CRMAuthController

      def index
        @ticket_types = TicketType.all
        render json: @ticket_types
      end
    end
  end
end