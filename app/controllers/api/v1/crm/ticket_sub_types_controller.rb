module API
  module V1
    class CRM::TicketSubTypesController < CRMAuthController

      before_action :set_ticket_type

      def index
        @ticket_sub_types = @ticket_type.ticket_sub_types
        render json: @ticket_sub_types
      end

      def set_ticket_type
        @ticket_type = TicketType.find(params[:ticket_type_id])
      end

    end
  end
end