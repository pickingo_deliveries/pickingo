module API
  module V1
    class CRM::TicketsController < CRMAuthController

      before_action :set_request, :only => [:create]

      def index
        @ticketCollection = Ticket.search do
          fulltext params[:query]
          #@TODO send the department id
          order_by(:priority_to_time, :asc)
          order_by(:created_at, :desc)
          paginate :page => params[:page], :per_page => 20
        end
        @tickets = @ticketCollection.results
        render json: @tickets , :each_serializer => TicketSerializer
      end

      def tickets_count
        @tickets = Ticket.ticket_summary params
        render json: @tickets
      end

      def create
        @ticket = Ticket.new(ticket_params)
        if @ticket.save!
          @ticket.add_comment
          render json: @ticket
        else
          render json: @ticket.errors.full_messages, status: 400
        end
      end

      def show
        @ticket = Ticket.find(params[:id])

        render json: @ticket , :serializer => TicketDetailSerializer
      end

      def comment
        @ticket = Ticket.find(params[:id])
        comment = @ticket.comments.create!(comment:params[:comment],title:params[:title],crm_user_id:current_crm_user.id,status:@ticket.status)
        if comment.save
          @ticket.touch
          render json: @ticket, :serializer => TicketDetailSerializer
        else
          render json: @ticket.errors, status: :unprocessable_entity
        end
      end

      def change_state
        @ticket = Ticket.find(params[:id])
        @ticket.assign_attributes(change_params)
        if @ticket.send(params[:event])
          manager = HistoryManagers::TicketHistoryManager.new(@ticket,current_crm_user)
          manager.send(logger_method(params[:event]))
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end


      private

      def ticket_params
        params.permit(:title,:description,:ticket_type_id,:ticket_sub_type_id,:owner_id,:priority,:assignee_id,:pickup_request_id,:call_type)
      end

      def change_params
        params.permit(:assigned_to_id,:remarks)
      end

      def set_request
        @request = PickupRequest.find(params[:request_id]) if params[:request_id]
      end

      def logger_method(event)
        name = event.scan(/[a-zA-Z]/).join('')
        "#{name}_complete"
      end



    end
  end
end