module API
  module V1
    class CRM::DepartmentsController < CRMAuthController
      def index
        @departments = Department.all
        render json: @departments
      end
    end
  end
end