module API
  module V1
    class CRM::RequestsController < CRMAuthController

      def index
        @search = PickupRequest.search do
          fulltext params[:query]
          order_by :created_at, :desc
          paginate :page => params[:page], :per_page => 20
        end
        @requests = @search.results
        puts "=====#{@requests.inspect}===S"
        render json: @requests, :each_serializer => CRMRequestListSerializer
      end

      def show
        @request = PickupRequest.find(params[:id])
        render json: @request, :serializer => CRMRequestSerializer
      end


    end
  end
end