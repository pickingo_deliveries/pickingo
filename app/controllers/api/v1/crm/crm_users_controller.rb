module API
  module V1
    class CRM::CRMUsersController < CRMAuthController

      before_action :set_department

      def index
        @users = @department.crm_users
        render json: @users
      end

      private

      def set_department
        @department = Department.find(params[:department_id])
      end
    end
  end
end