module API
  module V1
    class CRM::CommentsController < CRMAuthController

      def index
        @comments = Comment.limit(30)
        render json: @comments, :each_serializer => CommentSerializer
      end

    end
  end
end