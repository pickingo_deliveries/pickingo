module API
  module V1
    class AssetsController < ApplicationController
      before_action :set_asset, only: [:assign,:unassign,:update]
      before_action :set_user, only: [:assign]
      before_action :set_user_by_assignee_id,only: [:index,:create,:assign,:unassign,:get_summary]
      set_pagination_headers :assets, only: [:index,:search]

      def index
        entity = @user.entity
        if entity.nil?
          @assets =  Asset.includes(:entity,:asset_transitions).search_asset(params,entity).paginate(page: params[:page] || 1, per_page: params[:perPage] || 20)
        else
          @assets =  Asset.where(entity_id:entity.id,entity_type: entity.class.name).includes(:entity,:asset_transitions).search_asset(params,entity).paginate(page: params[:page] || 1, per_page: params[:perPage] || 20)
        end
          render json: @assets , each_serializer: AssetSerializer
      end

      def get_summary
        entity = @user.entity
        unless entity.nil?
          asset_summary = entity.asset_summary
        else
          asset_summary = @user.asset_summary
        end
          render json: asset_summary
      end

      # def create
      #   entity = @user.entity
      #   if entity.nil?
      #     @asset = @user.assets.create(asset_params)
      #   else
      #     @asset = entity.assets.build(asset_params)
      #   end
      #
      #   if @asset.save
      #     render json: @asset.as_json, status: :ok
      #   else
      #     render json: {asset: @asset.errors, status: 400}
      #   end
      # end

      def create
        entity = @user.entity
        asset_params.each do |a|
          if entity.nil?
            @asset = @user.assets.create(a)
          else
            @asset = entity.assets.build(a)
          end
            @asset.save
        end

        if @asset
          render json: @asset.as_json, status: :ok
        else
          render json: {asset: @asset.errors, status: 400}
        end
      end

      def update
        if @asset.update(asset_params)
          render json: @asset.as_json, status: :ok
        else
          render json: {asset: @asset.errors, status: 400}
        end
      end

      def assign
        entity = @user.entity
        if entity.present?
          @asset.update(entity_id: entity.id,entity_type: entity.class.name,user_id: @employee.id,state: asset_params[:state])
          create_transition(@asset,asset_params,temp=0)
          render json: @asset.as_json, status: :ok
        elsif entity.nil?
          @asset.update(entity_id: @user.id,entity_type: @user.class.name,user_id: @employee.id,state: asset_params[:state])
          create_transition(@asset,asset_params,temp=0)
          render json: @asset.as_json, status: :ok
        else
          render json: {asset: @asset.errors, status: 400}
        end
      end

      def unassign
        entity = @user.entity
        if entity.nil?
          @asset.update(entity_id: @user.id,entity_type: @user.class.name,user_id: nil,state: asset_params[:state])
          create_transition(@asset,asset_params,temp=1)
          render json: @asset.as_json, status: :ok
        elsif
           @asset.update(entity_id:entity.id,entity_type: entity.class.name,user_id: nil,state: asset_params[:state])
           create_transition(@asset,asset_params,temp=1)
           render json: @asset.as_json, status: :ok
        else
          render json: {asset: @asset.errors, status: 400}
        end
      end

      private

      def create_transition(asset,asset_params,temp)
        if temp==1
          @transition = asset.asset_transitions.build(user_id: params[:assignee_id],return_date: Date.today,state: asset_params[:state],remarks: asset_params[:remarks],quantity: 1 )
        else
          @transition = asset.asset_transitions.build(user_id: params[:assignee_id],assign_date: Date.today,state: asset_params[:state],quantity: 1 )
        end
          @transition.save
      end

      def set_asset
        @asset = Asset.find(params[:id])
      end

      def set_user_by_assignee_id
        unless params[:assignee_id].blank?
          @user = User.find_by(id: params[:assignee_id])
        end
      end

      def set_user
        unless params[:user_id].blank?
          @employee = User.find_by(id: params[:user_id])
        end
      end

      def asset_params
        #params.require(:asset).permit(:asset_type,:serial_no,:reference_no,:entity_id,:entity_type,:state,:if_other,:remarks)
        params.require(:asset).map do |p|
          ActionController::Parameters.new(p.to_hash).permit(:asset_type,:serial_no,:reference_no,:entity_id,:entity_type,:state,:if_other,:remarks)
        end
      end
    end
  end
end
