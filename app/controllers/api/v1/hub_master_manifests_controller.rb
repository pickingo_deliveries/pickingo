module API
  module V1
    class HubMasterManifestsController < AuthorizationController
      set_pagination_headers :hub_master_manifests, only: [:index, :incoming]

      before_action :set_hub

      def index
        @hub_master_manifests = @hub.originating_hub_master_manifests.includes(:origin, :destination, :hub_manifests).
            order('hub_master_manifests.created_at desc').search_with_params(params)
        @hub_master_manifests = @hub_master_manifests.paginate(page: params[:page] || 1, per_page: 20)
        render json: @hub_master_manifests
      end

      def incoming
        @hub_master_manifests = HubMasterManifest.incoming_manifests(@hub).order('hub_master_manifests.created_at desc')
        @hub_master_manifests = @hub_master_manifests.paginate(page: params[:page] || 1, per_page: 20)
        render json: @hub_master_manifests
      end

      def show
        @hub_master_manifest = HubMasterManifest.find(params[:id])
        render json: @hub_master_manifest ,:serializer => HubMasterManifestDetailSerializer
      end

      def create
        @hub_master_manifest =  HubMasterManifest.new(hub_master_manifest_params)
        if @hub_master_manifest.save
          render json: @hub_master_manifest, status: :created
        else
          render json: @hub_master_manifest.errors, status: 400
        end
      end

      def update
        @hub_master_manifest = HubMasterManifest.find(params[:id])
        if @hub_master_manifest.update(hub_master_manifest_params)
          head :no_content
        else
          render json: @hub_master_manifest.errors, status: 400
        end
      end


      def change_state
        @hub_master_manifest = HubMasterManifest.find(params[:id])
        if @hub_master_manifest.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      private

      def hub_master_manifest_params
        params.permit(:origin_id, :destination_id, :destination_type, :coloader_id, :transport_mode,
                      :tracking_id, :chargeable_weight, :gross_weight, hub_manifest_ids:[])
      end

      def change_params
        params.permit(:origin_id,:destination_id,:destination_type,hub_manifest_ids:[])
      end


      def set_hub
        @hub = Hub.find_by(:id => params[:hub_id])
        return render json: {message:'Not a Valid Hub'} if @hub.blank?
      end

    end
  end
end
