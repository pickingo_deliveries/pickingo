module API
  module V1
    class PickupBoys::BoysController < PickupBoysAuthorizationController

      before_action :set_pickup_boy

      def show
        @boy = User.find_by(id: params[:id])
        render json: @boy
      end

      def update
        @boy = User.find_by(id: params[:id])
        if @boy.update(boy_params)
          render json: @boy
        else
          render json: @boy.errors.full_messages, status: 400
        end
      end

      private
      def boy_params
        params.permit(:name,:phone)
      end

      def set_pickup_boy
        @user = User.find_by(id: params[:pickup_boy_id])
        return render json: {message:'Not a valid Pickup Boy Id'} if @user.blank?
      end
    end
  end
end
