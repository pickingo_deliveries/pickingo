module API
  module V1
    class PickupBoys::PbActivitiesController < PickupBoysAuthorizationController
      before_action :set_pickup_boy_activity

      def check_in
        response = @attendance.check_in
        render json: {message: response[:message]} ,:status => response[:status]
      end


      def check_out # TODO - if pickup_boy don't check out till EOD.
        response = @attendance.check_out
        render json: {message: response[:message]} ,:status => response[:status]
      end

      def trip_start
        # check for request parameters type.
        if params[:start_km].nil? || !params[:start_km].to_f.is_a?(Numeric)
          return render json: {message: 'please enter valid start km value!'}, :status => 400
        end

        response = @attendance.start_trip(params)
        render json: {message: response[:message]} ,:status => response[:status]
      end

      def trip_stop
        # check data type of request parameters
        if params[:end_km].nil? || !params[:end_km].to_f.is_a?(Numeric)
          return render json: {message: 'please enter valid end km value!'}, :status => 400
        end
        response = @attendance.end_trip(params)
        render json: {message: response[:message]} ,:status => response[:status]
      end

      private

      def set_pickup_boy_activity
        @attendance = Attendance.find_or_create_by(user_id: params[:pickup_boy_id], date: Date.current)
      end
    end
  end
end
