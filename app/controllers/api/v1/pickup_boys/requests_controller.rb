module API
  module V1
    class PickupBoys::RequestsController < PickupBoysAuthorizationController

      before_action :set_pickup_boy

      def index
        @requests = PickupRequest.search_with_params(params.merge({:out_for_pickup => "#{Time.now.to_i*1000}"}))
        render json: @requests, :each_serializer => AppPickupRequestSerializer
      end

      def show
        @request = PickupRequest.find(params[:id])
        render json: @request
      end

      def update
        @pickup_request = PickupRequest.find(params[:id])
        if @pickup_request.update(request_params)
          render json: @pickup_request, serializer: AppPickupRequestSerializer
        else
          render json: @pickup_request.errors.full_messages, status: 400
        end
      end

      private
      def request_params
        params.permit(:client_order_number,:client_request_id,:request_type,:preferred_date,:signature,
                      :preferred_location,:aasm_state, :remarks, :user_id,:scheduled_date,:client_id,:weight,:length,:height,:breadth,:barcode,
                      address_attributes: [:locality, :sub_locality,:city,:country,:phone_number,:name,:pincode,:address_line,:id],
                      skus_attributes:[:name,:client_sku_id,:id,:picked])
      end

      def set_pickup_boy
        @pickup_boy = User.find_by(id: params[:pickup_boy_id])
        return render json: {message:'Not a valid Pickup Boy Id'} if @pickup_boy.blank?
      end
    end
  end
end
