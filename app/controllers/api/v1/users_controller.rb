module API
  module V1
    class UsersController < ApplicationController
      set_pagination_headers :users, only: [:index]


      def index
        @users = User.includes(:user_profile, :role).search_with_params(params)
        @users = @users.paginate(page: params[:page] || 1, per_page: params[:per_page]) if params[:per_page].present?
        render json: @users
      end

      def create
        user = nil
        profile = nil
        ActiveRecord::Base.transaction do
          user = User.create(user_params.except(:user_profile).merge({user_name: "#{user_params[:user_profile][:first_name]}"+" "+ "#{user_params[:user_profile][:last_name]}"}))
          profile = UserProfile.create(user_params[:user_profile].merge(user_id: user.id)) if user.id
        end

        if user && profile
          render json: user, status: :created
        else
          render json: user.errors.full_messages, status: 400
        end
      end

      def show
        user = User.find(params[:id])
        render json: user
      end

      def update
        profile = nil
        usr = nil
        user = User.find_by(id: params[:id])
        old_entity_id = user.entity_id unless user.entity_id
        ActiveRecord::Base.transaction do
          usr = user.update(user_params.except(:user_profile))
          user.reset_requests if user_params[:entity_id] != old_entity_id
          profile = user.user_profile.update(user_params[:user_profile])
        end
        if usr && profile
          render json: user, status: 200
        else
          render json: user.errors.full_messages, status: 400
        end
      end

      def pickup_requests
        user = User.find_by(id: params[:user_id])
        @requests = user.pickup_request_state_histories.select("state, Date(pickup_request_state_histories.created_at) as date,count(state) as count").group(:state, :date).to_a

        @requests = @requests.sort.reverse.group_by{|request|request.date}

        render json: @requests, each_serializer: PickupBoyDetailsSerializer
      end

      def attendances
        hub = Hub.find_by(id: params[:hub_id])
        @attendances = hub.attendances.where(:date => Date.parse(params[:attendance_date]))
        @attendances = @attendances.where(user_id: params[:user_id]) if params[:user_id]
        render json: @attendances
      end

      def get_employee
        @user = User.find_by(uid: params[:id])
        render json: @user
      end

      private

      def user_params
        params.permit(:email, :role_id, :entity_type, :entity_id, :status, :reffered_by, :ctc, :id, :reason_of_resignation,
            :remarks, :date_of_resignation, :last_working_date, :active, :vendor_id, :vendor_emp_id,
            user_profile: [:id, :user_id, :first_name, :middle_name, :last_name, :date_of_joining, :alias, :app_installed_on,
               :date_of_birth, :gender, :marital_status, :blood_group, :father_name, :father_number, :permanent_address,
               :permanent_district, :permanent_city, :permanent_state, :permanent_pincode, :current_address,
               :current_district, :current_city, :current_state, :current_pincode, :personal_number, :company_number,
               :vehicle_number, :dl_number, :rc_number, :emergency_contact_name, :emergency_contact_number, :relation])
      end

    end
  end
end
