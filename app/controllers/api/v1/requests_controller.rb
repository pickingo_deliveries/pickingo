module API
  module V1
    class RequestsController < ApplicationController
      include ActionController::HttpAuthentication::Token::ControllerMethods

      VERIFIER = ActiveSupport::MessageVerifier.new('f5bfb0763699f0d8258ca70b2c23af1d')

      TOKEN='pickingo-client'

      before_action :authenticate

      def show
        @request = PickupRequest.includes(:client).find_by(client_request_id:params[:id])
        if @request
          render json: @request, :serializer => ClientRequestDetailsSerializer
        else
          render json: nil
        end
      end


      private

      def authenticate
        authenticate_or_request_with_http_token do |token, options|
          VERIFIER.verify(token) == TOKEN
        end
      end
    end
  end
end
