module API
  module V1
    class RolesController < ApplicationController

      def index
        roles = Role.all
        render json: roles
      end

    end
  end
end
