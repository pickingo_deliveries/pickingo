module API
  module V1
    class DispatchCenterMasterManifestsController < AuthorizationController
      set_pagination_headers :master_manifests, only: [:index]

      before_action :set_dispatch_center

      def index
        @master_manifests = @dispatch_center.originating_master_manifests.order('dispatch_center_master_manifests.created_at desc').
            includes(:origin, :destination, :client, :dc_vehicle).search_with_params(params)
        @master_manifests = @master_manifests.paginate(page: params[:page] || 1, per_page: 20)
        render json: @master_manifests
      end

      def show
        master_manifest = DispatchCenterMasterManifest.find(params[:id])
        render json: master_manifest, :serializer => DispatchCenterMasterManifestDetailsSerializer
      end

      def create
        master_manifest = DispatchCenterMasterManifest.new(master_manifest_params)
        if master_manifest.save
          render json: master_manifest, status: :created
        else
          render json: master_manifest.errors, status: 400
        end
      end

      def update
        master_manifest = DispatchCenterMasterManifest.find(params[:id])
        if master_manifest.update(master_manifest_params)
          head :no_content
        else
          render json: master_manifest.errors, status: 400
        end
      end


      def change_state
        master_manifest = DispatchCenterMasterManifest.find(params[:id])
        master_manifest.dc_vehicle_id = params[:dc_vehicle_id] if params[:dc_vehicle_id]
        if master_manifest.send(params[:event])
          master_manifest.dc_requests_close(params[:damaged_requests]) if params[:event] == 'close!'
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      private

      def master_manifest_params
        params.permit(:origin_id,:destination_id,:client_id,:dc_vehicle_id,dispatch_center_manifest_ids:[])
      end

      def set_dispatch_center
        @dispatch_center = DispatchCenter.find(params[:dispatch_center_id])
        return render json: {message:'Not a Dispatch Center'} if @dispatch_center.blank?
      end

    end
  end
end
