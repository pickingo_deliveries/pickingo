module API
  module V1
    class DispatchCenterManifestsController < AuthorizationController
      set_pagination_headers :dispatch_center_manifests, only: [:index]
      set_pagination_headers :hub_master_manifests, only: [:incoming]

      before_action :set_dispatch_center

      def index
        @dispatch_center_manifests = @dispatch_center.originating_manifests.order('dispatch_center_manifests.created_at desc').
            includes(:origin, :destination, :client, :dc_vehicle).search_with_params(params)
        @dispatch_center_manifests = @dispatch_center_manifests.paginate(page: params[:page] || 1, per_page: 20)
        render json: @dispatch_center_manifests
      end

      def show
        @dispatch_center_manifest = DispatchCenterManifest.includes(:pickup_requests, :dispatch_center_manifest_histories).find(params[:id])
        render json: @dispatch_center_manifest, :serializer => DispatchCenterManifestDetailsSerializer
      end

      def incoming
        @hub_master_manifests = HubMasterManifest.incoming_manifests(@dispatch_center).order('hub_master_manifests.created_at desc')
        @hub_master_manifests = @hub_master_manifests.paginate(page: params[:page] || 1, per_page: 20)
        render json: @hub_master_manifests, :each_serializer => HubMasterManifestSerializer
      end

      def create
        @dispatch_center_manifest = DispatchCenterManifest.new(dispatch_center_manifest_params)
        if @dispatch_center_manifest.save
          render json: @dispatch_center_manifest, status: :created
        else
          render json: @dispatch_center_manifest.errors, status: 400
        end
      end

      def update
        @hub_manifest = HubManifest.find(params[:id])
        if @hub_manifest.update(hub_manifest_params)
          head :no_content
        else
          render json: @hub_manifest.errors, status: 400
        end
      end


      def find_request
        @request = Finders::ManifestRequestFinder.new(params,@dispatch_center).find_request
        if @request
          render json: @request
        else
          render json: {message:"No recieved request found for #{@dispatch_center.name}. Please Enter a valid Barcode"}
        end
      end

      def change_state
        @dispatch_center_manifest = DispatchCenterManifest.find(params[:id])
        @dispatch_center_manifest.dc_vehicle_id = params[:dc_vehicle_id] if params[:dc_vehicle_id]
        if @dispatch_center_manifest.send(params[:event])
          @dispatch_center_manifest.dc_requests_close(params[:damaged_requests]) if params[:event] == 'close!'
          render json: @request
        else
          render json: {message:"No recieved request found for #{@dispatch_center.name}. Please Enter a valid Barcode"}
        end
      end


      private

      def dispatch_center_manifest_params
        params.permit(:origin_id,:destination_id,:length,:breadth,:height,:weight,:seal_number,
                      :client_id,pickup_request_ids:[])
      end

      def change_params
        params.permit(:start_id,:halt_id,:halt_type,:remarks)
      end

      def set_dispatch_center
        @dispatch_center = DispatchCenter.find(params[:dispatch_center_id])
        return render json: {message:'Not a Dispatch Center'} if @dispatch_center.blank?
      end

      def set_client_warehouse
        @client_warehouse = ClientWarehouse.find(params[:client_warehouse_id])
        return render json: {message:'Not a Warehouse'} if @client_warehouse.blank?
      end

    end
  end
end
