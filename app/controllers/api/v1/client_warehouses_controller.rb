module API
  module V1
    class ClientWarehousesController < AuthorizationController


      def index
        @warehouses = ClientWarehouse.all
        render json: @warehouses
      end

    end
  end
end
