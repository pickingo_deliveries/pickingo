module API
  module V1
    class ClientsController < AuthorizationController
      def index
        @clients = Client.all
        render json: @clients
      end

      def show
        @client = Client.find(params[:id])
        render json: @client
      end

      def create
        @client = Client.new(hub_params)
        if @client.save
          render json: @client, status: :created
        else
          render json: @client.errors, status: 400
        end
      end

      def update
        @client = Client.find(params[:id])
        if @client.update(hub_params)
          head :no_content
        else
          render json: @client.errors, status: 400
        end
      end

      def destroy
        @client = Client.find(params[:id])
        @client.destroy
        head :no_content
      end

      private

      def hub_params
        params.permit(:name)
      end
    end
  end
end
