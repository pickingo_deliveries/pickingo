module API
  module V1
    class RoutesController < AuthorizationController

      def index
        @routes = Route.includes(:origin,:route_destination,:via_locations).search_with_params(origin_id:params[:origin_id],destination_id:params[:destination_id])
        render json: @routes, :each_serializer => RouteSerializer
      end

    end
  end
end
