module API
  module V1
    class DcHubManifestController < AuthorizationController

      before_action :set_dc

      def index
        @hub_manifests = HubMasterManifest.find(params[:master_manifest_id]).hub_manifests
        render json: @hub_manifests, :each_serializer => HubManifestSerializer
      end

      def hub_manifest
        @hub_manifest = HubManifest.find(params[:manifest_id])
        render json: @hub_manifest, :serializer => IncomingManifestSerializer
      end

      def receive_request
        @request = PickupRequest.find(params[:request_id])
        begin
          @request.current_location = @dc
          @request.manifest_receive!
          render json: @request
        rescue Exception => e
          render json: {message:'Request Not received'}, :status => 400
        end
      end

      def change_hub_state
        @hub_manifest = HubManifest.find(params[:manifest_id])
        if @hub_manifest.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      def change_master_state
        @hub_master_manifest = HubMasterManifest.find(params[:master_manifest_id])
        if @hub_master_manifest.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      def single_receive
        hub_master_manifest = HubMasterManifest.find(params[:master_manifest_id])
        begin
          hub_master_manifest.mark_received(@dc)
          DispatchCenterMasterManifest.create_dup_master(@dc.id, hub_master_manifest)
          render json: {message:'Master manifest received'}
        rescue Exception => e
          puts e
          render json: {message:'Failed'}, :status => 400
        end
      end

      def find_request
        request = PickupRequest.find_by(client_request_id: params[:client_request_id])
        master_manifest = request.try(:manifest).try(:hub_master_manifest)
        if master_manifest && ['in_transit','partially_received'].include?(master_manifest.state) &&
            master_manifest.destination_id == @dc.id && master_manifest.destination_type == @dc.class.name
          render json: {manifest_id: request.manifest_id, master_manifest_id: master_manifest.id}
        else
          render json: {message:'Request does not exist'}, :status => 400
        end
      end


      private
      def set_dc
        @dc = DispatchCenter.find(params[:dispatch_center_id])
        return render json: {message:'Not a DC'} if @dc.blank?
      end

    end
  end
end
