module API
  module V1
    class HubManifestsController < AuthorizationController
      set_pagination_headers :hub_manifests, only: [:index]

      before_action :set_hub

      def index
        @hub_manifests = @hub.originating_manifests.includes(:origin, :destination, :pickup_requests).
            order('hub_manifests.created_at desc').search_with_params(params)
        @hub_manifests = @hub_manifests.paginate(page: params[:page] || 1, per_page: 20)
        render json: @hub_manifests, :each_serializer => HubManifestSerializer
      end

      def show
        @hub_manifest = HubManifest.includes(:pickup_requests, :hub_manifest_histories).find(params[:id])
        render json: @hub_manifest, :serializer => HubManifestDetailsSerializer
      end

      def create
        @hub_manifest = HubManifest.new(hub_manifest_params)
        if @hub_manifest.save
          render json: @hub_manifest, status: :created
        else
          render json: @hub_manifest.errors, status: 400
        end
      end

      def update
        @hub_manifest = HubManifest.find(params[:id])
        if @hub_manifest.update(hub_manifest_params)
          head :no_content
        else
          render json: @hub_manifest.errors, status: 400
        end
      end

      def find_request
        @data = Finders::ManifestRequestFinder.new(params,@hub).find_request
        render json: @data
      end

      def receive_request
        @request = PickupRequest.find(params[:request_id])
        begin
          @request.current_location = @hub
          @request.manifest_receive!
          render json: @request
        rescue Exception => e
          render json: {message:'Request Not received'}
        end
      end

      def change_state
        @hub_manifest = HubManifest.find(params[:id])
        if @hub_manifest.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      private

      def hub_manifest_params
        params[:pickup_request_ids] = params[:pickup_request_ids].uniq
        params.permit(:origin_id,:destination_id,:destination_type,:length,:breadth,:height,:weight,
                      :seal_number,pickup_request_ids:[])
      end


      def set_hub
        @hub = Hub.find_by(:id => params[:hub_id])
        return render json: {message:'Not a Valid Hub'} if @hub.blank?
      end

    end
  end
end
