module API
  module V1
    class ClientPincodeDestinationController < AuthorizationController

      skip_before_filter :authenticate_user!, :only => :upload

      def upload
        @destination_import = Uploaders::DestinationUploader.new(file:params[:file],success_count:0,fail_count:0,fail_data:[],fail_errors:[])
        if @destination_import.save
          render json: {success_count:@destination_import.success_count, fail_count:@destination_import.fail_count, fail_data:@destination_import.fail_data,errors:@destination_import.fail_errors}
        else
          render json: {message: 'Uploaded File has error in more than 20 request. Please check and re-upload'}, status: 400
        end
      end

    end
  end
end
