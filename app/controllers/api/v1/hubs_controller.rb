module API
  module V1
    class HubsController < AuthorizationController
      set_pagination_headers :requests, only: [:inventory]

      before_action :set_hub, only: [:destinations, :inventory, :generate_inventory_excel, :generate_summary]

      def index
        @hubs = Hub.includes(:users).all
        render json: @hubs
      end

      def show
        @hub = Hub.find(params[:id])
        render json: @hub
      end

      def create
        @hub = Hub.new(hub_params)
        if @hub.save
          render json: @hub, status: :created
        else
          render json: @hub.errors, status: 400
        end
      end

      def update
        @hub = Hub.find(params[:hubId])
        if @hub.update(hub_params)
          head :no_content
        else
          render json: @hub.errors, status: 400
        end
      end

      def destroy
        @hub = Hub.find(params[:id])
        @hub.destroy
        head :no_content
      end

      def destinations
        @destinations = Utilities::HubDestinationProvider.new(@hub).get_destinations
        render json: @destinations.to_json
      end

      def inventory
        @requests = PickupRequest.includes(:client,:seller,:final_destination,:skus).received_at(@hub)
        @requests = PickupRequest.get_requests_type(@requests,params[:request_type])
        @requests = @requests.paginate(page: params[:page] || 1, per_page: 20)
        render json: @requests , :each_serializer => InventorySerializer
      end

      def generate_inventory_excel
        requests = PickupRequest.received_at(@hub)
        begin
          excel = Utilities::RequestsExcelGenerator.new(requests).generate
          render json: {file:excel.excel_file.url}
        rescue Exception => e
          puts e
          Rollbar.error(e)
          render json: {message:'Some Error ocurred'}, status: 400
        end
      end

      def generate_summary
        transactions = Transaction.where(hub_id: @hub.id)
        desired_transactions = transactions.where(["payment_date between ? and ?",Date.parse(params[:from_date]),Date.parse(params[:to_date])])
        begin
          excel = Utilities::TransactionsExcelGenerator.new(desired_transactions, @hub.name).generate
          render json: {file:excel.excel_file.url}
        rescue Exception => e
          puts e
          Rollbar.error(e)
          render json: {message:'Some Error ocurred'}, status: 400
        end
      end

      private

      def set_hub
        @hub= Hub.find(params[:id])
        return render json: {message:'Not a Hub'} if @hub.blank?
      end

      def hub_params
        params.permit(:address_line, :name, :city)
      end
    end
  end
end
