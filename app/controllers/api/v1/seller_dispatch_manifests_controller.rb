module API
  module V1
    class SellerDispatchManifestsController < ApplicationController

      before_action :set_hub, :except => [:upload_request]

      def index
        @seller_dispatch_manifests = @hub.originating_dispatch_manifests.includes(:origin,:dsp)
        render json: @seller_dispatch_manifests
      end

      def create
        @seller_dispatch_manifest = SellerDispatchManifest.new(seller_dispatch_manifest_params)
        if @seller_dispatch_manifest.save
          render json: @seller_dispatch_manifest
        else
          render json: @seller_dispatch_manifest.errors.full_messages, status:400
        end
      end

      def show
        @seller_dispatch_manifest = SellerDispatchManifest.find(params[:id])
        render json: @seller_dispatch_manifest, :serializer => SellerDispatchManifestDetailSerializer
      end

      def seller_slips
        @requests = PickupRequest.where('id in (?)',params[:request_ids])
        @requests.where(manifest_id: params[:manifest_id]) if params[:manifest_id]
        begin
          bundled_slip = Utilities::SlipGenerator.new(@requests).generate
          render json: {file:bundled_slip.slip.url}
        rescue Exception => e
          Airbrake.notify(e,parameters:{message: e.message})
          render json: {message:'Some Error ocurred'}, status: 400
        end
      end


      def pickup_requests
        @requests = []
        seller_requests = PickupRequest.includes(:address,:skus,:client,:user,:hub,:pickup_request_state_histories,:seller,:dsp, :awb_number).received.current_location_requests(@hub.id)
        seller_requests.each do |req|
          @requests << req if req.dispatch?
        end
        render json: @requests, :each_serializer => PickupRequestSerializer
      end

      def change_state
        @seller_dispatch_manifest = SellerDispatchManifest.find(params[:id])
        @seller_dispatch_manifest.assign_attributes(change_params)
        if @seller_dispatch_manifest.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      def upload_request
        @seller_requests = Uploaders::SellerRequestsUploader.new(file:params[:file],success_count:0,fail_count:0,fail_data:[],fail_errors:[])
        if @seller_requests.update
          render json: {success_count:@seller_requests.success_count, fail_count:@seller_requests.fail_count, fail_data:@seller_requests.fail_data,errors:@seller_requests.fail_errors}
        else
          render json: {message: 'Uploaded File has error in more than 20 request. Please check and re-upload'}, status: 400
        end
      end

      private

      def set_hub
        @hub = Hub.find(params[:hub_id])
        return render json: [] if @hub.blank?
      end

      def change_params
        params.permit(:tracking_id)
      end

      def seller_dispatch_manifest_params
        params.permit(:origin_id,:dsp_id,:length,:breadth,:height,:weight,:origin_type,pickup_request_ids:[])
      end

    end
  end
end
