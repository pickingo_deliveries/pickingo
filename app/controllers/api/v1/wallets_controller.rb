module API
  module V1
    class WalletsController < ApplicationController

      def index
        wallets = Wallet.all
        render json: wallets
      end

      def show
        wallet = Wallet.find(params[:id])
        render json: wallet
      end

      private

      def wallet_params
        params.permit([:entity_id, :entity_type, :amount])
      end

    end
  end
end
