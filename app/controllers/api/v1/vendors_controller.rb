module API
  module V1
    class VendorsController < ApplicationController

      def index
        vendors = Vendor.all
        render json: vendors
      end
    end
  end
end