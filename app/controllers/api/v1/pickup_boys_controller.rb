module API
  module V1
    class PickupBoysController < AuthorizationController
      before_action :set_hub

      def index
        @pickup_boys = @hub.pickup_boys
        render json: @pickup_boys, each_serializer: PickupBoySerializer
      end

      def show
        @pickup_boy = PickupBoy.find(params[:pickup_boy_id])
        render json: @pickup_boy
      end

      def pickup_requests
        @pickup_boy = PickupBoy.find(params[:pickup_boy_id])
        @requests = @pickup_boy.pickup_request_state_histories.select("state, Date(pickup_request_state_histories.created_at) as date,count(state) as count").group(:state, :date).to_a

        @requests = @requests.sort.reverse.group_by{|request|request.date}

        render json: @requests, each_serializer: PickupBoyDetailsSerializer
      end

      def create
        @pickup_boy = @hub.pickup_boys.build(pickup_boy_params)
        if @pickup_boy.save
          render json: @pickup_boy, status: :created
        else
          render json: @pickup_boy.errors, status: 400
        end
      end

      def update
        @pickup_boy = PickupBoy.find(params[:id])
        if @pickup_boy.update(pickup_boy_params)
          render json: @pickup_boy
        else
          render json: @pickup_boy.errors.messages, status: 400
        end
      end

      def destroy
        @pickup_boy = PickupBoy.find(params[:id])
        @pickup_boy.update(active: false)

        render json: @pickup_boy
      end

      def attendances
        @attendances = @hub.attendances.where(
            :date => Date.parse(params[:attendance_date])
        )

        render json: @attendances
      end


      private

      def pickup_boy_params
        params.permit(:name, :phone,:active)
      end

      def set_hub
        @hub = Hub.includes(:pickup_boys).find_by(:id => params[:hub_id])
      end
    end
  end
end
