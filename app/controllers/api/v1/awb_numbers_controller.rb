module API
  module V1
    class AwbNumbersController < AuthorizationController

      skip_before_filter :authenticate_user!, :only => :upload

      def upload
        @awb_import = Uploaders::AwbNumberUploader.new(file:params[:file],success_count:0,fail_count:0,fail_data:[],fail_errors:[])
        if @awb_import.save

          render json: {success_count:@awb_import.success_count, fail_count:@awb_import.fail_count, fail_data:@awb_import.fail_data,errors:@awb_import.fail_errors}
        else
          render json: {message: 'Uploaded File has error in more than 20 request. Please check and re-upload'}, status: 400
        end
      end

    end
  end
end
