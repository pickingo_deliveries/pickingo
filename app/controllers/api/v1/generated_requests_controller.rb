module API
  module V1
    class GeneratedRequestsController < AuthorizationController

      def index
        @gr = GeneratedRequest.where.not(:client_id => nil)

        render json: @gr
      end

      def create
        @gr = GeneratedRequest.new(request_count:params[:request_count].to_i,for_client:Client.find(params[:client_id]))
        requests = @gr.get_requests
        render json: requests
      end

    end
  end
end
