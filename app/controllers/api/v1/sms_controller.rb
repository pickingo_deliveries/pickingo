module API
  module V1
    class SmsController < AuthorizationController

      def index
        @sms = Sm.includes(pickup_request_state_history:[pickup_request:[:client,:hub,:user]]).
            where("created_at > ? and sent = 0",Time.zone.now.to_date()+0.hours)
        render json: @sms
      end

      def send_sms
        count = 0
        failed_ids = []
        smses = Sm.where({id:params[:sms_ids]})
        xml = SmsXml.new(smses)
        xml_file = xml.build_xml
        begin
          Api::Sms.send_multiple(xml_file)
          smses.update_all(sent:true)
        rescue Exception => e
          Airbrake.notify_or_ignore(e, parameters: {message:"SMS message failed"})
          #failed_ids << request.id
          count = count+1
          puts e
        end
        render json: {message:"#{smses.count - count} sms sent", failed: failed_ids}
      end

    end
  end
end
