module API
  module V1
    class DispatchCentersController < AuthorizationController
      set_pagination_headers :requests, only: [:inventory]

      before_action :set_dc , only: [:inventory, :generate_inventory_excel]

      def index
        @dispatch_centers = DispatchCenter.all
        render json: @dispatch_centers
      end

      def show
        @dispatch_center = DispatchCenter.find(params[:id])
        render json: @dispatch_center
      end

      def inventory
        @requests = PickupRequest.includes(:awb_number, :client, :seller, :hub, :final_destination,:skus).received_at(@dc)
        @requests = PickupRequest.get_requests_type(@requests,params[:request_type])
        @requests = @requests.paginate(page: params[:page] || 1, per_page: 20)
        render json: @requests , :each_serializer => InventorySerializer
      end

      def generate_inventory_excel
        requests = PickupRequest.received_at(@dc)
        begin
          excel = Utilities::RequestsExcelGenerator.new(requests).generate
          render json: {file:excel.excel_file.url}
        rescue Exception => e
          puts e
          Rollbar.error(e)
          render json: {message:'Some Error ocurred'}, status: 400
        end
      end


      private
      def set_dc
        @dc = DispatchCenter.find(params[:id])
        return render json: {message:'Not a DC'} if @dc.blank?
      end

    end
  end
end
