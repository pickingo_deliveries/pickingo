module API
  module V1
    class RunsheetsController < AuthorizationController
      def index
       user = User.find_by(id: params[:user_id])
        if params[:runsheet_date]
          @runsheets = user.runsheets.where("created_at > ? and created_at < ? ", (Time.zone.at(params[:runsheet_date].to_i/1000).to_date() +0.hours),(Time.zone.at(params[:runsheet_date].to_i/1000).to_date() + 24.hours))
        else
          @runsheets = user.runsheets
        end
        render json: @runsheets
      end

      def show
        @runsheet = Runsheet.find(params[:id])
        render json: @runsheet
      end

      def create
        @runsheet = Runsheet.new(hub_params)
        if @runsheet.save
          render json: @runsheet, status: :created
        else
          render json: @runsheet.errors, status: 400
        end
      end

      def update
        @runsheet = Runsheet.find(params[:id])
        if @runsheet.update(hub_params)
          head :no_content
        else
          render json: @runsheet.errors, status: 400
        end
      end

      def destroy
        @runsheet = Runsheet.find(params[:id])
        @runsheet.destroy
        head :no_content
      end

      private

      def hub_params
        params.permit(:name)
      end
    end
  end
end
