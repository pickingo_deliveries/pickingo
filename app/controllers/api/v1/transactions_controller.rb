module API
  module V1
    class TransactionsController < ApplicationController

      def index
        transactions = Transaction.search_with_params(params)
        render json: transactions
      end

      def create
        @trans = Transaction.new(transaction_params)
        ActiveRecord::Base.transaction do
          begin
            if @trans.transaction_type === "Advance"
              @trans.wallet_add(@trans.sender_id,"User",@trans.hub_id,"Hub",@trans.amount)
            else
              @trans.wallet_add(@trans.hub_id,"Hub",@trans.reciever_id,"User",@trans.amount)
            end
          rescue => e
            @trans.errors.add(:transaction_type,"amount not allowed")
          end
          if @trans.errors.count > 0
            render json: @trans.errors.full_messages, status: 400
          elsif @trans.save
            render json: @trans, status: :created
          else 
            render json: @trans.errors.full_messages, status: 400
          end
        end
      end

      def update
        @trans = Transaction.find(params[:id])
        ActiveRecord::Base.transaction do
          if @trans.transaction_type === "Advance"
            @trans.wallet_add(@trans.hub_id,"Hub",@trans.sender_id,"User",@trans.amount)
            @trans.wallet_add(@trans.sender_id,"User",@trans.hub_id,"Hub",transaction_params[:amount])
          else
            @trans.wallet_add(@trans.reciever_id,"User",@trans.hub_id,"Hub",@trans.amount)
            @trans.wallet_add(@trans.hub_id,"Hub",transaction_params[:reciever_id],"User",transaction_params[:amount])
          end
          if @trans.update(transaction_params)
            render json: @trans, status: :created
          else
            render json: @trans.errors.full_messages, status: 400
          end
        end
      end

      private

      def transaction_params
        params.permit([:expense_type, :expense_sub_type, :sender_id, :reciever_id, :mod_of_payment,
          :amount, :transaction_type, :payment_id, :payment_date, :bank, :hub_id, :auth_id, :sender_name,
          :remark, :paid_for_date])
      end

    end
  end
end
