module API
  module V1
    class DcSellerDispatchManifestsController < AuthorizationController


      before_action :set_dc

      def index
        @seller_dispatch_manifests = @dc.originating_dispatch_manifests.includes(:origin,:dsp)
        render json: @seller_dispatch_manifests
      end

      def create
        @seller_dispatch_manifest = SellerDispatchManifest.new(seller_dispatch_manifest_params)
        if @seller_dispatch_manifest.save
          render json: @seller_dispatch_manifest
        else
          render json: @seller_dispatch_manifest.errors.full_messages, status:400
        end
      end

      def show
        @seller_dispatch_manifest = SellerDispatchManifest.find(params[:id])
        render json: @seller_dispatch_manifest, :serializer => SellerDispatchManifestDetailSerializer
      end


      def pickup_requests
        @requests = []
        seller_requests = PickupRequest.includes(:address,:skus,:client,:user,:hub,:pickup_request_state_histories,:seller).received_at(@dc)
        seller_requests.each do |req|
          @requests << req if req.dispatch?
        end
        render json: @requests, :each_serializer => PickupRequestSerializer
      end

      def seller_slips
        requests = PickupRequest.all
        requests = requests.where('id in (?)',params[:request_ids]) if params[:request_ids]
        requests = requests.where(manifest_id: params[:manifest_id],
                                  manifest_type: params[:manifest_type]) if params[:manifest_id]
        begin
          bundled_slip = Utilities::SlipGenerator.new(requests).generate
          render json: {file:bundled_slip.slip.url}
        rescue Exception => e
          raise e
          Airbrake.notify(e,parameters:{request:requests})
          # Rollbar.error(e)
          render json: {message:'Some Error ocurred'}, status: 400
        end
      end

      def change_state
        @seller_dispatch_manifest = SellerDispatchManifest.find(params[:id])
        @seller_dispatch_manifest.assign_attributes(change_params)
        if @seller_dispatch_manifest.send(params[:event])
          render json: {message:'State Successfully Changed'}
        else
          render json: {message:'State change failed'}
        end
      end

      private

      def seller_dispatch_manifest_params
        params.permit(:origin_id,:dsp_id,:length,:breadth,:height,:weight,:origin_type,pickup_request_ids:[])
      end

      def change_params
        params.permit(:tracking_id)
      end

      def set_dc
        @dc = DispatchCenter.find(params[:dispatch_center_id])
        return render json: [] if @dc.blank?
      end

    end
  end
end
