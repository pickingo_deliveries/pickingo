module API
  module V1
    class SubscribersController < ApplicationController

      def create
        @subscriber = Subscriber.new(subscriber_params)
        if @subscriber.save
          render json: @subscriber, status: :created
        else
          render json: @subscriber.errors, status: 400
        end
      end

      private

      def subscriber_params
        params.permit(:email)
      end

    end
  end
end
