class ApplicationController < ActionController::API
  require 'will_paginate/array'

  protected
  def self.set_pagination_headers(name, options = {})
    after_filter(options) do |controller|
      results = instance_variable_get("@#{name}")
      if results.try(:total_entries)
        headers["x-pagination"] = {
            total: results.total_entries,
            total_pages: results.total_pages,
            first_page: results.current_page == 1,
            last_page: results.next_page.blank?,
            previous_page: results.previous_page,
            next_page: results.next_page,
            out_of_bounds: results.out_of_bounds?,
            offset: results.offset
        }.to_json
      end
    end
  end

end
