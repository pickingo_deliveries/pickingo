class ClientTokenAuthController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_action :restrict_access

  def current_client
    @current_client
  end

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      client_token = ClientToken.find_by(access_token: token)
      if client_token
        @current_client = client_token.client_customer.client
        return true
      else
        return render json: {message:'Access Denied'}, status:401
      end
    end
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end

end
