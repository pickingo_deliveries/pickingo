class SmSerializer < ActiveModel::Serializer

  cached

  attributes :id,:phone, :client_request_id, :state, :client_name, :current_status, :content, :hub_name, :user_name

  def state
    object.pickup_request_state_history.state
  end

  def client_request_id
    object.pickup_request_state_history.pickup_request.client_request_id
  end

  def current_status
    object.pickup_request_state_history.pickup_request.aasm_state
  end

  def client_name
    object.pickup_request_state_history.pickup_request.client.name
  end

  def hub_name
    object.pickup_request_state_history.pickup_request.hub.name
  end

  def user_name
    if object.pickup_request_state_history.pickup_request.user.nil?
      nil
    else
      object.pickup_request_state_history.pickup_request.user.user_name
    end
  end

  def cache_key
    [object.id, object.updated_at]
  end

end