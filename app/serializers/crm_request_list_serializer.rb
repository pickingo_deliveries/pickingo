class CRMRequestListSerializer < ActiveModel::Serializer

  attributes :id,:client_order_number, :client_request_id , :picked_date, :hub_name,
             :date_created, :status, :scheduled_date, :client_name,:remarks

  has_many :skus
  has_one :address

  def remarks
    unless object.pickup_request_state_histories.blank?
      object.pickup_request_state_histories.last.comment if object.pickup_request_state_histories.last.comment
    end
  end

  def status
    object.aasm_state.titleize
  end

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def client_name
    object.client.name
  end

  def hub_name
    object.hub.name
  end

end