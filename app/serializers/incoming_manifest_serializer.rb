class IncomingManifestSerializer < ActiveModel::Serializer
  attributes :id, :request_count, :origin, :uid, :destination, :state, :date_created, :pickup_requests

  def origin
    object.origin.name
  end

  def destination
    object.destination.display_name
  end

  def request_count
    object.pickup_requests.size
  end

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def pickup_requests
    object.pickup_requests.includes(:client, :address, :skus).collect do |pur|
      {id:pur.id,client_order_number: pur.client_order_number, client_request_id:pur.client_request_id,
       status:pur.aasm_state.titleize, date_created:pur.created_at.to_time.to_i*1000,
       address: pur.address,
       skus: pur.skus,
       client: {name:pur.client.name, id:pur.client.id},
       }
    end
  end

  # def cache_key
  #   [object.id, object.updated_at, object.address.updated_at]
  # end

end