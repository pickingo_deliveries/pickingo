class DispatchCenterManifestSerializer < ActiveModel::Serializer

  cached
  delegate :cache_key , to: :object

  attributes :id, :origin,:destination,:uid,:client, :dc_vehicle, :pdf_file, :excel_file,
             :length, :breadth, :height, :weight, :request_count, :state, :date_created, :seal_number


  def date_created
    object.created_at.to_time.to_i*1000
  end

  def request_count
    object.pickup_requests.size
  end

  def pdf_file
    object.pdf_file.url if object.pdf_file.exists?
  end

  def excel_file
    object.excel_file.url if object.excel_file.exists?
  end

end