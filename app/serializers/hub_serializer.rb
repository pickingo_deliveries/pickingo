class HubSerializer < ActiveModel::Serializer

  cached
  delegate :cache_key, to: :object

  attributes :id, :name, :city, :address_line, :dec, :pb_count, :ac_user_count, :in_user_count, :res_user_count

  def pb_count
  	object.users.where(status: 'active').joins(:role, :user_profile).where("roles.name = 'Field Executive'").size
  end

  def ac_user_count
  	object.users.where(status: 'active').count - 1
  end

  def in_user_count
  	object.users.where(status: 'inactive').size
  end

  def res_user_count
  	object.users.where(status: 'resigned').size
  end

  # has_many :users

  # def pickup_boys
  #   object.users.joins(:role, :user_profile).where("roles.name = 'Field Executive'")
  # end

end