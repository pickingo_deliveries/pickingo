class ClientRequestSerializer < ActiveModel::Serializer
  attributes :client_order_number, :client_request_id, :date_created, :status, :scheduled_date, :seller, :status_last_updated_at

  has_one :address
  has_many :skus
  has_many :pickup_request_state_histories


  #cached

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def status_last_updated_at
    if object.pickup_request_state_histories.present?
      (object.pickup_request_state_histories.last.created_at.to_time.to_i)*1000
    end
  end

  def status
    object.aasm_state.titleize
  end

  def scheduled_date
    unless object.scheduled_date.nil?
      (object.scheduled_date.to_time.to_i)*1000
    end
  end

  def seller
    object.seller if object.seller_present?
  end

  def cache_key
    [object.id, object.updated_at, object.address.updated_at]
  end


end