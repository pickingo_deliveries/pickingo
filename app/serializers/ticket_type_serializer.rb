class TicketTypeSerializer < ActiveModel::Serializer

  cached
  delegate :cache_key , to: :object

  attributes :id,:name, :display_name, :department

end