class AssetSerializer < ActiveModel::Serializer
  attributes :id,:emp_id,:emp_name,:emp_designation,:serial_no,:reference_no, :asset_type,:state,:assign_date

  def id
    object.id
  end

  def state
    object.state
  end

  def emp_id
    if object.entity && object.user_id != nil?
      user = User.find_by(id: object.user_id)
      user.uid unless user.nil?
    else
      "N/A"
    end
  end

  def emp_name
    if object.entity && object.user_id != nil?
      user = User.find_by(id: object.user_id)
      user.user_profile.full_name unless user.nil?
    end
  end

  def emp_designation
    if object.entity && object.user_id != nil?
      user = User.find_by(id: object.user_id)
      user.role.name unless user.nil?
    end
  end

  def serial_no
    object.serial_no if object.serial_no
  end

  def reference_no
    object.reference_no if object.reference_no
  end

  def asset_type
    object.asset_type if object.asset_type
  end

  def assign_date
    if object.asset_transitions.length > 0
       object.asset_transitions.last.assign_date
    end
  end

end
