class RouteSerializer < ActiveModel::Serializer
  attributes :id, :origin, :destination,:display_name, :default

  cached
  delegate :cache_key , to: :object

  def origin
    {id:object.origin.id, dislay_name:object.origin.name}
  end

  def destination
    {id:object.route_destination.id, display_name:object.route_destination.display_name,destination_type:object.route_destination.class.name }
  end

  def display_name
    object.display_name
  end

  def default
    if object.default
      true
    else
      false
    end
  end


end