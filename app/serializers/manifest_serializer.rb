class ManifestSerializer < ActiveModel::Serializer

  cached

  delegate :cache_key, to: :object


  attributes :id, :request_count, :client_name, :origin, :client_id,:uid,:start,:halt,
             :destination, :state, :date_created, :start_id, :halt_id, :halt_type



  # def file
  #   object.file.url if object.file.exists?
  # end

  def origin
    object.origin.name
  end

  def destination
    object.destination.name
  end

  def client_name
    object.client.name
  end

  def client_id
    object.client.id
  end

  def request_count
    object.pickup_requests.size
  end

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def start
    object.start ? object.start.name : ''
  end

  def halt
    object.halt ? object.halt.name : ''
  end

  # def cache_key
  #   [object.id, object.updated_at, object.address.updated_at]
  # end

end