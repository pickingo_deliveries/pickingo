module PurSerializers
  class PurMinSerializer < ActiveModel::Serializer
    #cached

    attributes :id,:client_order_number, :client_request_id, :hub, :date_created, :client,
               :status, :remarks, :scheduled_date, :user, :user_id, :length, :breadth, :height, :weight,:initial_weight,
               :attempts,:return_reason,:is_urgent

    has_one :address
    has_many :skus

    def remarks
      object.remarks if object.remarks
      # unless object.pickup_request_state_histories.blank?
      #   object.pickup_request_state_histories.last.comment if object.pickup_request_state_histories.last.comment
      # end
    end

    def attempts
        #object.attempts

      PickupRequestStateHistory.where("state = ? AND pickup_request_id = ?",'out_for_pickup',object.id).size -
          PickupRequestStateHistory.where("state = ? AND pickup_request_id = ?",'not_attempted',object.id).size

    end

    def date_created
      (object.created_at.to_time.to_i)*1000
    end

    def status
      object.aasm_state.titleize
    end

    def user
      {id:object.user.id, name:object.user.full_name} if object.user
    end

    def hub
      {id:object.hub.id,name:object.hub.name}
    end

    def length
      object.length.to_i
    end

    def breadth
      object.breadth.to_i
    end

    def weight
      object.weight.to_f
    end

    def height
      object.height.to_i
    end

    def cache_key
      [object.id, object.updated_at]
    end
  end
end
