module PurSerializers
  class HubPurSerializer < ActiveModel::Serializer
    cached

    attributes :id,:client_order_number, :client_request_id, :hub, :date_created,
               :status, :client

    # has_one :address
    # has_many :skus

    def date_created
      (object.created_at.to_time.to_i)*1000
    end

    def status
      object.aasm_state.titleize
    end

    def hub
      {id:object.hub.id,name:object.hub.name}
    end

    def cache_key
      [object.id, object.updated_at]
    end
  end
end
