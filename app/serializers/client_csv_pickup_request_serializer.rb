class ClientCsvPickupRequestSerializer < ActiveModel::Serializer
  attributes :client_order_number, :client_request_id,:status, :address,:date_created, :picked_date,:weight,
               :length, :breadth, :height

  cached

  def date_created
    object.created_at.strftime("%d-%m-%Y")
  end

  def status
    object.aasm_state.titleize
  end

  def picked_date
    if object.aasm_state == 'received' || object.aasm_state == 'closed' || object.aasm_state == 'in_transit'
      object.pickup_request_state_histories.where(state:'received').first.created_at.to_date
    else
      nil
    end
  end

  def address
    object.complete_address
  end

  def cache_key
    [object.id, object.updated_at, object.address.updated_at]
  end


end