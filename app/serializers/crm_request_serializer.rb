class CRMRequestSerializer < ActiveModel::Serializer

  attributes :id,:client_order_number,:client_request_id,:picked_date, :hub_name,
             :date_created, :status, :scheduled_date, :remarks,:attempts, :client_name

  has_many :skus
  has_one :user
  has_one :address
  has_many :pickup_request_state_histories


  def status
    object.aasm_state.titleize
  end

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def remarks
    object.remarks
  end

  def client_name
    object.client.name
  end

  def hub_name
    object.hub.name
  end

end