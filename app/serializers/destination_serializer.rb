class DestinationSerializer < ActiveModel::Serializer

  attributes :id, :name, :destination_type, :request_destination

  def id
    object.location.id
  end

  def name
    object.location.display_name + ' (Final Request for ' + final_destination + ' )'
  end

  def destination_type
    object.location.class.name
  end

  def final_destination
    object.route.destination.display_name
  end

  def request_destination
    object.route.destination
  end

end