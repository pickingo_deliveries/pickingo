class CommentSerializer < ActiveModel::Serializer

  cached
  delegate :cache_key , to: :object

  attributes :id, :comment,:title, :crm_user, :created_at

  def created_at
    object.created_at.to_time.to_i*1000
  end

end