class AddressSerializer < ActiveModel::Serializer
  attributes :address_line, :city, :country, :pincode, :name, :phone_number,
             :locality, :sub_locality,:id

  cached
  delegate :cache_key , to: :object

  def name
    object.name.titleize
  end
end