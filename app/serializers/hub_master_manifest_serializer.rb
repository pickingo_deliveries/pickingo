class HubMasterManifestSerializer < ActiveModel::Serializer

  attributes :id, :manifest_count, :origin, :origin_details, :uid, :destination, :state, :date_created,
             :display_state, :chargeable_weight, :gross_weight

  def origin
    object.origin.name
  end

  def origin_details
    object.origin.attributes
  end

  def display_state
    object.state.titleize
  end

  def manifest_count
    object.hub_manifests.size
  end

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def destination
    object.destination.attributes.merge({destination_type:object.destination_type,display_name:object.destination.display_name})
  end

  # def pdf_file
  #   object.pdf_file.url if object.pdf_file.exists?
  # end
  #
  # def excel_file
  #   object.excel_file.url if object.excel_file.exists?
  # end
end