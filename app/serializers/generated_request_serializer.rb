class GeneratedRequestSerializer < ActiveModel::Serializer
  attributes :code, :client_name

  cached

  delegate :cache_key, to: :object

  def client_name
    object.client.name
  end

end