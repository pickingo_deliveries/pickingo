class ManifestRequestSerializer < ActiveModel::Serializer

  cached

  attributes :id,:client_order_number, :client_request_id, :hub, :date_created, :status, :destination, :destination_type

  has_one :address
  has_many :skus
  has_one :client

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def status
    object.aasm_state.titleize
  end

  def hub
    {id:object.hub.id,name:object.hub.name}
  end

  def destination
    {id:object.final_destination.id,name:object.final_destination.display_name}
  end

  def destination_type
    object.final_destination.class.name
  end

  def cache_key
    [object.id, object.updated_at]
  end

end