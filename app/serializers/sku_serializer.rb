class SkuSerializer < ActiveModel::Serializer

  cached

  attributes :id,:name,:picked

  delegate :cache_key, to: :object

end