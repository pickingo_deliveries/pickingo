class ClientRequestDetailsSerializer < ActiveModel::Serializer
  attributes :client_order_number, :client_request_id,
              :date_created, :status, :scheduled_date,:client_name, :hub_name

  has_one :address
  has_many :skus
  has_many :pickup_request_state_histories

  cached
  delegate :cache_key , to: :object

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def status
    object.aasm_state.titleize
  end

  def scheduled_date
    unless object.scheduled_date.nil?
      (object.scheduled_date.to_time.to_i)*1000
    end
  end

  def client_name
    object.client.name
  end

  def hub_name
    object.hub.name
  end


end