class DispatchCenterMasterManifestSerializer < ActiveModel::Serializer
  attributes :id, :origin,:destination,:uid,:client, :dc_vehicle,
             :manifest_count, :state, :date_created, :pdf_file, :excel_file, :request_excel_file

  def date_created
    object.created_at.to_time.to_i*1000
  end

  def manifest_count
    object.dispatch_center_manifests.size
  end

  def pdf_file
    object.pdf_file.url if object.pdf_file.exists?
  end

  def excel_file
    object.excel_file.url if object.excel_file.exists?
  end

  def request_excel_file
    object.request_excel_file.url if object.request_excel_file.exists?
  end

end