class AppPickupRequestSerializer < ActiveModel::Serializer
  attributes :id,:client_order_number, :client_request_id,:aasm_state,:date_created, :weight,
             :length, :breadth, :height,:scheduled_date,:remarks,:barcode, :client_name, :hub_name

  has_many :skus

  has_one :address

  cached

  def date_created
    object.created_at.strftime("%d-%m-%Y")
  end

  def client_name
    object.client.try(:name)
  end

  def hub_name
    object.hub.try(:name)
  end


  def cache_key
    [object.id, object.updated_at, object.address.updated_at]
  end


end