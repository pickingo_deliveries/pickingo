class HubManifestSerializer < ActiveModel::Serializer

  attributes :id, :request_count, :origin, :uid, :destination, :state, :display_state, :date_created,
             :destination_type, :seal_number

  cached
  delegate :cache_key , to: :object


  def origin
    object.origin.name
  end

  def request_count
    object.pickup_requests.size
  end

  def display_state
    object.state.titleize
  end

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def destination
    object.destination.attributes.merge({destination_type: object.destination.class.name, display_name: object.destination.display_name})
  end

  # def pdf_file
  #   object.pdf_file.url if object.pdf_file.exists?
  # end
  #
  # def excel_file
  #   object.excel_file.url if object.excel_file.exists?
  # end

end