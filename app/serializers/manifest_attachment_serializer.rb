class ManifestAttachmentSerializer < ActiveModel::Serializer

  attributes :pdf_file, :excel_file, :request_excel_file

  def pdf_file
    object.pdf_file.url if object.respond_to?(:pdf_file) && object.pdf_file.exists?
  end

  def excel_file
    object.excel_file.url if object.respond_to?(:excel_file) && object.excel_file.exists?
  end

  def request_excel_file
    object.request_excel_file.url if object.respond_to?(:request_excel_file) && object.request_excel_file.exists?
  end

end