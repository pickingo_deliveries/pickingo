class HubManifestDetailsSerializer < ActiveModel::Serializer
  attributes :id, :request_count, :origin, :uid, :destination, :display_state,
             :date_created, :pdf_file, :excel_file, :pickup_requests

  has_many :hub_manifest_histories

  def origin
    object.origin.name
  end

  def destination
    object.destination.display_name
  end

  def request_count
    if (object.state == 'received' && object.created_at >= "Thu, 13 Oct 2015 00:00:00 IST +05:30".to_date)
      object.request_ids.size
    else
      object.pickup_requests.size
    end
  end

  def date_created
    (object.created_at.to_time.to_i)*1000
  end
  
  def pdf_file
    object.pdf_file.url if object.pdf_file.exists?
  end

  def excel_file
    object.excel_file.url if object.excel_file.exists?
  end

  def display_state
    object.state.titleize
  end
  
  def pickup_requests
    object.pickup_requests.includes(:client).collect { |pur| PurSerializers::HubPurSerializer.new(pur) }
  end

  # def cache_key
  #   [object.id, object.updated_at, object.address.updated_at]
  # end

end