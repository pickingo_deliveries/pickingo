class UserSerializer < ActiveModel::Serializer

  attributes :id, :email, :role_id, :role_name, :name, :status, :entity_type, :entity_id,
             :code, :request_count, :vendor, :vendor_id, :vendor_emp_id, :reffered_by, :ctc,:is_login

  has_one :user_profile

  def vendor
    object.vendor.name if object.vendor_id
  end


  def status
    object.status.try(:titleize)
  end

  def role_name
    object.role.name
  end

  def name
    object.full_name
  end

  def request_count
    object.pickup_requests.size
  end

end