class HubMasterManifestDetailSerializer < ActiveModel::Serializer

  cached
  delegate :cache_key , to: :object

  attributes :id, :manifest_count, :origin, :uid, :destination, :state, :date_created, :destination_type,
             :display_state, :pdf_file, :excel_file, :hub_manifests, :chargeable_weight, :gross_weight

  has_many :hub_master_manifest_histories

  def origin
    object.origin.name
  end

  def display_state
    object.state.titleize
  end

  def manifest_count
    object.hub_manifests.size
  end

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def destination
    object.destination.attributes.merge({destination_type:object.destination.class.name,display_name:object.destination.display_name})
  end

  def pdf_file
    object.pdf_file.url if object.pdf_file.exists?
  end

  def excel_file
    object.excel_file.url if object.excel_file.exists?
  end

  def hub_manifests
    object.hub_manifests.includes(:destination).collect { |hm| {id:hm.id,uid:hm.uid, state:hm.state, destination:{name:hm.destination.display_name, id:hm.destination.id, destination_type:hm.destination.class.name}, date_created:hm.created_at.to_time.to_i*1000, request_count:hm.pickup_requests.size} }
  end

end