class PickupBoySerializer < ActiveModel::Serializer
  attributes :id, :name, :phone, :active

  cached

  delegate :cache_key, to: :object

  def active
    if object.active
      'Active'
    else
      'Inactive'
    end
  end


end