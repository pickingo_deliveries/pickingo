class PickupRequestSerializer < ActiveModel::Serializer

  cached

  attributes :id,:client_order_number, :client_request_id, :client,:picked_date,:weight,
             :volumetric_weight, :hub, :destination, :current_location, :date_created, :status, :user_id,
             :scheduled_date, :remarks,:attempts, :length, :breadth, :height, :dsp, :local, :dispatch, :user

  has_one :address
  has_one :awb_number
  has_many :skus
  has_one :client
  has_one :seller

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def user
    {id:object.user.id, name:object.user.full_name} if object.user
  end

  def current_location
    {id:object.current_location.id,name:object.current_location.name} if object.current_location
  end

  def status
    object.aasm_state.titleize
  end

  def scheduled_date
    unless object.scheduled_date.nil?
      (object.scheduled_date.to_time.to_i)*1000
    end
  end

  def length
    object.length.to_i
  end

  def breadth
    object.breadth.to_i
  end

  def weight
    object.weight.to_f
  end

  def height
    object.height.to_i
  end

  def volumetric_weight
    (object.height.to_f*object.length.to_f*object.breadth.to_f)/5000
  end

  def hub
    {id:object.hub.id,name:object.hub.name}
  end

  def destination
    {id:object.final_destination.id,name:object.final_destination.display_name} if object.final_destination
  end

  def local
    !object.returned_to_client? && object.local?
  end

  def dispatch
    object.dispatch?
  end


  def cache_key
    [object.id, object.updated_at]
  end

end