class PickupRequestStateHistorySerializer < ActiveModel::Serializer
  attributes :state, :created_at, :current_location,:comment#,:manifest_id

  cached
  delegate :cache_key , to: :object

  def state
    object.state.titleize
  end

  def comment
    object.comment if object.comment
  end

  # def current_location
  #
  #   return object.pickup_request.hub.try(:name).try(:titleize) if object.manifest.nil?
  #
  #   if object.manifest && object.state == 'received'
  #     return object.manifest.try(:destination).try(:name).try(:titleize)
  #   else
  #     return object.manifest.try(:origin).try(:name).try(:titleize)
  #   end
  #
  #   #return object.manifest.try(:origin).try(:name).try(:titleize) if object.manifest
  #   #return object.pickup_request.try(:current_location).try(:name).try(:titleize)
  # end

  def current_location
    if object.created_at <= "Wed, 17 Sep 2015 00:00:00 IST +05:30".to_date
        return object.pickup_request.hub.try(:name).try(:titleize) if object.manifest.nil?

        if object.manifest && object.state == 'received'
          return object.manifest.try(:destination).try(:name).try(:titleize)
        else
          return object.manifest.try(:origin).try(:name).try(:titleize)
        end
    else
         return object.try(:current_location).try(:name).try(:titleize)
    end
  end



end