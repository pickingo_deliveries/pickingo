class TicketSerializer < ActiveModel::Serializer

  cached

  attributes :id,:title, :description,:date_created,:date_updated, :owner,
             :assignee, :ticket_type,:ticket_sub_type, :uid, :priority, :status


  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def date_updated
    (object.updated_at.to_time.to_i)*1000
  end

  def cache_key
    [object.id, object.updated_at]
  end

end