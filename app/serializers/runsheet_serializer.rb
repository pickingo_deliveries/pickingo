class RunsheetSerializer < ActiveModel::Serializer

  cached
  delegate :cache_key , to: :object
  
  attributes :id, :url

  def url
    object.file.url
  end
end