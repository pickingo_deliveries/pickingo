class TicketDetailSerializer < ActiveModel::Serializer

  cached

  attributes :id,:title, :description,:date_created,:date_updated, :owner, :status,:assigned_to,
             :assignee, :ticket_type,:ticket_sub_type, :uid, :priority, :pickup_request,:call_type

  has_many :comments

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def date_updated
    (object.updated_at.to_time.to_i)*1000
  end

  def cache_key
    return [object.id, object.updated_at, object.pickup_request.updated_at] if object.pickup_request_id
    return [object.id, object.updated_at] if !object.pickup_request_id
  end

end