class SellerDispatchManifestSerializer < ActiveModel::Serializer
  cached
  delegate :cache_key , to: :object

  attributes :id, :uid, :origin, :dsp, :length, :weight, :breadth, :height,
             :request_count, :state, :date_created, :tracking_id, :display_state#, :pdf_file, :excel_file

  def request_count
    object.pickup_requests.size
  end

  def display_state
    object.state.titleize
  end

  def origin
    {id:object.origin.id,name:object.origin.name,type:object.origin.class.name}
  end

  def hub
    {id:object.pincode.hub.id, name:object.pincode.hub.name}
  end

  def date_created
    object.created_at.to_time.to_i*1000
  end

  # def pdf_file
  #   object.pdf_file.url if object.pdf_file.exists?
  # end
  #
  # def excel_file
  #   object.excel_file.url if object.excel_file.exists?
  # end
end