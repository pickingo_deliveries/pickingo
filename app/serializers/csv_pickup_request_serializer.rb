class CsvPickupRequestSerializer < ActiveModel::Serializer
  attributes :client_order_number, :client_request_id, :client,:status, :address,:date_created, :hub, :remarks,:user,
             :scheduled_date,:received_date,:weight,:length, :breadth,:height

  cached

  def date_created
    object.created_at.strftime("%d-%m-%Y")
  end

  def status
    object.aasm_state.titleize
  end

  def client
    object.client.name.titleize
  end

  def scheduled_date
    unless object.scheduled_date.nil?
      object.scheduled_date.strftime("%d-%m-%Y")
    end
  end

  def received_date
    if object.aasm_state == 'received' || object.aasm_state == 'closed'|| object.aasm_state == 'in_transit'
      rd = object.pickup_request_state_histories.where(state:'received')
      rd.first.created_at.to_date if rd.first
    else
      nil
    end
  end

  def address
    object.complete_address
  end

  def hub
    object.hub.name
  end

  def user
    object.user.user_name if object.user
  end

  def cache_key
    [object.id, object.updated_at, object.address.updated_at]
  end


end