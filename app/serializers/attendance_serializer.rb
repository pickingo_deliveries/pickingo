class AttendanceSerializer < ActiveModel::Serializer

  cached

  attributes :id, :status, :date, :start_time, :end_time, :start_km, :end_km, :user_name, :user_id

  def user_name
    object.user.full_name
  end

  def user_id
    object.user.id
  end

  def cache_key
    [object.id, object.updated_at]
  end

end