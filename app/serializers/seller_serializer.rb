class SellerSerializer < ActiveModel::Serializer

  cached


  attributes :id, :name, :address_line,:city, :country, :phone, :pincode, :hub

  def pincode
    object.pincode.code
  end

  def hub
    {id:object.pincode.hub.id, name:object.pincode.hub.name} if object.pincode.hub
  end

  def cache_key
    [object.id, object.updated_at, object.pincode.updated_at]
  end


end