class InventorySerializer < ActiveModel::Serializer

  cached

  attributes :id,:client_order_number, :client_request_id, :client,:weight,
              :destination, :date_created, :status, :local, :dispatch


  has_many :skus

  def date_created
    (object.created_at.to_time.to_i)*1000
  end

  def status
    object.aasm_state.titleize
  end

  def scheduled_date
    unless object.scheduled_date.nil?
      (object.scheduled_date.to_time.to_i)*1000
    end
  end

  def destination
    {id:object.final_destination.id,name:object.final_destination.display_name} if object.final_destination
  end

  def local
    !object.returned_to_client? && object.local?
  end

  def dispatch
    object.dispatch?
  end


  def cache_key
    [object.id, object.updated_at, object.final_destination.updated_at]
  end

end