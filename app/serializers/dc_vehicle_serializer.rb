class DcVehicleSerializer < ActiveModel::Serializer

  cached
  delegate :cache_key , to: :object

  attributes :id, :driver_name,:driver_phone,:vehicle_model,:reg_number,:display_name

  def display_name
    object.vehicle_model + "(#{object.reg_number})"
  end

end