class PurHistoryWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(pur_history_id)
    pickup_history = PickupRequestStateHistory.find(pur_history_id)
    boy = PickupBoy.find_by(id: pickup_history.pickup_boy_id)
    pickup_history.update_column(:user_id, User.find_by(email: boy.email).id) if boy && pickup_history.pickup_request
  end
end