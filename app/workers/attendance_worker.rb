class AttendanceWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(attendance_id)
    attendance = Attendance.find(attendance_id)
    boy = PickupBoy.find_by(id: attendance.pickup_boy_id)
    attendance.update_column(:user_id,User.find_by(email: boy.email).id) if boy
  end
end