class BarcodeSheetWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(barcode_id)
    barcode_sheet = BarcodeSheet.find(barcode_id)
    boy = PickupBoy.find_by(id: barcode_sheet.pickup_boy_id)
    barcode_sheet.update_column(:user_id, User.find_by(email: boy.email).id) if boy
  end
end