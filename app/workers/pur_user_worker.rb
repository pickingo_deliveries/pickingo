class PurUserWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(pur_id)
    pickup_request = PickupRequest.find(pur_id)
    boy = PickupBoy.find_by(id: pickup_request.pickup_boy_id)
    pickup_request.update_column(:user_id,User.find_by(email: boy.email).id) if boy
  end
end