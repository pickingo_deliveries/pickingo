class UserEntityWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(user_id)
    user = User.find(user_id)
    if user.hub_id
      user.entity = Hub.find_by(id: user.hub_id)
    elsif user.dispatch_center_id
      user.entity = DispatchCenter.find_by(id: user.dispatch_center_id)
    end
    user.save
    UserProfile.find_or_initialize_by(user_id: user.id).save
  end
end