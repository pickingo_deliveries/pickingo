class RunsheetWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(runsheet_id)
    runsheet = Runsheet.find(runsheet_id)
    boy = PickupBoy.find_by(id: runsheet.pickup_boy_id)
    runsheet.update_column(:user_id,User.find_by(email: boy.email).id) if boy
  end
end