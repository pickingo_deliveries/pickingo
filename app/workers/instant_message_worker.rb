class InstantMessageWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(history_ids)
    puts "====#{history_ids.inspect}==InsideWorker"
    count = 0
    smses = Sm.where({pickup_request_state_history_id: history_ids })
    xml = SmsXml.new(smses)
    xml_file = xml.build_xml
    begin
      Api::Sms.send_multiple(xml_file)
      smses.update_all(sent:true)
    rescue Exception => e
      Airbrake.notify_or_ignore(e, parameters: {message:"SMS message failed"})
      #failed_ids << request.id
      count = count+1
      puts e
    end
  end
end