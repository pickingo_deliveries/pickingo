class MoveUsersWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(boy_id)
    role = Role.find_by(short_name: 'FE')
    boy = PickupBoy.find(boy_id)
    status = boy.active ? 'active' : 'inactive'
    user = User.find_or_initialize_by(email: boy.email, status: status, aasm_state: boy.aasm_state,
                                      entity_type: 'Hub', entity_id: boy.hub_id, role: role)
    user.password = boy.password
    user.user_name = boy.name
    user.save
    UserProfile.find_or_initialize_by(company_number: boy.phone, user: user, first_name: boy.name).save
  end
end