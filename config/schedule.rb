# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#

every 1.day,:at => '3:00 pm' do
  rake 'cleanup:files'
end

every 1.day, :at => '6:30 pm' do
  rake 'pickup_boy:checkout'
end

every 1.day, :at => '00:10 am' do
  rake 'stored_procs:all'
end

# Learn more: http://github.com/javan/whenever
