require 'sidekiq/web'
Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: '/auth'

  mount_devise_token_auth_for 'PickupBoy', at: '/pb_auth', controllers: {
      sessions:  'overrides/pb_sessions'
  }

  mount_devise_token_auth_for 'ClientCustomer', at: '/clients'

  mount_devise_token_auth_for 'CRMUser', at: '/crm_auth'
  as :crm_user do
    # Define routes for CRMUser within this block.
  end

  mount Sidekiq::Web, at: '/sidekiq'


  namespace :api, :defaults => {:format => :json} do
    namespace :v1 do
      resources :users
      resources :transactions
      resources :wallets, only:['index']
      resources :roles, only:['index']
      resources :expenses
      resources :dsps, only:['index']
      resources :awb_numbers, only:['index'] do
        collection do
          post 'upload'
        end
      end

      resources :assets, only: [:index,:create,:update] do
        member do
          post 'assign'
          post 'unassign'
        end
        collection do
          get 'get_summary'
        end
      end

      resources :client_seller_pincode_destinations, only:['index'] do
        collection do
          post 'upload'
        end
      end
      resources :client_dsps, only:['index'] do
        collection do
          post 'upload'
        end
      end
      resources :client_dsp_pincodes, only:['index'] do
        collection do
          post 'upload'
        end
      end
      resources :requests, only:['show']
      resources :routes, only:['index']
      resources :subscribers, only:['create']
      resources :client_pincode_destination, only:['index'] do
        collection do
          post 'upload'
        end
      end

      resources :pickup_requests do
        collection do
          get 'metrics'
          get 'revert'
          post 'upload'
          post 'assign'
          post 'unassign'
          post 'close'
          post 'out_for_pickup'
          post 'schedule_later'
          get 'find_master'
        end
        member do
          get 'change_state'
          put 'claim'
        end
      end

      resources :dispatch_centers, :only => ['index', 'show'] do
        member do
          get 'inventory'
          get 'generate_inventory_excel'
        end
        resources :dc_hub_manifest, :only => ['index'] do
          collection do
            get 'hub_manifest'
            get 'receive_request'
            get 'change_hub_state'
            get 'change_master_state'
            get 'single_receive'
          end
        end
        resources :dispatch_center_manifests, :only => [:index,:create,:update, :show] do
          collection do
            get 'find_request'
            get 'incoming'
          end
          member do
            get 'change_state'
          end
        end
        resources :dispatch_center_master_manifests, :only => [:index,:create,:update, :show] do
          member do
            get 'change_state'
          end
        end
        resources :dc_seller_dispatch_manifests, only:['index','create','show'] do
          collection do
            get 'pickup_requests'
            get 'seller_slips'
          end

          member do
            get 'change_state'
          end
        end
        resources :dc_seller_local_manifests, only:['index','create','show'] do
          collection do
            get 'pickup_requests'
            get 'seller_slips'
          end

          member do
            get 'change_state'
          end
        end
        resources :dc_vehicles, only:[:index,:show,:create,:destroy]
      end

      resources :hubs do
        member do
          get 'destinations'
          get 'inventory'
          get 'generate_inventory_excel'
          get 'generate_summary'
        end
        resources :seller_dispatch_manifests, only:['index','create','show'] do
          collection do
            get 'pickup_requests'
            get 'seller_slips'
            # post 'upload_request'
          end
          member do
            get 'change_state'
          end
        end
        resources :seller_local_manifests, only:['index','create','show'] do
          collection do
            get 'pickup_requests'
            get 'seller_slips'
          end
          member do
            get 'change_state'
          end
        end
        resources :users, :only => [] do
          resources :runsheets , :only => [:index]
          resources :barcode_sheets , :only => [:index]
          collection do
            get 'attendances'
          end
          get 'pickup_requests'
        end
        resources :pincodes
        resources :hub_manifests do
          collection do
            get 'find_request'
            get 'receive_request'
          end
          member do
            get 'change_state'
          end
        end
        resources :hub_master_manifests do
          member do
            get 'change_state'
          end
          collection do
            get 'incoming'
          end
        end
        resources :hub_destination_routes , only:['index','update']
      end

      resources :seller_dispatch_manifests, only:['upload_request'] do
        collection do
          post 'upload_request'
        end
      end

      resources :users, only: ['get_employee'] do
        member do
          get 'get_employee'
        end
      end

      resources :coloaders, only: ['index']
      resources :vendors, only: ['index']
      resources :clients
      resources :client_warehouses, only:['index']

      resources :attendances, :only => [:create]

      resources :sms, :only => [:index] do
        collection do
          post 'send_sms'
        end
      end


      resources :generated_requests, :only => [:index, :create]


      namespace :pickup_boys do
        scope ":pickup_boy_id" do
          resources :requests, only: %w[index show update]
          resources :boys, only: %w[show update]
          resources :pb_activities do
            collection do
              post 'check_in'
              post 'check_out'
              post 'trip_start'
              post 'trip_stop'
            end
          end
        end
      end

      namespace :clients do
        scope ":client_id" do
          resources :requests, only: %w[index show create]
          resources :dashboard,  only: %w[index] do
            collection do
              get 'metrics'
            end
          end
        end
      end

      namespace :crm do
        resources :requests, only:['index','show']
        resources :comments, only:['index']
        resources :ticket_types, only:['index'] do
          resources :ticket_sub_types, only:['index']
        end
        resources :tickets, only:['index','show','create','tickets_count'] do
          member do
            post 'comment'
            post 'change_state'
          end
          collection do
            get 'tickets_count'
          end
        end
        resources :departments, only:['index'] do
          resources :crm_users, only:['index']
        end
      end

    end

    namespace :v2 do
      namespace :clients do
        resources :requests, only: %w[index show create] do
          collection do
            post 'bulk_query'
            post 'mark_cancel'
            get 'awbs'
          end
        end
        resources :dashboard,  only: %w[index] do
          collection do
            get 'metrics'
          end
        end
      end
    end
  end
end
